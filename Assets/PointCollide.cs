using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
public class PointCollide : MonoBehaviour
{


    public void Start()
    {
        //this.GetComponent<Renderer>().enabled = false;
        
    }
    // Update is called once per frame
    void Update()
    {
        Vector3 normal, positionRay;
        int index;
        bool hit;
        //Vector3[] lines = new Vector3[] {new Vector3(), new Vector3() };
        //int numberpts;
        GetComponentInParent<XRRayInteractor>().TryGetHitInfo( out positionRay, out normal, out index, out hit);
        if (hit)
        {

            this.transform.position = positionRay;

        }
        else
        {
            if (GetComponentInParent<LineRenderer>().positionCount > 1)
            {
                this.transform.position = GetComponentInParent<LineRenderer>().GetPosition(1);
            }
            
        }
    }
}
