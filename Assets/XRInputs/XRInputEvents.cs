using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class XRInputEvents : MonoBehaviour
{
    public static XRInputEvents currentXRInputEvents;

    public UnityEvent translationButtonDownEvent;
    public UnityEvent translationButtonPressedEvent;
    public UnityEvent translationButtonUpEvent;


    public bool TranslationButtonDown { get => translationButtonDown; }
    public bool TranslationButtonPressed { get => translationButtonPressed; }
    public bool TranslationButtonUp { get => translationButtonUp; }


    private bool translationButtonDown;
    private bool translationButtonPressed;
    private bool translationButtonUp;


    public Vector2 TranslationVector2Value { get => translationVector2Value; }
    private Vector2 translationVector2Value;


    public UnityEvent rotationButtonDownEvent;
    public UnityEvent rotationButtonPressedEvent;
    public UnityEvent rotationButtonUpEvent;


    public bool RotationButtonDown { get => rotationButtonDown; }
    public bool RotationButtonPressed { get => rotationButtonPressed; }
    public bool RotationButtonUp { get => rotationButtonUp; }


    private bool rotationButtonDown;
    private bool rotationButtonPressed;
    private bool rotationButtonUp;


    public Vector2 RotationVector2Value { get => rotationVector2Value; }
    private Vector2 rotationVector2Value;

    public UnityEvent goNextButtonDownEvent;
    public UnityEvent goNextButtonPressedEvent;
    public UnityEvent goNextButtonUpEvent;


    public bool GoNextButtonDown { get => goNextButtonDown; }
    public bool GoNextButtonPressed { get => goNextButtonPressed; }
    public bool GoNextButtonUp { get => goNextButtonUp; }


    private bool goNextButtonDown;
    private bool goNextButtonPressed;
    private bool goNextButtonUp;


    public UnityEvent startButtonDownEvent;
    public UnityEvent startButtonPressedEvent;
    public UnityEvent startButtonUpEvent;


    public bool StartButtonDown { get => startButtonDown; }
    public bool StartButtonPressed { get => startButtonPressed; }
    public bool StartButtonUp { get => startButtonUp; }


    private bool startButtonDown;
    private bool startButtonPressed;
    private bool startButtonUp;


    public UnityEvent goPreviousButtonDownEvent;
    public UnityEvent goPreviousButtonPressedEvent;
    public UnityEvent goPreviousButtonUpEvent;


    public bool GoPreviousButtonDown { get => goPreviousButtonDown; }
    public bool GoPreviousButtonPressed { get => goPreviousButtonPressed; }
    public bool GoPreviousButtonUp { get => goPreviousButtonUp; }



    private bool goPreviousButtonDown;
    private bool goPreviousButtonPressed;
    private bool goPreviousButtonUp;


    public UnityEvent repeatButtonDownEvent;
    public UnityEvent repeatButtonPressedEvent;
    public UnityEvent repeatButtonUpEvent;


    public bool RepeatButtonDown { get => repeatButtonDown; }
    public bool RepeatButtonPressed { get => repeatButtonPressed; }
    public bool RepeatButtonUp { get => repeatButtonUp; }


    private bool repeatButtonDown;
    private bool repeatButtonPressed;
    private bool repeatButtonUp;


    public UnityEvent escapeButtonDownEvent;
    public UnityEvent escapeButtonPressedEvent;
    public UnityEvent escapeButtonUpEvent;


    public bool EscapeButtonDown { get => escapeButtonDown; }
    public bool EscapeButtonPressed { get => escapeButtonPressed; }
    public bool EscapeButtonUp { get => escapeButtonUp; }


    private bool escapeButtonDown;
    private bool escapeButtonPressed;
    private bool escapeButtonUp;


    public UnityEvent teleportButtonDownEvent;
    public UnityEvent teleportButtonPressedEvent;
    public UnityEvent teleportButtonUpEvent;


    public bool TeleportButtonDown { get => teleportButtonDown; }
    public bool TeleportButtonPressed { get => teleportButtonPressed; }
    public bool TeleportButtonUp { get => teleportButtonUp; }


    private bool teleportButtonDown;
    private bool teleportButtonPressed;
    private bool teleportButtonUp;


    public void Awake()
    {
        currentXRInputEvents = this;
    }

    public void onTeleportAction(InputAction.CallbackContext inputAction)
    {
        if (inputAction.started)
        {
            teleportButtonPressedEvent.Invoke();
            teleportButtonDown = true;
            teleportButtonPressed = true;
        }
        if (inputAction.canceled)
        {
            teleportButtonPressed = false;
            teleportButtonUp = true;
            teleportButtonUpEvent.Invoke();
        }
    }

    public void onTranslationPressed(InputAction.CallbackContext inputAction)
    {
        if (inputAction.started)
        {
            translationButtonPressedEvent.Invoke();
            translationButtonDown = true;
            translationButtonPressed = true;
        }
        if (inputAction.canceled)
        {
            translationButtonPressed = false;
            translationButtonUp = true;
            translationButtonUpEvent.Invoke();
        }

        translationVector2Value = inputAction.ReadValue<Vector2>();
    }


    public void onRotationPressed(InputAction.CallbackContext inputAction)
    {
        if (inputAction.started)
        {
            rotationButtonPressedEvent.Invoke();
            rotationButtonDown = true;
            rotationButtonPressed = true;
        }
        if (inputAction.canceled)
        {
            rotationButtonPressed = false;
            rotationButtonUp = true;
            rotationButtonUpEvent.Invoke();
        }

        rotationVector2Value = inputAction.ReadValue<Vector2>();
    }



    public void onGoNextButtonPressed(InputAction.CallbackContext inputAction)
    {
        if (inputAction.started)
        {
            goNextButtonDownEvent.Invoke();
            goNextButtonDown = true;
            goNextButtonPressed = true;
        }
        if (inputAction.canceled)
        {
            goNextButtonPressed = false;
            goNextButtonUp = true;
            goNextButtonUpEvent.Invoke();
        }
    }

    public void onGoPreviousButtonPressed(InputAction.CallbackContext inputAction)
    {
        if (inputAction.started)
        {
            goPreviousButtonDownEvent.Invoke();
            goPreviousButtonDown = true;
            goPreviousButtonPressed = true;
        }
        if (inputAction.canceled)
        {
            goPreviousButtonPressed = false;
            goPreviousButtonUp = true;
            goPreviousButtonUpEvent.Invoke();
        }
    }


    public void onRepeatButtonPressed(InputAction.CallbackContext inputAction)
    {
        if (inputAction.started)
        {
            repeatButtonDownEvent.Invoke();
            repeatButtonDown = true;
            repeatButtonPressed = true;
        }
        if (inputAction.canceled)
        {
            repeatButtonPressed = false;
            repeatButtonUp = true;
            repeatButtonUpEvent.Invoke();
        }
    }

    public void onEscapeButtonPressed(InputAction.CallbackContext inputAction)
    {
        if (inputAction.started)
        {
            escapeButtonDownEvent.Invoke();
            escapeButtonDown = true;
            escapeButtonPressed = true;
        }
        if (inputAction.canceled)
        {
            escapeButtonPressed = false;
            escapeButtonUp = true;
            escapeButtonUpEvent.Invoke();
        }
    }

    public void onStartButtonPressed(InputAction.CallbackContext inputAction)
    {
        if (inputAction.started)
        {
            startButtonDownEvent.Invoke();
            startButtonDown = true;
            startButtonPressed = true;
        }
        if (inputAction.canceled)
        {
            startButtonPressed = false;
            startButtonUp = true;
            startButtonUpEvent.Invoke();
        }
    }

    public void LateUpdate()
    {

        if (translationButtonDown)
        {
            translationButtonDown = false;
        }

        if (translationButtonPressed)
        {
            translationButtonPressedEvent.Invoke();
        }

        if (translationButtonUp)
        {
            translationButtonUp = false;
        }


        if (rotationButtonDown)
        {
            rotationButtonDown = false;
        }

        if (rotationButtonPressed)
        {
            rotationButtonPressedEvent.Invoke();
        }

        if (rotationButtonUp)
        {
            rotationButtonUp = false;
        }



        if (goNextButtonDown)
        {
            goNextButtonDown = false;
        }

        if (goNextButtonPressed)
        {
            goNextButtonPressedEvent.Invoke();
        }

        if (goNextButtonUp)
        {
            goNextButtonUp = false;
        }



        if (goPreviousButtonDown)
        {
            goPreviousButtonDown = false;
        }

        if (goPreviousButtonPressed)
        {
            goPreviousButtonPressedEvent.Invoke();
        }

        if (goPreviousButtonUp)
        {
            goPreviousButtonUp = false;
        }



        if (repeatButtonDown)
        {
            repeatButtonDown = false;
        }

        if (repeatButtonPressed)
        {
            repeatButtonPressedEvent.Invoke();
        }

        if (repeatButtonUp)
        {
            repeatButtonUp = false;
        }



        if (startButtonDown)
        {
            startButtonDown = false;
        }

        if (startButtonPressed)
        {
            startButtonPressedEvent.Invoke();
        }

        if (startButtonUp)
        {
            startButtonUp = false;
        }


        if (escapeButtonDown)
        {
            escapeButtonDown = false;
        }

        if (escapeButtonPressed)
        {
            escapeButtonPressedEvent.Invoke();
        }

        if (escapeButtonUp)
        {
            escapeButtonUp = false;
        }

        if (teleportButtonDown)
        {
            teleportButtonDown = false;
        }

        if (teleportButtonPressed)
        {
            teleportButtonPressedEvent.Invoke();
        }

        if (teleportButtonUp)
        {
            teleportButtonUp = false;
        }
    }
}