using UnityEditor;
using UnityEngine;

namespace CrowdMP.Core
{
    public class XRInteractionsTest : MonoBehaviour
    {
        /*public void Start()
         {
             XRInputEvents.currentXRInputEvents.leftTriggerDownEvent.AddListener(LeftTriggerDownCatchEvent);
             XRInputEvents.currentXRInputEvents.leftTriggerUpEvent.AddListener(LeftTriggerUpCatchEvent);
             XRInputEvents.currentXRInputEvents.leftTriggerPressedEvent.AddListener(LeftTriggerPressedCatchEvent);
         }
         public void LeftTriggerDownCatchEvent()
         {
             Debug.Log("down event");
         }
         public void LeftTriggerUpCatchEvent()
         {
             Debug.Log("up event");
         }

         public void LeftTriggerPressedCatchEvent()
         {
             Debug.Log("pressed event");
         }*/

        void curvedRaycast(int iterations, Vector3 startPos, Ray ray, int velocity)
        {
            RaycastHit hit;
            Vector3 pos = startPos;
            var slicedGravity = Physics.gravity.y / iterations / velocity;
            Ray ray2 = new Ray(ray.origin, ray.direction);
            print(slicedGravity);
            for (int i = 0; i < iterations; i++)
            {
                if (Physics.Raycast(pos, ray2.direction * velocity, out hit, velocity))
                {
                    Debug.DrawRay(pos, ray2.direction * hit.distance, Color.green);
                    if (hit.transform.tag == "Permeable")
                    {
                        Debug.DrawRay(pos, ray2.direction * velocity, Color.green);
                        pos += ray2.direction * velocity;
                        ray2 = new Ray(ray2.origin, ray2.direction + new Vector3(0, slicedGravity, 0));
                        for (int x = 0; x < iterations; x++)
                        {
                            if (Physics.Raycast(pos, ray2.direction * velocity, out hit, velocity))
                            {
                                Debug.DrawRay(pos, ray2.direction * hit.distance, Color.yellow);
                                return;
                            }
                            Debug.DrawRay(pos, ray2.direction * velocity, Color.magenta);
                            pos += ray2.direction * velocity;
                            ray2 = new Ray(ray2.origin, ray2.direction + new Vector3(0, slicedGravity, 0));
                        }
                    }
                    else
                    {
                        return;
                    }
                }
                Debug.DrawRay(pos, ray2.direction * velocity, Color.cyan);
                pos += ray2.direction * velocity;
                ray2 = new Ray(ray2.origin, ray2.direction + new Vector3(0, slicedGravity, 0));
            }
            Debug.DrawRay(startPos, pos, Color.red);
            /*for (int i = 0; i < iterations; i++)
            {
                Debug.DrawRay(pos, ray2.direction * velocity, Color.red);
                pos += ray2.direction * velocity;
                ray2 = new Ray(ray2.origin, ray2.direction + new Vector3(0, slicedGravity, 0));
            }*/
        }

        Vector3[] GravCast(Vector3 startPos, Vector3 direction, int killAfter, int velocity, bool reflect)
        {
            RaycastHit hit;
            Vector3[] vectors = new Vector3[killAfter];
            Ray ray = new Ray(startPos, direction);
            for (int i = 0; i < killAfter; i++)
            {
                if (Physics.Raycast(ray, out hit, 1f))
                {
                    if (reflect)
                    {
                        print(hit.normal);
                        /*for (int e = 0; e < killAfter; e++)
                        {
                            if (Physics.Raycast(ray, out hit, 1f))
                            {
                                return vectors;
                            }
                            ray = new Ray(ray.origin + ray.direction, ray.direction + (Physics.gravity / killAfter / velocity));
                        }*/
                    }
                    return vectors;
                }
                Debug.DrawRay(ray.origin, ray.direction, Color.blue);
                ray = new Ray(ray.origin + ray.direction, ray.direction + (Physics.gravity / killAfter / velocity));
                vectors[i] = ray.origin;

            }
            return vectors;
        }
    




    // Update is called once per frame
    void Update()
        {
            if (XRInputEvents.currentXRInputEvents.TeleportButtonPressed)
            {
                // do something
                Debug.LogError("right trigger pressed");

               

            }

            /*if (XRInputEvents.currentXRInputEvents.GoNextButtonDown)
            {
                // do something
                Debug.LogError("GoNextButtonDown");
            }
            if (XRInputEvents.currentXRInputEvents.GoNextButtonPressed)
            {
                // do something
                Debug.LogError("GoNextButtonPressed");
            }
            if (XRInputEvents.currentXRInputEvents.GoNextButtonUp)
            {
                // do something
                Debug.LogError("GoNextButtonUp");
            }


            if (XRInputEvents.currentXRInputEvents.LeftTriggerDown)
            {
                // do something
                Debug.LogError("down left");
            }
            if (XRInputEvents.currentXRInputEvents.LeftTriggerPressed)
            {
                // do something
                Debug.LogError("pressed left");
            }
            if (XRInputEvents.currentXRInputEvents.LeftTriggerUp)
            {
                // do something
                Debug.LogError("up left");
            }

            if (XRInputEvents.currentXRInputEvents.RightTriggerDown)
            {
                // do something
                Debug.LogError("down right");
            }
            if (XRInputEvents.currentXRInputEvents.RightTriggerPressed)
            {
                // do something
                Debug.LogError("pressed right");
            }
            if (XRInputEvents.currentXRInputEvents.RightTriggerUp)
            {
                // do something
                Debug.LogError("up right");
            }




            if (XRInputEvents.currentXRInputEvents.LeftTrackedPadPressed)
            {
                // do something
                Debug.LogError("left tracker pad pressed");
            }*/
        }
    }
}
