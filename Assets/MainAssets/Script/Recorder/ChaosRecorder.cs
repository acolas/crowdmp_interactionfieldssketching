﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace CrowdMP.Core
{
    /// <summary>
    /// Recorder recording basic data (Spatial data of agents and player as well as player inputs)
    /// </summary>
    public class ChaosRecorder : MonoBehaviour, Recorder
    {

        private Player player;
        private List<Agent> agents;
        private List<ToolsOutput> recordingObject;
        private GameObject playerCam;


        /// <summary>
        /// Reset the recorder
        /// </summary>
        public void clear()
        {
            if (recordingObject != null)
            {
                recordingObject = null;
            }
            if (agents != null)
            {
                agents.Clear();
                agents = null;
            }

        }

        /// <summary>
        /// Initialize the recorder
        /// </summary>
        /// <param name="agentList"> The list of agents to watch </param>
        public void initRecorder(Player p, List<Agent> agentList)
        {
            player = p;
            playerCam = GameObject.FindGameObjectWithTag("MainCamera");


            string dirPrototype = LoaderConfig.sceneOutputFile;
            if (dirPrototype == "")
            {
                recordingObject = null;
                return;
            }
            string path = Path.Combine(Path.Combine(LoaderConfig.dataPath, LoaderConfig.RecFolder), dirPrototype);
            path = path.Replace("{USER}", LoaderConfig.xpCurrentUser.ToString());
            path = path.Replace("{ITT}", LoaderConfig.xpCurrentTrial.ToString());

            if (!System.IO.Directory.Exists(path))
                System.IO.Directory.CreateDirectory(path);

            recordingObject = new List<ToolsOutput>();
            // Create data files
            for (int i = 0; i < agentList.Count; i++)
            {

                string currFile = Path.Combine(path, agentList[i].id.ToString("D" + 4) + ".csv");
                recordingObject.Add(new ToolsOutput(currFile));

            }

            // Save agents list
            agents = new List<Agent>(agentList);
        }

        /// <summary>
        /// Add new agent to record position
        /// </summary>
        /// <param name="agent">the new agent to add</param>
        public void addAgent(Agent agent)
        {
            string dirPrototype = LoaderConfig.sceneOutputFile;
            if (dirPrototype == "")
            {
                recordingObject = null;
                return;
            }
            string path = Path.Combine(Path.Combine(LoaderConfig.dataPath, LoaderConfig.RecFolder), dirPrototype);
            path = path.Replace("{USER}", LoaderConfig.xpCurrentUser.ToString());
            path = path.Replace("{ITT}", LoaderConfig.xpCurrentTrial.ToString());
            uint i = agent.id;
            string currFile = Path.Combine(path, i.ToString("D" + 4) + ".csv");
            
            agents.Add(agent);
            recordingObject.Add(new ToolsOutput(currFile));
        }

        /// <summary>
        /// Removed killed agent to record position
        /// </summary>
        /// <param name="agent">the new agent to add</param>
        public void removeAgent(Agent agent)
        {
            recordingObject.RemoveAt(agents.IndexOf(agent));
            agents.Remove(agent);
        }

        // Use this for initialization
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {

        }

        /// <summary>
        /// Save data during LateUpdate when all movement as be done (done during the update)
        /// </summary>
        void LateUpdate()
        {
            if (ToolsTime.DeltaTime != 0 && recordingObject != null)
            {
                /* Store position virtual human */
                int i = 0;
                foreach (Agent a in agents)
                {
                    string dataText = ToolsTime.TrialTime.ToString().Replace(".", LoaderConfig.RecDecimalSeparator).Replace(",", LoaderConfig.RecDecimalSeparator) +
                                        LoaderConfig.RecDataSeparator +
                                       a.gameObject.transform.position.x.ToString().Replace(".", LoaderConfig.RecDecimalSeparator).Replace(",", LoaderConfig.RecDecimalSeparator) +
                                       LoaderConfig.RecDataSeparator +
                                       a.gameObject.transform.position.z.ToString().Replace(".", LoaderConfig.RecDecimalSeparator).Replace(",", LoaderConfig.RecDecimalSeparator);
                    recordingObject[i].writeLine(dataText);
                    i++;
                }
            }
        }
    }
}
