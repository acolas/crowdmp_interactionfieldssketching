﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrowdMP.Core
{
    /// <summary>
    /// Convert the player input device's data to text for recording
    /// </summary>
    public class DataDeviceState : RecorderData
    {

        public string getAgentData(Agent a)
        {
            return "";
        }

        public string getAgentHeader(Agent a)
        {
            return "";
        }

        public string getOtherData()
        {
            string dataText =   /* Store value of VAxis and Haxis */
                                LoaderConfig.RecDataSeparator +
                                ToolsInput.getAxisValue(ToolsAxis.Vertical).ToString().Replace(".", LoaderConfig.RecDecimalSeparator) +
                                LoaderConfig.RecDataSeparator +
                                ToolsInput.getAxisValue(ToolsAxis.Horizontal).ToString().Replace(".", LoaderConfig.RecDecimalSeparator);
            return dataText;
        }

        public string getOtherHeader()
        {
            string dataText =   /* Store value of VAxis and Haxis */
                                LoaderConfig.RecDataSeparator +
                                "Input Vertical Axis" +
                                LoaderConfig.RecDataSeparator +
                                "Input Horizontal Axis";
            return dataText;
        }

        public string getPlayerData(Player p)
        {
            return "";
        }

        public string getPlayerHeader(Player p)
        {
            return "";
        }

        public void initialize()
        {

        }
    }
}