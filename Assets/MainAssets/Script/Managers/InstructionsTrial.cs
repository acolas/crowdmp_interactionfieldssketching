﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CrowdMP.Core
{

    /// <summary>
    /// Trial Manager for Instructions
    /// </summary>
    public class InstructionsTrial : MonoBehaviour, TrialManager
    {

        /// <summary>
        /// List the instruction to show to the users in the correct order
        /// </summary>
        public GameObject[] instructionsList;
        int currentInstuction;
        Player player;

        /// <summary>
        /// Reset the trial manager
        /// </summary>
        public void clear()
        {
            player.clear();
            //player.gameObject.SetActive(false);
            Destroy(player);
        }

        /// <summary>
        /// Perform a trial step
        /// </summary>
        public void doStep()
        {
            if (ToolsTime.DeltaTime == 0)
                return;

            player.doStep();
        }

        /// <summary>
        /// Check ending conditions
        /// </summary>
        /// <returns>True if the trial is over</returns>
        public bool hasEnded()
        {
            return !(currentInstuction < instructionsList.Length);
        }

        /// <summary>
        /// Initialize trial
        /// </summary>
        /// <param name="p">The player</param>
        /// <param name="agentModels">All the available agent models</param>
        /// <returns>True if the trial has been correctly intialized</returns>
        public bool initializeTrial(GameObject[] playersModel, GameObject[] agentModels)
        {

            // Setup the player
            foreach (GameObject p in playersModel)
            {

                if (p.name == LoaderConfig.playerInfo.mesh)
                {
                    player = LoaderConfig.playerInfo.createPlayerComponnent(p, 0);
                    p.SetActive(true);
                    break;
                }
            }

            currentInstuction = 0;

            if (instructionsList.Length > 0)
            {
                foreach (GameObject instruction in instructionsList)
                    instruction.SetActive(false);

                instructionsList[currentInstuction].SetActive(true);
            }
            else
            {
                currentInstuction = 1;
            }

            //ToolsTime.pauseAndResumeGame(false);


            Canvas c = GetComponentInChildren<Canvas>();
            if (c != null)
            {
                /*float height = c.gameObject.GetComponent<RectTransform>().rect.height / c.gameObject.GetComponent<RectTransform>().localScale.y;
                float width = c.gameObject.GetComponent<RectTransform>().rect.width / c.gameObject.GetComponent<RectTransform>().localScale.x;

                c.renderMode = RenderMode.ScreenSpaceCamera;
                CanvasScaler scaler = c.gameObject.GetComponent<CanvasScaler>();
                scaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;

                scaler.referenceResolution = new Vector2(width, height);
                scaler.screenMatchMode = CanvasScaler.ScreenMatchMode.Expand;
                */
                CanvasScaler scaler = c.gameObject.GetComponent<CanvasScaler>();

                if (scaler != null)
                {
                    scaler.enabled = true;
                }
            }
            



            return true;
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        protected virtual void LateUpdate()
        {
            if (XRInputEvents.currentXRInputEvents.StartButtonDown)
            {
                Canvas c = GetComponentInChildren<Canvas>();
                if (c != null)
                {
                    c.enabled = false;
                    GameObject gm= GameObject.FindGameObjectWithTag("GameManager");
                    if (gm != null)
                    {
                        MainManager mm = gm.GetComponent<MainManager>();
                        if (mm != null)
                        {
                            mm.endTrial();
                        }
                    }
                }
            }
        }

        /*/// <summary>
        /// Handle button/key input in LateUpdate once all trial steps are over
        /// </summary>
        private void LateUpdate()
        {
            if (ToolsInput.GetButtonDown(ButtonCode.Fire5) || ToolsInput.GetKeyDown(KeyCode.RightArrow))
            {
                instructionsList[currentInstuction].SetActive(false);

                currentInstuction++;
                if (currentInstuction < instructionsList.Length)
                    instructionsList[currentInstuction].SetActive(true);
            }
            else if (ToolsInput.GetButtonDown(ButtonCode.Fire4) || ToolsInput.GetKeyDown(KeyCode.LeftArrow))
            {
                instructionsList[currentInstuction].SetActive(false);

                currentInstuction = Mathf.Max(0, currentInstuction - 1);
                instructionsList[currentInstuction].SetActive(true);
            }

        }*/

        public Player getPlayer()
        {
            return player;
        }

        public virtual bool isReady()
        {
            return true;
        }
        public virtual void startTrial()
        {


        }
    }
}