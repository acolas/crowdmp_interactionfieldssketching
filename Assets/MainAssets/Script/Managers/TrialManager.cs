﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrowdMP.Core
{
    /// <summary>
    /// Trial Manager interface to create classes that control a trial
    /// </summary>
    public interface TrialManager
    {

        /// <summary>
        /// Initialize trial
        /// </summary>
        /// <param name="p">The player</param>
        /// <param name="agentModels">All the available agent models</param>
        /// <returns>True if the trial has been correctly intialized</returns>
        bool initializeTrial(GameObject[] Player, GameObject[] agentModels);
        /// <summary>
        /// Check the ending conditions of the trials
        /// </summary>
        /// <returns>True if the trial is over</returns>
        bool hasEnded();
        /// <summary>
        /// Reset trial
        /// </summary>
        void clear();
        /// <summary>
        /// Perform a step of the trial
        /// </summary>
        void doStep();
        /// <summary>
        /// Return the player of the trial
        /// </summary>
        Player getPlayer();

        bool isReady();
        void startTrial();
    }

    /// <summary>
    /// Trial parameters concerning the player's input device (XML serializable)
    /// </summary>
    public interface TrialConfig
    {
    }
}