﻿    /*  CrowdMP - Platform to design virtual experiment with reactive crowd
    **  MIT License
    **  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
    **
    **  Permission is hereby granted, free of charge, to any person obtaining 
    **  a copy of this software and associated documentation files (the 
    **  "Software"), to deal in the Software without restriction, including 
    **  without limitation the rights to use, copy, modify, merge, publish, 
    **  distribute, sublicense, and/or sell copies of the Software, and to 
    **  permit persons to whom the Software is furnished to do so, subject 
    **  to the following conditions:
    **
    **  The above copyright notice and this permission notice shall be 
    **  included in all copies or substantial portions of the Software.
    **
    **  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
    **  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
    **  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
    **  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
    **  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
    **  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
    **  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
    **  SOFTWARE.
    **  
    **  Authors: Julien Bruneau
    **  Contact: crowd_group@inria.fr
    */

    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;

    namespace CrowdMP.Core
    {

        /// <summary>
        /// The main manager controlling the Experiment flow
        /// </summary>
        public class MainManager : MonoBehaviour
        {

            public string configPath = "./config.xml";
            public bool noTransition = false;
            public bool sketchBreak = false;

            protected bool trialStarted;
            protected bool isEndXP;

            protected Player player;
            protected GameObject[] sceneObjectsModels;
            protected GameObject[] agentModels;
            protected GameObject[] playerModels;
            protected GameObject sceneGameObject;

            protected GameObject transitionSign;
            protected string transitionText;
            protected GameObject endSign;

            protected TrialManager currentTrialManager;

            protected void Awake()
            {
                UnityEngine.Random.InitState(454682);
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                if (!LoaderConfig.loadConfig(configPath))
                {
                    ToolsDebug.logFatalError("Config file not found at "+ configPath);
                    CleanQuit();
                }
            }

            // Use this for initialization
            protected virtual void Start()
            {
                if (!initializeConstants())
                {
                    ToolsDebug.logFatalError("Error on GUIManager constants initialisation");
                    CleanQuit();
                }
                isEndXP = false;
                startTrial();
                startTransition();
                if (noTransition)
                {
                    if (!trialStarted)
                    {
                        trialStarted = true;
                        currentTrialManager.startTrial();
                    }

                    if (isEndXP)
                        CleanQuit();
                    else
                        transitionSign.SetActive(ToolsTime.tooglePause());
                }
            }

            /// <summary>
            /// Load and setup current trial
            /// </summary>
            protected virtual void startTrial()
            {
                // Load trial file
                if (!LoaderConfig.LoadXP())
                {
                    ToolsDebug.log("End of the trials", 2);
                    isEndXP = true;
                    return;
                }

                // Load the trial scene and unload the others
                string sceneMeshName = LoaderConfig.sceneName;
                foreach (GameObject currentGameObject in sceneObjectsModels)
                {
                    /*Make every scene disapered exept the one given by the scenarioLoader*/
                    if (currentGameObject.name == sceneMeshName)
                    {
                        currentGameObject.SetActive(true);

                        if (currentGameObject.GetComponent<TrialManager>() == null)
                        {
                            ToolsDebug.logFatalError("The trial needs a Manager");
                            return;
                        }


                        sceneGameObject = currentGameObject;
                        sceneGameObject.transform.position = LoaderConfig.scenePosition;
                        sceneGameObject.transform.rotation = Quaternion.Euler(LoaderConfig.sceneRotation);
                        break;
                    }
                }

                if (sceneGameObject == null)
                    ToolsDebug.logFatalError("Scene not found (" + sceneMeshName + ")");

                // Reset trial time
                ToolsTime.newLevel();

                // Initialize the trial manager
                currentTrialManager = sceneGameObject.GetComponent<TrialManager>();
                if (currentTrialManager == null)
                {
                    ToolsDebug.logFatalError("Trials " + LoaderConfig.xpCurrentTrial.ToString() + " does not have a manager");
                    CleanQuit();
                }
                currentTrialManager.initializeTrial(playerModels, agentModels);
                player = currentTrialManager.getPlayer();

                trialStarted = false;
            }

            /// <summary>
            /// Method used to initialize scene constants
            /// </summary>
            /// <returns>True if constant are well initialized, otherwise False</returns>
            protected virtual bool initializeConstants()
            {
                playerModels = GameObject.FindGameObjectsWithTag("Player");
                foreach (GameObject p in playerModels)
                {
                    p.SetActive(false);
                }
                sceneObjectsModels = GameObject.FindGameObjectsWithTag("Stage");
                foreach (GameObject scene in sceneObjectsModels)
                {
                    scene.SetActive(false);
                }

                agentModels = GameObject.FindGameObjectsWithTag("AgentModels");
                foreach (GameObject go in agentModels)
                {
                    go.SetActive(false);
                }
            
                Canvas[] signs = GetComponentsInChildren<Canvas>();
                foreach (Canvas sign in signs)
                {
                    if (sign.gameObject.name == "Transition")
                    {
                        transitionSign = sign.gameObject;
                        transitionText = transitionSign.GetComponentInChildren<Text>().text;
                        CanvasScaler scaler = sign.gameObject.GetComponent<CanvasScaler>();

                        if (scaler != null)
                        {
                            scaler.enabled = true;
                        }
                        //// Place in front of camera
                        //transitionSign.transform.SetParent(playerHead.transform);
                        //transitionSign.transform.position = playerHead.transform.position + playerHead.transform.forward * 2;
                        //transitionSign.transform.rotation = playerHead.transform.rotation;
                    }
                    if (sign.gameObject.name == "End")
                    {
                        endSign = sign.gameObject;
                        CanvasScaler scaler = sign.gameObject.GetComponent<CanvasScaler>();

                        if (scaler != null)
                        {
                            scaler.enabled = true;
                        }
                        // Place in front of camera
                        //endSign.transform.SetParent(playerHead.transform);
                        //endSign.transform.position = playerHead.transform.position + playerHead.transform.forward * 2;
                        //endSign.transform.rotation = playerHead.transform.rotation;

                    }

                    sign.gameObject.SetActive(false);
                }

                PluginManager[] plugins = gameObject.GetComponentsInChildren<PluginManager>();
                foreach (PluginManager plugin in plugins)
                {
                    plugin.LoadPlugin();
                }

                return true;
            }

            // Update is called once per frame
            protected virtual void Update()
            {
                ToolsTime.updateTime();

                if (currentTrialManager == null)
                {
                return;
                }


                if (currentTrialManager.hasEnded())
                {
                    endTrial();
                }

                else
                {
                    if (!sketchBreak)
                    {
                        currentTrialManager.doStep();
                    }

                    
                }


            }

            // Handle key/button input in LateUpdate
            protected virtual void LateUpdate()
            {
                if ((currentTrialManager == null || currentTrialManager.isReady()) 
                && (XRInputEvents.currentXRInputEvents.StartButtonDown))
                {
                    if (isEndXP)
                        CleanQuit();
                    else
                    {
                        if (!trialStarted)
                        {
                            trialStarted = true;
                            currentTrialManager.startTrial();
                        }
                        transitionSign.SetActive(false);
                        ToolsTime.tooglePause();
                     }
                }

                if (XRInputEvents.currentXRInputEvents.GoNextButtonDown)
                {
                    endTrial();
                }

                if (!isEndXP && XRInputEvents.currentXRInputEvents.GoPreviousButtonDown)
                {
                    endTrial(-1);
                }
            
                if (XRInputEvents.currentXRInputEvents.RepeatButtonDown)
                {
                    endTrial(0);
                }

                if (XRInputEvents.currentXRInputEvents.EscapeButtonDown)
                {
                    CleanQuit();
                }


            }

            /// <summary>
            /// End the current trial change it
            /// </summary>
            public virtual void endTrial(int trialSwitch = 1)
            {
                if (isEndXP)
                {
                    if (trialSwitch < 0)
                        isEndXP = false;
                    else
                        return;
                } else {
                    currentTrialManager.clear();
                    sceneGameObject.gameObject.SetActive(false);
                    sceneGameObject = null;
                    currentTrialManager = null;
                }

                LoaderConfig.ChangeTrial(trialSwitch);
                startTrial();
                startTransition();
                if (noTransition)
                {
                    if (!trialStarted)
                    {
                        trialStarted = true;
                        currentTrialManager.startTrial();
                    }

                    if (isEndXP)
                        CleanQuit();
                    else
                        transitionSign.SetActive(ToolsTime.tooglePause());
                }
            }

            /// <summary>
            /// Update and load transition screen
            /// </summary>
            protected virtual void startTransition()
            {
                if (isEndXP)
                {
                    if (player != null)
                    {
                        player.gameObject.SetActive(true);
                    }
                    if (endSign != null)
                    {
                        endSign.gameObject.SetActive(true);
                    }
                    return;
                }

                if (player == null)
                {
                    Debug.LogError("Player present in xml for the trial does not exist in scene hierarchy");
                    return;
                }

                GameObject playerHead = player.getHeadObject();
                // Place in front of camera
                transitionSign.transform.SetParent(playerHead.transform);
                transitionSign.transform.position = playerHead.transform.position + playerHead.transform.forward * 2;
                transitionSign.transform.rotation = playerHead.transform.rotation;

                endSign.transform.SetParent(playerHead.transform);
                endSign.transform.position = playerHead.transform.position + playerHead.transform.forward * 2;
                endSign.transform.rotation = playerHead.transform.rotation;


                transitionSign.GetComponentInChildren<Text>().text = transitionText;
                transitionSign.GetComponentInChildren<Text>().text = transitionSign.GetComponentInChildren<Text>().text.Replace("{ITT}", (LoaderConfig.xpCurrentTrial + 1).ToString());
                transitionSign.GetComponentInChildren<Text>().text = transitionSign.GetComponentInChildren<Text>().text.Replace("{TOT}", LoaderConfig.xpMaxTrial.ToString());

                transitionSign.SetActive(true);
            }
            protected void CleanQuit()
            {
                PluginManager[] plugins = gameObject.GetComponentsInChildren<PluginManager>();
                foreach (PluginManager plugin in plugins)
                {
                    plugin.UnloadPlugin();
                }

                #if UNITY_EDITOR
                    UnityEditor.EditorApplication.isPlaying = false;
                #else
                    Application.Quit();
                #endif
            }
        }

    }