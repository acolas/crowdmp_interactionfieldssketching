﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrowdMP.Core
{
    public class AgentGen : MonoBehaviour
    {
        [Tooltip("Possible meshes for agents, will be selected randomly during spawn")]
        public GameObject[] meshes;
        [Tooltip("animOffset parameters of the agents (-1 for random)")]
        public float animOffset = -1f;
        [Tooltip("visualVariation parameters of the agents (-1 for random)")]
        public int visualVariation = -1;
        [Tooltip("heightVariation parameters of the agents (-1 for random)")]
        public float heightVariation = -1;
        [Tooltip("Radius of the agent")]
        public float radius = 0.33f;

        private int totalNumberOfVariations = 10;
        private float heightVariationValue = 0.05f;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void defaultMeshes()
        {
            meshes = GameObject.FindGameObjectsWithTag("AgentModels");
        }

        public virtual AgentGen randDraw(GameObject agent)
        {
            if (meshes.Length == 0)
            {
                Debug.LogError("No meshes registred in spawner(s), fix this before trying to spawn agents");
                return null;
            }

            AgentGen newAgentParam = agent.AddComponent<AgentGen>();
            newAgentParam.meshes = new GameObject[1];

            int randomPositionInArray = Random.Range(0, meshes.Length);
            newAgentParam.meshes[0] = meshes[randomPositionInArray];
    

            if (animOffset < 0)
                newAgentParam.animOffset = Random.Range(0f, 1f);
            else
                newAgentParam.animOffset = animOffset;

            if (visualVariation < 0)
                newAgentParam.visualVariation = Random.Range(0, totalNumberOfVariations);
            else
                newAgentParam.visualVariation = visualVariation;

            if (heightVariation < 0)
                newAgentParam.heightVariation = Random.Range(-heightVariationValue, heightVariationValue);
            else
                newAgentParam.heightVariation = heightVariation;

            newAgentParam.radius = radius;

            return newAgentParam;
        }

        public virtual TrialAgent createAgent(int seedGroup)
        {
            TrialRegularAgent agent = new TrialRegularAgent();

            agent.mesh = meshes[Random.Range(0, meshes.Length)].name;

            if (animOffset < 0)
                agent.animationOffset = Random.Range(0f, 1f);
            else
                agent.animationOffset = animOffset;

            if (visualVariation < 0)
                agent.visualVariation = Random.Range(0, 10);
            else
                agent.visualVariation = visualVariation;

            agent.heightOffset = heightVariation;

            agent.radius = radius;


            agent.Position.vect = gameObject.transform.position;
            agent.Rotation.vect = gameObject.transform.rotation.eulerAngles;

            ControlLawGen lawGen = null;
            ControlLawGen[] tmpLG = gameObject.GetComponents<ControlLawGen>();
            foreach (ControlLawGen lg in tmpLG)
                if (lg.enabled)
                {
                    lawGen = lg;
                    break;
                }
            if (lawGen != null)
                agent.xmlControlLaw = lawGen.createControlLaw();

            ControlSimGen simGen = null;
            ControlSimGen[] tmpSG = gameObject.GetComponents<ControlSimGen>();
            foreach (ControlSimGen sg in tmpSG)
                if (sg.enabled)
                {
                    simGen = sg;
                }
            if (simGen != null)
                agent.xmlControlSim = simGen.createControlSim(seedGroup);

            return agent;
        }
    }
}
