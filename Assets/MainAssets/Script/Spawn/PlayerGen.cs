﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrowdMP.Core
{
    public class PlayerGen : MonoBehaviour
    {
        [Tooltip("Name of the player's mesh")]
        public GameObject playerMesh; // = "Player";
        [Tooltip("Radius of the player during the trial")]
        public float playerRadius = 0.33f;
        //public Vector3 playerPosition = new Vector3();
        //public Vector3 playerRotation = new Vector3();

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public virtual TrialPlayer createPlayer()
        {
            TrialRegularPlayer player = new TrialRegularPlayer();
            player.mesh = playerMesh.name;
            player.radius = playerRadius;
            player.Position.vect = gameObject.transform.position;
            player.Rotation.vect = gameObject.transform.rotation.eulerAngles;

            ControlLawGen lawGen = gameObject.GetComponent<ControlLawGen>();
            if (lawGen!=null)
                player.xmlControlLaw = lawGen.createControlLaw();
            else
                player.xmlControlLaw = new LawCamControlEditor();

            player.xmlControlSim = null;

            return player;
        }
    }
}
