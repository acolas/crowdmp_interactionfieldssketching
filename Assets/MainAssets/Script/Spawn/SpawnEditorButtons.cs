﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

#if (UNITY_EDITOR) 
using UnityEngine;
using System.Collections;
using UnityEditor;
namespace CrowdMP.Core
{

    /// <summary>
    /// Editor code to add button to the MainManager script object
    /// </summary>
    [CustomEditor(typeof(Spawn))]
    public class SpawnButtons : Editor
    {

        public override void OnInspectorGUI()
        {
            Spawn myTarget = (Spawn)target;
            DrawDefaultInspector();

            if (GUILayout.Button("Spawn"))
            {
                Undo.RecordObject(target, "Spawn");
                EditorUtility.SetDirty(target);
                myTarget.spawn();
            }
        }
    }

    /// <summary>
    /// Editor code to add button to the MainManager script object
    /// </summary>
    [CustomEditor(typeof(TrialGen))]
    public class TrialGenButtons : Editor
    {

        public override void OnInspectorGUI()
        {
            TrialGen myTarget = (TrialGen)target;
            DrawDefaultInspector();

            if (GUILayout.Button("Generation"))
            {
                myTarget.generate();
            }
            if (GUILayout.Button("SpawnAll"))
            {
                myTarget.spawn();
            }
            if (GUILayout.Button("ClearAll"))
            {
                myTarget.clear();
            }
        }
    }

    [CustomEditor(typeof(AgentGen))]
    public class AgentGenButton : Editor
    {

        public override void OnInspectorGUI()
        {
            AgentGen myTarget = (AgentGen)target;
            DrawDefaultInspector();

            if (GUILayout.Button("Get meshes"))
            {
                Undo.RecordObject(target, "AgentGen");
                EditorUtility.SetDirty(target);
                myTarget.defaultMeshes();
            }
        }
    }
}
#endif
