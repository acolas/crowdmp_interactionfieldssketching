﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using System;
using System.Collections;
using System.Collections.Generic;
#if (UNITY_EDITOR) 
using UnityEditor;
#endif
using UnityEngine;

namespace CrowdMP.Core
{
    public class TrialGen : MonoBehaviour {

        [Tooltip("Path of the generated XML file")]
        public string path = "./trial.xml"; //

        [Tooltip("Scene to use for the trial")]
        public GameObject stage;

        [Tooltip("Name of the output file to save data during the trial")]
        public string recordingFile = "Output_{USER}_{ITT}.csv";

        [Tooltip("The player parameters")]
        public GameObject playerGenerator;

        [Tooltip("Spawners containing the agents to include in the trial")]
        public Spawn[] spawners;

        private void Awake()
        {
            gameObject.SetActive(false);
        }

        // Use this for initialization
        void Start() {
            //gameObject.SetActive(false);
        }

        // Update is called once per frame
        void Update() {

        }

        public void spawn()
        {
            foreach (Spawn s in spawners)
            {
#if (UNITY_EDITOR)
                Undo.RecordObject(s,"Spawn");
                EditorUtility.SetDirty(s);
#endif
                s.spawn();
            }
            Debug.Log("Spawn done");
        }

        public void clear()
        {
            foreach (Spawn s in spawners)
            {
#if (UNITY_EDITOR)
                Undo.RecordObject(s, "ClearSpawn");
                EditorUtility.SetDirty(s);
#endif
                s.clearAll();                
            }
            Debug.Log("Clean done for all spawns");
        }

        public Trial createTrial() {

            Trial newTrial = new Trial();

            // SCENE PARAMETERS
            if (stage ==null)
            {
                Debug.LogError("No stage associated to trial");
                return null;
            }
            else if (stage.tag != "Stage")
            {
                Debug.LogError("Gameobject associated not tagged with Stage tag");
                return null;
            }
            newTrial.scene.meshName = stage.name;
            newTrial.scene.Position.vect = stage.transform.position;
            newTrial.scene.Rotation.vect = stage.transform.rotation.eulerAngles;
            newTrial.scene.recordingFile = recordingFile;

            // PLAYER PARAMETERS
            if (playerGenerator == null)
            {
                Debug.LogError("No player generator associated to trial");
                return null;
            }
            else if (playerGenerator.GetComponent<PlayerGen>()==null)
            {
                Debug.LogError("Gameobject associated to trial does not have PlayerGen script attached");
                return null;
            }

            newTrial.player = playerGenerator.GetComponent<PlayerGen>().createPlayer();

            int seedGroup = 0;
            foreach (Spawn s in spawners)
            {
                AgentGen[] agents = s.getAllAgent();
                foreach (AgentGen a in agents)
                {
                    if (a != null)
                    {
                        TrialAgent ta = a.createAgent(seedGroup);
                        newTrial.agents.Add(ta);
                    }
                }
                seedGroup += agents.Length;

            }

            return newTrial;
        }

        public void generate()
        {
            if (stage == null)
            {
                //ToolsDebug.logError("No scene selected"); toolsdebug not yet initialized here, this is done by manager after

                //Try to log name to get unassigned log error
                Debug.Log(stage.name);
                return;
            }

            if (playerGenerator == null)
            {                
                //Try to log name to get unassigned log error
                Debug.Log(playerGenerator.name);
                return;
            }
                       
            Trial newTrial = createTrial();
            if (newTrial != null)
            {
                if (path.Equals("NoConfigFileNoGenerationNeeded"))
                {
                    Debug.LogWarning("No generation needed in no config file mode");
                    return;
                }
                LoaderXML.CreateXML<Trial>(path, newTrial);
                Debug.Log("Trial generated at " + path);
            }
            else
            {
                Debug.LogError("Abort creation of xml for trial due to previous errors");
            }
        }
    }
}