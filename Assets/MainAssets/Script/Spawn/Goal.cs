﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrowdMP.Core
{
    public class Goal : MonoBehaviour {

        public Goal[] nextPoint;

        // Use this for initialization
        void Start() {

        }

        // Update is called once per frame
        void Update() {

        }

        public Goal getNextPoint(Goal entryPoint)
        {
            if (nextPoint.Length == 0)
                return this;
            if (nextPoint.Length == 1)
                return nextPoint[0];

            int selection = Random.Range(0, nextPoint.Length);
            if (nextPoint[selection] == entryPoint)
                selection = (selection + Random.Range(1, nextPoint.Length)) % nextPoint.Length;

            return nextPoint[selection];
        }


        public Vector3 randPosInArea()
        {
            Vector3 newVec = gameObject.transform.position
                    + gameObject.transform.right * gameObject.transform.lossyScale.x * Random.Range(-0.5f, 0.5f) * 10f
                    + gameObject.transform.forward * gameObject.transform.lossyScale.z * Random.Range(-0.5f, 0.5f) * 10f;

            return newVec;
        }
    }
}
