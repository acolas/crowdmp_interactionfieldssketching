﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrowdMP.Core
{
    public class ControlSimGen_Helbing : ControlSimGen
    {
        [Header("Main Parameters")]
        public int id = 0;
        public float radius = 0.33f;
        public float neighborDist = 10.0f;
        public bool doBoids = false;

        [System.Serializable]
        public class SpawnerParams
        {
            public float neighborDistOffset = 0;
            public float radiusOffset = 0;
        }
        [Header("Spawner Parameters")]
        public SpawnerParams randomness;

        public override CustomXmlSerializer<TrialControlSim> createControlSim(int GroupSeed)
        {
            HelbingConfig sim = new HelbingConfig();
            sim.id = id;
            sim.neighborDist = neighborDist;
            sim.radius = radius;
            sim.doBoids = doBoids;

            return sim;
        }

        public override ControlSimGen randDraw(GameObject agent, int groupID = 0)
        {
            ControlSimGen_Helbing csg = agent.AddComponent<ControlSimGen_Helbing>();


            csg.id = id;
            csg.neighborDist = neighborDist + Random.Range(-randomness.neighborDistOffset, randomness.neighborDistOffset);
            csg.radius = radius + Random.Range(-randomness.radiusOffset, randomness.radiusOffset);
            csg.doBoids = doBoids;

            return csg;
        }
    }
}
