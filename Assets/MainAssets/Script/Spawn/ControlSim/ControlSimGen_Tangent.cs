﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrowdMP.Core
{
    public class ControlSimGen_Tangent : ControlSimGen
    {
        [Header("Main Parameters")]
        public int id = 0;
        public float speedComfort = 1.3f;
        public float personalArea = 0.33f;
        public float speedMax = 2.0f;
        public float g_beta = 0.5f;
        public float g_gamma = 0.25f;
        public float timeHorizon = 15;
        public uint maxNeighbors = 15;

        [System.Serializable]
        public class SpawnerParams
        {
            public float speedComfortOffset = 0;
            public int personalAreaOffset = 0;
            public float speedMaxOffset = 0;
            public float g_betaOffset = 0;
            public float g_gammaOffset = 0;
            public float timeHorizonOffset = 0;
            public int maxNeighborsOffset = 0;
        }
        [Header("Spawner Parameters")]
        public SpawnerParams randomness;

        public override CustomXmlSerializer<TrialControlSim> createControlSim(int GroupSeed)
        {
            TangentConfig sim = new TangentConfig();
            sim.id = id;
            sim.speedComfort = speedComfort;
            sim.personalArea = personalArea;
            sim.speedMax = speedMax;
            sim.g_beta = g_beta;
            sim.g_gamma = g_gamma;
            sim.timeHorizon = timeHorizon;
            sim.maxNeighbors = maxNeighbors;

            return sim;
        }

        public override ControlSimGen randDraw(GameObject agent, int groupID = 0)
        {
            ControlSimGen_Tangent csg = agent.AddComponent<ControlSimGen_Tangent>();


            csg.id = id;
            csg.speedComfort = speedComfort + Random.Range(-randomness.speedComfortOffset, randomness.speedComfortOffset);
            csg.personalArea = personalArea + Random.Range(-randomness.personalAreaOffset, randomness.personalAreaOffset);
            csg.speedMax = speedMax + Random.Range(-randomness.speedMaxOffset, randomness.speedMaxOffset);
            csg.g_beta = g_beta + Random.Range(-randomness.g_betaOffset, randomness.g_betaOffset);
            csg.g_gamma = g_gamma + Random.Range(-randomness.g_gammaOffset, randomness.g_gammaOffset);
            csg.timeHorizon = timeHorizon + Random.Range(-randomness.timeHorizonOffset, randomness.timeHorizonOffset);
            csg.maxNeighbors = maxNeighbors + (uint)Random.Range(-randomness.maxNeighborsOffset, randomness.maxNeighborsOffset);

            return csg;
        }
    }
}
