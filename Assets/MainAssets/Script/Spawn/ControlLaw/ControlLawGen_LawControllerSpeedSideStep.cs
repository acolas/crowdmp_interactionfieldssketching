﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using System;
using System.Xml.Serialization;
using UnityEngine;

//LawControllerSpeedSideStep : ControlLaw
namespace CrowdMP.Core
{
    public class ControlLawGen_LawControllerSpeedSideStep : ControlLawGen
    {
        public float speedCurrent = 0.0f;
        public float speedDefault = 1.33f;
        public float accelerationMax = 0.8f;
        public float sideSpeed = 0.5f;
        public float timeBeforeControl = 0;
        public float speedVariation = 0.5f;


        public override CustomXmlSerializer<ControlLaw> createControlLaw()
        {
            LawControllerSpeedSideStep law = new LawControllerSpeedSideStep();
            law.speedCurrent = speedCurrent;
            law.speedDefault = speedDefault;
            law.accelerationMax = accelerationMax;
            law.sideSpeed = sideSpeed;
            law.timeBeforeControl = timeBeforeControl;
            law.speedVariation = speedVariation;


            return law;
        }
    }
}