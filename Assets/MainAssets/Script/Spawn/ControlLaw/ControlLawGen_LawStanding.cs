﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

// public class LawStanding : ControlLaw
namespace CrowdMP.Core
{
    public class ControlLawGen_LawStanding : ControlLawGen
    {
        [Header("Main Parameters")]
        public int animationType = 0; // 0 - No animation | 1 - idle | 2 - talking | 3 - applause | 4 - look around
        public Vector3 LookAt= new Vector3();
        public bool lookDirection = false; 

        [System.Serializable]
        public class SpawnerParams
        {
            public int[] animationTypes;
            public bool randTarget=false;
        }
        [Header("Spawner Parameters")]
        public SpawnerParams randomness;


        public override CustomXmlSerializer<ControlLaw> createControlLaw()
        {
            LawStanding law = new LawStanding();
            law.animationType = animationType;
            //law.LookAt.vect = LookAt;

            if (lookDirection)
                law.LookAt.vect = gameObject.transform.position + LookAt;
            else
                law.LookAt.vect = LookAt;


            return law;
        }

        public override ControlLawGen randDraw(GameObject agent, int id = 0)
        {
            ControlLawGen_LawStanding law = agent.AddComponent<ControlLawGen_LawStanding>();

            if (randomness.animationTypes.Length > 0)
                law.animationType = randomness.animationTypes[Random.Range(0, randomness.animationTypes.Length)];
            else
                law.animationType = animationType;

            if (randomness.randTarget)
            {
                Vector2 dir = Random.insideUnitCircle.normalized;
                law.LookAt = agent.transform.position + new Vector3(dir.x, 0, dir.y);
            } else
                law.LookAt = LookAt;
            return law;
        }

        public override ControlLawGen randDraw(GameObject agent, ControlLawGen groupTemplate, int id = 0)
        {
            ControlLawGen_LawStanding nl = (ControlLawGen_LawStanding)randDraw(agent, id);
            nl.LookAt = ((ControlLawGen_LawStanding)groupTemplate).LookAt;

            return nl;
        }
    }
}