﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrowdMP.Core
{
    public class ControlLawGen_LawMoveStraight : ControlLawGen
    {
        [Header("Main Parameters")]
        public float speedCurrent = 0f;
        public float speedDefault = 1.33f;
        public float accelerationMax = 0.8f;

        [System.Serializable]
        public class SpawnerParams
        {
            public float speedCurrentOffset = 0f;
            public float speedDefaultOffset = 0f;
            public float accelerationMaxOffset = 0f;
        }
        [Header("Spawner Parameters")]
        public SpawnerParams randomness;


        public override CustomXmlSerializer<ControlLaw> createControlLaw()
        {
            LawMoveStraight law = new LawMoveStraight();
            law.speedCurrent = speedCurrent;
            law.speedDefault = speedDefault;
            law.accelerationMax = accelerationMax;


            return law;
        }

        public override ControlLawGen randDraw(GameObject agent, int id = 0)
        {
            ControlLawGen_LawMoveStraight law = agent.AddComponent<ControlLawGen_LawMoveStraight>();

            law.speedCurrent = speedCurrent + Random.Range(-randomness.speedCurrentOffset, randomness.speedCurrentOffset);
            law.speedDefault = speedDefault + Random.Range(-randomness.speedDefaultOffset, randomness.speedDefaultOffset);
            law.accelerationMax = accelerationMax + Random.Range(-randomness.accelerationMaxOffset, randomness.accelerationMaxOffset);

            return law;
        }
    }
}
