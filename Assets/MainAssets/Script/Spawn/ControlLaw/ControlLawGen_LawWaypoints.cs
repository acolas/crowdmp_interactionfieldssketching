﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using System.Collections.Generic;
using UnityEngine;

//public class LawWaypoints : ControlLaw
namespace CrowdMP.Core
{
    public class ControlLawGen_LawWaypoints : ControlLawGen
    {
        // should not be exposed in editor because goals are defined using zones (with randomisation inside a zone)
        private Vector3[] goals;

        public enum GoalType
        {
            FixOrder,
            RandOrder,
            SequentialOrder,
            WithNextPointsInGoal
        }

        [System.Serializable]
        public class SpawnerParams
        {
            public float speedCurrentOffset = 0f;
            public float speedDefaultOffset = 0f;
            public float accelerationMaxOffset = 0f;
            public float angularSpeedOffset = 0f;
        }

        [Header("Main parameters")]
        public float speedCurrent = 0;
        public float speedDefault = 1.33f;
        public float accelerationMax = 0.8f;
        public float angularSpeed = 360 * 100;

        // Use zones where random position in the zone will be attributed
        public Goal[] goalAreas;

        [Header("Other parameters")]
        public GoalType goalSelectionOrder = GoalType.FixOrder;
        public float reachedDist = 0.5f;
        public bool isLooping = false;

        [Header("Spawner Parameters")]
        public SpawnerParams randomness;

        public override CustomXmlSerializer<ControlLaw> createControlLaw()
        {
            LawWaypoints law = new LawWaypoints();

            law.speedCurrent = speedCurrent;
            law.speedDefault = speedDefault;
            law.accelerationMax = accelerationMax;
            law.angularSpeed = angularSpeed;

            law.reachedDist = reachedDist;
            law.isLooping = isLooping;

            if (goals != null)
            {
                foreach (Vector3 v in goals)
                    law.goals.Add(new ConfigVect(v));
            }
            else
            {
                Debug.LogWarning("There should be some goals if agents aimed to move");
            }

            return law;
        }

        public override ControlLawGen randDraw(GameObject agent, int id = 0)
        {
            ControlLawGen_LawWaypoints law = agent.AddComponent<ControlLawGen_LawWaypoints>();

            law.speedCurrent = speedCurrent + Random.Range(-randomness.speedCurrentOffset, randomness.speedCurrentOffset);
            law.speedDefault = speedDefault + Random.Range(-randomness.speedDefaultOffset, randomness.speedDefaultOffset);
            law.accelerationMax = accelerationMax + Random.Range(-randomness.accelerationMaxOffset, randomness.accelerationMaxOffset);
            law.angularSpeed = angularSpeed + Random.Range(-randomness.angularSpeedOffset, randomness.angularSpeedOffset);

            law.reachedDist = reachedDist;
            law.isLooping = isLooping;

            law.goalSelectionOrder = goalSelectionOrder;

            if (goalAreas.Length == 0)
            {
                Debug.LogError("At least one goal area must be added in the list. Fix it in the editor before spawning agents with this law");
                return null;
            }
            else
            {
                law.goals = new Vector3[goalAreas.Length];
                switch (law.goalSelectionOrder)
                {
                    case GoalType.WithNextPointsInGoal:
                        {
                            List<Goal> list = new List<Goal>();

                            int randomSelection = Random.Range(0, goalAreas.Length);
                            Goal firstGoal = goalAreas[randomSelection];
                            list.Add(firstGoal);

                            Goal current = firstGoal;
                            while (current.getNextPoint(current) != current)
                            {
                                Goal next = current.getNextPoint(current);
                                list.Add(next);
                                current = next;
                            }

                            law.goals = new Vector3[list.Count];
                            for (int i = 0; i < list.Count; i++)
                            {
                                law.goals[i] = list[i].randPosInArea();
                            }
                            break;
                        }
                    case GoalType.RandOrder:
                        {
                            for (int i = 0; i < goalAreas.Length; ++i)
                            {
                                int randomSelection = Random.Range(0, goalAreas.Length);
                                law.goals[i] = goalAreas[randomSelection].randPosInArea();
                            }
                            break;
                        }
                    case GoalType.SequentialOrder:
                        {
                            if (isLooping)
                                law.goals = new Vector3[goalAreas.Length * 2-1];

                            Goal[] sequence = new Goal[goalAreas.Length];

                            int randomSelection = Random.Range(0, goalAreas.Length);
                            sequence[0] = goalAreas[randomSelection];
                            law.goals[0] = sequence[0].randPosInArea();

                            for (int i = 1; i < goalAreas.Length; ++i)
                            {
                                sequence[i] = sequence[i - 1].getNextPoint(sequence[Mathf.Max(0, i - 2)]);
                                law.goals[i] = sequence[i].randPosInArea();
                            }

                            if (isLooping)
                            {
                                for (int i = 1; i < goalAreas.Length; ++i)
                                {
                                    law.goals[goalAreas.Length + i - 1] = law.goals[goalAreas.Length - i];
                                }
                            }

                            break;
                        }
                    case GoalType.FixOrder:
                    default:
                        {

                            for (int i = 0; i < goalAreas.Length; ++i)
                            {
                                int randomSelection = i % goalAreas.Length;
                                law.goals[i] = goalAreas[randomSelection].randPosInArea();

                            }
                            break;
                        }
                }
            }

            return law;
        }

        public override ControlLawGen randDraw(GameObject agent, ControlLawGen groupTemplate, int id = 0)
        {
            ControlLawGen_LawWaypoints newLaw = (ControlLawGen_LawWaypoints)randDraw(agent, id);

            if (newLaw != null)
            {
                newLaw.goals = ((ControlLawGen_LawWaypoints)groupTemplate).goals;
            }

            return newLaw;
        }
    }
}