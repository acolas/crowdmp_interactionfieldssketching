﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

namespace CrowdMP.Core
{

    /// <summary>
    /// Player agent that can be control using different input devices
    /// </summary>
    public abstract class Player : Agent
    {

        GameObject headObject;
#if MIDDLEVR
    public GameObject HeadNode=null;
    public GameObject HandNode=null;
#endif

        public abstract GameObject getHeadObject();
        public abstract void clear();

        //public Vector3 OrientationTorso()
        //{
        //    Vector3 orientationHead= getHeadObject().transform.rotation * Vector3.forward;
        //    int quotien = int(orientationHead / 50);
        //    return 
               
            
        //}

#if MIDDLEVR
    public override Vector3 Position { get { return HeadNode != null ? new Vector3(HeadNode.transform.position.x, transform.position.y, HeadNode.transform.position.z) : transform.position; } }

    public override void Translate(Vector3 translation)
    {
        if (HandNode != null)
        {
            Vector3 forward = HandNode.transform.forward;
            forward.y = 0;
            transform.position = transform.position + translation.z * forward;
        } else
            base.Translate(translation);
    }

    public override void Rotate(Vector3 rotation)
    {
        if (HeadNode)
        {
            transform.RotateAround(HeadNode.transform.position, transform.up, rotation.y);
        } else
            base.Rotate(rotation);
    }
#endif

    }
    
    /// <summary>
    /// Trial parameters concerning the player (XML serializable)
    /// </summary>
    public abstract class TrialPlayer : TrialAgent
    {

        public abstract Player createPlayerComponnent(GameObject agentObject, uint id);
    }
}