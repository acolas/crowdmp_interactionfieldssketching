﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
  MIT License
  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre

  Permission is hereby granted, free of charge, to any person obtaining 
  a copy of this software and associated documentation files (the 
  "Software"), to deal in the Software without restriction, including 
  without limitation the rights to use, copy, modify, merge, publish, 
  distribute, sublicense, and/or sell copies of the Software, and to 
  permit persons to whom the Software is furnished to do so, subject 
  to the following conditions:

  The above copyright notice and this permission notice shall be 
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
  SOFTWARE.
  
  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

namespace CrowdMP.Core
{
    /// <summary>
    /// Player agent that can be control using different input devices
    /// </summary>
    public class RegularPlayer : Player
    {

        internal GameObject headObject;
        internal ControlLaw playerController;

        internal bool isPresentHMDVR;

        bool checkedVR = false;

        //void OnDisable()
        //{
        //    Debug.Log("PrintOnDisable: script was disabled => " + this.gameObject );
        //}

        //void OnEnable()
        //{
        //    Debug.Log("PrintOnEnable: script was enabled => " + this.gameObject);
        //}

        /// <summary>
        /// Reset the player
        /// </summary>
        public override void clear()
        {
            playerController = null;
        }

        // Use this for initialization
        IEnumerator Start()
        {
            if (headObject == null)
            {
                initObjectLink();
            }
            while (Time.frameCount < 5)
                yield return null;
           
        }
        public override void setIFDisplay(InteractionField IF, int numberIFVel, int numberIFOr)
        {

            ViewInteractionField viewIF = this.gameObject.GetComponent<ViewInteractionField>();
            if (viewIF != null)
            {
                if (viewIF.IF1 == null)
                {
                    viewIF.IF1 = IF;
                    viewIF.initDisplay(0.05f, numberIFVel, numberIFOr);
                    viewIF.MatrixSwith = true;
                }
                else
                {
                    if (viewIF.IF1 != IF || viewIF.IF1.ParameterChanged)
                    {
                        viewIF.MatrixSwith = true;
                        viewIF.IF1 = IF;
                    }
                    else
                    {
                        viewIF.MatrixSwith = false;
                    }

                }
                viewIF.updateMatrix(this.gameObject.transform.position);
            }

        }

        public void checkVRState()
        {
            isPresentHMDVR = false;
            var inputDevices = new List<UnityEngine.XR.InputDevice>();
            var desiredCharacteristics = UnityEngine.XR.InputDeviceCharacteristics.HeadMounted;
            UnityEngine.XR.InputDevices.GetDevicesWithCharacteristics(desiredCharacteristics, inputDevices);
            isPresentHMDVR = inputDevices.Count >= 1;
            //Debug.Log("state : " + isPresentHMDVR + " frame : " + Time.frameCount);
        }



        public virtual void initObjectLink()
        {
            //foreach (Transform go in gameObject.GetComponentsInChildren<Transform>())
            //{
            // if (go.tag == "HeadPlayer")
            //{
            headObject = gameObject;
            this.gameObject.transform.position = transform.position + new Vector3(0, LoaderConfig.xpUserHeight, 0);

            /*#if MIDDLEVR
                        }
                        if (go.name == "HeadNode")
                        {
                            HeadNode= go.gameObject;
                        } else if (go.name == "HandNode")
                        {
                            HandNode = go.gameObject;
                        }

            #else
                                break;
                            }

            #endif
                        }*/
        }
        void Update()
        {

            checkVRState();
            /*      if (isPresentHMDVR)
                  {

                     // Debug.Log("LoaderConfig.xpUserHeight" + LoaderConfig.xpUserHeight);
                    //Debug.Log("LoaderConfig.playerInfo.getStartingPosition()" + LoaderConfig.playerInfo.getStartingPosition());

                      Vector3 posFix = this.gameObject.transform.position + LoaderConfig.playerInfo.getStartingPosition();
                     // Vector3 posFit = posFix;// + new Vector3(0, LoaderConfig.xpUserHeight, 0);

                     // this.gameObject.transform.position = this.gameObject.transform.position  + posFix;
                  }
            */
        }

        //public override void LateUpdate()
        //{
        //   // if (!checkedVR && Time.frameCount > 10)
        //    {
        //     //   checkVRState();
        //       // if (isPresentHMDVR)
        //        {
        //            Vector3 startingPos = LoaderConfig.playerInfo.getStartingPosition();
        //            this.gameObject.transform.parent.position =   new Vector3(startingPos.x,0, startingPos.z);
        //        }
        //        //checkedVR = true;
        //    }
        //    /*      if (isPresentHMDVR)
        //          {

        //             // Debug.Log("LoaderConfig.xpUserHeight" + LoaderConfig.xpUserHeight);
        //            //Debug.Log("LoaderConfig.playerInfo.getStartingPosition()" + LoaderConfig.playerInfo.getStartingPosition());

        //              Vector3 posFix = this.gameObject.transform.position + LoaderConfig.playerInfo.getStartingPosition();
        //             // Vector3 posFit = posFix;// + new Vector3(0, LoaderConfig.xpUserHeight, 0);

        //             // this.gameObject.transform.position = this.gameObject.transform.position  + posFix;
        //          }
        //    */
        //}


        /// <summary>
        /// Compute agent behavior for a simulation step
        /// </summary>
        public override void doStep()
        {
            // No behavior
            if (playerController == null)
                return;

            // Check for new state
            Vector3 translation;
            Vector3 rotation;

            playerController.computeGlobalMvt(ToolsTime.DeltaTime, out translation, out rotation);

            playerController.applyMvt(this, translation, rotation);
        }

        public override GameObject getHeadObject()
        {
            return headObject;
        }

        public override bool toKill()
        {
            return false;
        }
    }

    /// <summary>
    /// Trial parameters concerning the player (XML serializable)
    /// </summary>
    public class TrialRegularPlayer : TrialPlayer
    {
        public ConfigVect Position;
        public ConfigVect Rotation;

        [XmlElement("controlLaw")]
        public CustomXmlSerializer<ControlLaw> xmlControlLaw;

        [XmlElement("controlSim")]
        public CustomXmlSerializer<TrialControlSim> xmlControlSim;

        RegularPlayer a;

        [XmlIgnore]
        public ControlLaw controlLaw { get { return xmlControlLaw.Data; } }
        [XmlIgnore]
        public TrialControlSim controlSim { get { return xmlControlSim == null ? null : xmlControlSim.Data; } }

        public TrialRegularPlayer()
        {
            Position = new ConfigVect();
            Rotation = new ConfigVect();

            xmlControlLaw = new LawPlayerStatic();
            xmlControlSim = null;
        }

        public override Player createPlayerComponnent(GameObject agentObject, uint id)
        {
            a = agentObject.AddComponent<RegularPlayer>();
            a.id = id;
            // Init starting state
            agentObject.transform.position = Position.vect;
            agentObject.transform.rotation = Quaternion.Euler(Rotation.vect);

            // Set the head at the user height
            if (a.headObject == null)
            {
                a.initObjectLink();
            }

            // Init control law
            a.playerController = controlLaw;
            controlLaw.initialize(a);

            return a;
        }


        public override Agent createAgentComponnent(GameObject agentObject, uint id)
        {
            return createPlayerComponnent(agentObject, id);
        }

        public override Vector3 getStartingPosition()
        {
            return Position.vect;
        }

        public override TrialControlSim getControlSimInfo()
        {
            return controlSim;
        }
    }

}