﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrowdMP.Core
{
    /// <summary>
    /// Interface to create new simulation controller
    /// </summary>
    public interface ControlSim
    {
        /// <summary>
        /// Reset the controller
        /// </summary>
        void clear();

        /// <summary>
        /// Perform a step of the simulation
        /// </summary>
        /// <param name="deltaTime">Time step since last step</param>
        void doStep(float deltaTime);

        /// <summary>
        /// Add obstacles in the simulations
        /// </summary>
        /// <param name="obst">List of all the obstacles present in the environment</param>
        void addObstacles(Obstacles obst);

        /// <summary>
        /// Remove an agent controlled by the simulation
        /// </summary>
        /// <param name="id">Number of the agent</param>
        void removeAgent(int id);

        /// <summary>
        /// Add an agent controlled by the simulation
        /// </summary>
        /// <param name="position">Starting position of the agent</param>
        /// <param name="infos">Simulation parameters of the agent</param>
        void addAgent(Vector3 position, TrialControlSim infos);

        /// <summary>
        /// Add an agent that is not controlled by the simulation
        /// </summary>
        /// <param name="position">Starting position of the agent</param>
        void addNonResponsiveAgent(Vector3 position, float radius);

        /// <summary>
        /// Update agent state from external control
        /// </summary>
        /// <param name="id">Number of the agent</param>
        /// <param name="position">New position</param>
        /// <param name="goal">New goal</param>
        void updateAgentState(int id, Vector3 position, Vector3 goal);

        void updateAgentState(int uid, Vector3 position, Vector3 orientation, Vector3 velocity, Vector3 goal);

        /// <summary>
        /// Get the agent position from the simulation
        /// </summary>
        /// <param name="id">Id of the agent</param>
        /// <returns>The agent position from the simulation</returns>
        Vector3 getAgentPos2d(int id);
        /// <summary>
        /// Get the agent speed from the simulation
        /// </summary>
        /// <param name="id">Id of the agent</param>
        /// <returns>The agent speed as a vector from the simulation</returns>
        Vector3 getAgentSpeed2d(int id);

        /// <summary>
        /// Get the id of the simulation given by the trial parameters
        /// </summary>
        /// <returns>Id of the simulation</returns>
        int getConfigId();
    }
}
