﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrowdMP.Core
{
    public class Obstacles
    {

        List<ObstCylinder> pillars;
        List<ObstWall> walls;

        public Obstacles()
        {
            pillars = new List<ObstCylinder>();
            walls = new List<ObstWall>();
        }

        public void addPillar(Vector3 center, float radius)//not used anymore
        {
            pillars.Add(new ObstCylinder(center, radius));
        }

        public void addPillar(GameObject pillar)
        {
            float diameter = pillar.transform.lossyScale.z;
            if (pillar.transform.lossyScale.z < pillar.transform.lossyScale.x)
                diameter = pillar.transform.lossyScale.x;
            ObstCylinder pillarObst = new ObstCylinder(pillar.transform.position, diameter/ 2);
            if (pillar.GetComponent<IFObstacle>() != null)
            {
                pillarObst.VelocityInteractionFields = pillar.GetComponent<IFObstacle>().VelocityInteractionFields;
                pillarObst.OrientationInteractionFields = pillar.GetComponent<IFObstacle>().OrientationInteractionFields;
                //pillarObst.SimID = pillar.GetComponent<IFObstacle>().SimID;
            }
            pillars.Add(pillarObst);

        }

        public void addWall(Vector3 p1, Vector3 p2, Vector3 p3, Vector3 p4)// not used anymore I think
        {
            walls.Add(new ObstWall(p1, p2, p3, p4));
        }

        public void addWall(GameObject wall)
        {
            Vector3 center = wall.transform.position;
            Vector3 p1 = center + wall.transform.forward * wall.transform.lossyScale.z / 2 - wall.transform.right * wall.transform.lossyScale.x / 2;
            Vector3 p2 = center + wall.transform.forward * wall.transform.lossyScale.z / 2 + wall.transform.right * wall.transform.lossyScale.x / 2;
            Vector3 p3 = center - wall.transform.forward * wall.transform.lossyScale.z / 2 + wall.transform.right * wall.transform.lossyScale.x / 2;
            Vector3 p4 = center - wall.transform.forward * wall.transform.lossyScale.z / 2 - wall.transform.right * wall.transform.lossyScale.x / 2;
            ObstWall wallObst = new ObstWall(p1, p2, p3, p4);
            if (wall.GetComponent<IFObstacle>() != null)
            {
                wallObst.VelocityInteractionFields = wall.GetComponent<IFObstacle>().VelocityInteractionFields;
                wallObst.OrientationInteractionFields = wall.GetComponent<IFObstacle>().OrientationInteractionFields;
                //wallObst.SimID = wall.GetComponent<IFObstacle>().SimID;

            }
            walls.Add(wallObst);
        }

        public List<ObstCylinder> Pillars { get { return pillars; } }
        public List<ObstWall> Walls { get { return walls; } }
    }

    public class ObstCylinder
    {
        public Vector3 position;
        public float radius;
        public List<InteractionField> VelocityInteractionFields; //the list of Interaction Field's attributed to this obstacle. 
        public List<InteractionField> OrientationInteractionFields;
        public int SimID; //the id of the SimID

        public ObstCylinder(Vector3 pos, float r)
        {
            position = pos;
            radius = r;
            VelocityInteractionFields = new List<InteractionField>();
            OrientationInteractionFields = new List<InteractionField>();
            SimID = new int();
        }
    }

    public class ObstWall
    {
        public Vector3 A;
        public Vector3 B;
        public Vector3 C;
        public Vector3 D;
        public List<InteractionField> VelocityInteractionFields; //the list of Interaction Field's attributed to this obstacle. 
        public List<InteractionField> OrientationInteractionFields;
        public int SimID; //the id of the SimID

        public ObstWall(Vector3 p1, Vector3 p2, Vector3 p3, Vector3 p4)
        {
            A = p1;
            B = p2;
            C = p3;
            D = p4;
            VelocityInteractionFields = new List<InteractionField>();
            OrientationInteractionFields = new List<InteractionField>();
            SimID = new int();
        }

        /// <summary>
        /// Check clockwise order between two vector
        /// </summary>
        /// <param name="v1">First vector</param>
        /// <param name="v2">Second vector</param>
        /// <returns>   1 if first comes before second in clockwise order.
        ///             -1 if second comes before first.
        ///             0 if the points are identical.</returns>
        public static float isClockwise(Vector3 center, Vector3 v1, Vector3 v2)
        {
            if (v1 == v2)
                return 0;

            Vector3 OA = (v1 - center);
            Vector3 OB = (v2 - center);
            float det = -OA.x * OB.z + OB.x * OA.z;

            return det;




            //det = (a.x - center.x) * (b.y - center.y) - (b.x - center.x) * (a.y - center.y)
        }
    }
}
