﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrowdMP.Core
{

    public class SimRVO : ControlSim
    {
        int ConfigId;
        RVO.Simulator sim;

        List<int> UIDtoSIMID;

        public SimRVO(int id)
        {
            ConfigId = id;
            sim = RVO.Simulator.Instance;
            UIDtoSIMID = new List<int>();
        }

        public SimRVO(int id, float neighborDist, int maxNeighbors, float timeHorizon, float timeHorizonObst, float radius, float maxSpeed)
        {
            ConfigId = id;
            sim = RVO.Simulator.Instance;
            sim.setAgentDefaults(neighborDist, maxNeighbors, timeHorizon, timeHorizonObst, radius, maxSpeed, new RVO.Vector2(0, 0));
        }

        public void removeAgent(int uid)
        {
            updateAgentState(UIDtoSIMID[uid], new Vector3(-10000, -10000, -10000), new Vector3(0, 0, 0));
        }

        public void addAgent(Vector3 position, TrialControlSim infos)
        {
            RVOconfig RVOinfos = (RVOconfig)infos;
            int i=sim.addAgent(position, RVOinfos.neighborDist, RVOinfos.maxNeighbors, RVOinfos.timeHorizon, RVOinfos.timeHorizonObst, RVOinfos.radius, RVOinfos.maxSpeed, new RVO.Vector2(0, 0));
            UIDtoSIMID.Add(i);
        }

        public void addNonResponsiveAgent(Vector3 position, float radius)
        {
            int i=sim.addAgent(position, 0, 0, 0, 0, radius, 5, new RVO.Vector2(0, 0));
            UIDtoSIMID.Add(i);
        }

        public void addObstacles(Obstacles obst)
        {
            foreach (ObstCylinder pillar in obst.Pillars)
            {
                sim.addAgent(pillar.position, 0, 0, 0, 0, pillar.radius, 5, new RVO.Vector2(0, 0));
            }

            foreach (ObstWall wall in obst.Walls)
            {
                List<RVO.Vector2> poly = new List<RVO.Vector2>();

                Vector3 center = (wall.A + wall.B + wall.C + wall.D) / 4;
                if (ObstWall.isClockwise(center, wall.A, wall.B) > 0)
                {
                    poly.Add(wall.A);
                    poly.Add(wall.B);
                    poly.Add(wall.C);
                    poly.Add(wall.D);

                }
                else
                {
                    poly.Add(wall.A);
                    poly.Add(wall.D);
                    poly.Add(wall.C);
                    poly.Add(wall.B);

                }

                sim.addObstacle(poly);
            }

            sim.processObstacles();
        }

        public void clear()
        {
            sim.Clear();
        }

        public void doStep(float deltaTime)
        {
            sim.setTimeStep(deltaTime);
            sim.doStep();
        }

        public Vector3 getAgentPos2d(int uid)
        {
            return sim.getAgentPosition(UIDtoSIMID[uid]);
        }

        public Vector3 getAgentSpeed2d(int uid)
        {
            return sim.getAgentVelocity(UIDtoSIMID[uid]);
        }

        public int getConfigId()
        {
            return ConfigId;
        }

        public void updateAgentState(int uid, Vector3 position, Vector3 goal)
        {
            sim.setAgentPosition(UIDtoSIMID[uid], position);
            sim.setAgentPrefVelocity(UIDtoSIMID[uid], goal);
        }

        public void updateAgentState(int uid, Vector3 position, Vector3 orientation, Vector3 velocity, Vector3 goal)
        {
            sim.setAgentPosition(UIDtoSIMID[uid], position);
            sim.setAgentPrefVelocity(UIDtoSIMID[uid], goal);
        }
    }
}