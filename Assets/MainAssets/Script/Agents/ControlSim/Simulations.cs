﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau, Adèle Colas
**  Contact: crowd_group@inria.fr
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrowdMP.Core
{

    /// <summary>
    /// Class managing all the simulations used during the trial
    /// </summary>
    public class SimManager
    {

        List<ControlSim> simulators;
        List<int> agentToSim;

        /// <summary>
        /// Initialize the class
        /// </summary>
        public SimManager()
        {
            simulators = new List<ControlSim>();
            agentToSim = new List<int>();
        }

        /// <summary>
        /// Reset the manager
        /// </summary>
        public void clear()
        {
            foreach (ControlSim sim in simulators)
                sim.clear();
            simulators.Clear();
            agentToSim.Clear();
        }

        /// <summary>
        /// Initialize all the simulations needed for the Trial using the LoaderConfig
        /// </summary>
        public void initSimulations(Obstacles obst)
        {
            //List<TrialControlSim> tmp=new List<TrialControlSim>();

            // Create simulators
            agentToSim.Add(getInternalId(LoaderConfig.playerInfo.getControlSimInfo()));
            //tmp.Add(LoaderConfig.playerInfo.getControlSimInfo());

  
            // Fill simulators with agents and Obstacles
            foreach (TrialAgent a in LoaderConfig.agentsInfo)
            {
                getInternalId(a.getControlSimInfo());
                //tmp.Add(a.getControlSimInfo());
            }

            // Fill simulators with agents and Obstacles
            int internalID = 0;
            
            foreach (ControlSim sim in simulators)
            {
                // Agents
                int agent = 0;
                if (agentToSim[agent] == internalID)
                    sim.addAgent(LoaderConfig.playerInfo.getStartingPosition(), LoaderConfig.playerInfo.getControlSimInfo());

                else if (sim is SimIF)
                {
                    //Debug.Log("IF sim");
                    //Debug.Log(LoaderConfig.playerInfo.getStartingPosition() + ", " + LoaderConfig.playerInfo.radius + ", " + LoaderConfig.playerInfo.getControlSimInfo());
                    ((SimIF)sim).addIFNonResponsiveAgent(LoaderConfig.playerInfo.getStartingPosition(), LoaderConfig.playerInfo.radius, LoaderConfig.playerInfo.getControlSimInfo());

                }


                else
                {

                    sim.addNonResponsiveAgent(LoaderConfig.playerInfo.getStartingPosition(), LoaderConfig.playerInfo.radius);
                }



                //foreach (TrialAgent a in LoaderConfig.agentsInfo)
                //{
                //    ++agent;

                //    if (agentToSim[agent] == internalID)
                //    {
                //        sim.addAgent(a.getStartingPosition(), a.getControlSimInfo());
                //    }
                //    else
                //    {
                //        sim.addNonResponsiveAgent(a.getStartingPosition(), a.radius);
                //    }
                //}

                // Obstacles
                sim.addObstacles(obst);

                //initialise sketched fields if IF


                // update id
                ++internalID;
            }
        }

        public void removeAgent(int id)
        {
            foreach (ControlSim sim in simulators)
            {
                sim.removeAgent(id);
            }
        }

        public void addNewAgent(Agent a, float radius, TrialControlSim infos)
        {
            // Fill simulators with agents and Obstacles
            int internalID = 0;
            agentToSim.Add(getInternalId(infos));
            foreach (ControlSim sim in simulators)
            {
                
                
                if (agentToSim[agentToSim.Count - 1] == internalID)
                {

                    
                    sim.addAgent(a.transform.position, infos);
                }
                //else if (sim is SimIF)
                //{// check this up again, why would they be non repsonsive?
                //    ((SimIF)sim).addIFNonResponsiveAgent(a.transform.position, radius, infos);
                //}
                //else
                else
                {
                    sim.addNonResponsiveAgent(a.transform.position, radius);
                }
                    
                // update id
                ++internalID;
            }

        }

        /// <summary>
        /// Check the config id of a simulation and get the internal one if already created, create a new simulations otherwise
        /// </summary>
        /// <param name="info">Trial parameters of the simulation</param>
        /// <returns>Internal id</returns>
        private int getInternalId(TrialControlSim info)
        {
            if (info == null)
                return -1;

            int id = -1;
            int i = 0;
            foreach (ControlSim sim in simulators)
            {
                if (sim.getConfigId() == info.getConfigId())
                {
                    id = i;
                    break;
                }
            }
            if (id < 0)
            {
                id = simulators.Count;
                simulators.Add(info.createControlSim(id));
            }

            return id;
        }

        /// <summary>
        /// Update all the simulations with XP state, perform a simulation step and ovveride controlled agent state
        /// </summary>
        /// <param name="deltaTime">Time since last step</param>
        /// <param name="posList">List of current position fo the agents</param>
        /// <param name="playerGoalState">The player state he is trying to reach</param>
        /// <param name="agentsGoalState">The agent states they are trying to reach</param>
        public void doStep(float deltaTime, List<Vector3> posList, Agent playerGoalState, List<Agent> agentsGoalState)
        {
            int i = 0;

            // Update simulators state
            foreach (ControlSim sim in simulators)
            {
                sim.updateAgentState(0, posList[i], (playerGoalState.Position - posList[i]) / deltaTime);
                foreach (Agent a in agentsGoalState)
                {
                    ++i;
                    if (i < posList.Count)
                    {
                        sim.updateAgentState((int)a.id, posList[i], (a.Position - posList[i]) / deltaTime);
                    }
                }

                sim.doStep(deltaTime);
            }

            int simId = agentToSim[0];
            if (simId >= 0)
                playerGoalState.simOverride(simulators[simId].getAgentPos2d(0), simulators[simId].getAgentSpeed2d(0));

            foreach (Agent a in agentsGoalState)
            {
                simId = agentToSim[(int)a.id];
                if (simId >= 0)
                    a.simOverride(simulators[simId].getAgentPos2d((int)a.id) + new Vector3(0, a.transform.position.y, 0), simulators[simId].getAgentSpeed2d((int)a.id));
            }
        }

        /// <summary>
        /// Update all the simulations with XP state, perform a simulation step and ovveride controlled agent state
        /// </summary>
        /// <param name="deltaTime">Time since last step</param>
        /// <param name="posList">List of current position fo the agents</param>
        /// <param name="orientList">List of current orientation fo the agents</param>
        /// <param name="playerGoalState">The player state he is trying to reach</param>
        /// <param name="agentsGoalState">The agent states they are trying to reach</param>
        public void doStep(float deltaTime, List<Vector3> posList, List<Vector3> orientList, Agent playerGoalState, List<Agent> agentsGoalState)
        {

            int i = 0;

            // Update simulators state
            foreach (ControlSim sim in simulators)
            {
                
                sim.updateAgentState(0, posList[i], orientList[i], playerGoalState.Velocity, (playerGoalState.Position - posList[i]) / deltaTime); //update the player's data
                if (playerGoalState.GetComponent<ViewInteractionField>()!=null)
                {

                    InteractionField IF = ((SimIF)sim).getIFofAgent((int)playerGoalState.id, playerGoalState.GetComponent<ViewInteractionField>().NumIF, playerGoalState.GetComponent<ViewInteractionField>().IsVelocity);
                    if(IF != null)
                    {
                        playerGoalState.setIFDisplay(IF, ((SimIF)sim).getNumberVelocityIF(true, (int)playerGoalState.id), ((SimIF)sim).getNumberOrientationIF(true, (int)playerGoalState.id));
                    }
                    ((SimIF)sim).updateFieldsObstalce();
                    
                    
                }

          
                if (playerGoalState.gameObject.GetComponent("Interactor Object") as InteractorObject != null)
                {
                    GameObject interactorObject = GameObject.Find("Interactor Object");
                    InteractorObject interactor = (InteractorObject)interactorObject.GetComponent(typeof(InteractorObject));
                    if(interactor.IsActivate == true)
                    {
                        ((SimIF)sim).updateFieldsAgent(0, posList[i], orientList[i]);
                    }
                }
                //if (this.game.GetComponent("YourDesiredScript") as YourDesiredScript) != null)

                //    if (sim is SimIF && playerGoalState.Activate)
                //{

                //    ((SimIF)sim).updateFieldsAgent(0, posList[i], orientList[i]);
                //    ((SimIF)sim).updateFieldsObstalce();
                //}
                foreach (Agent a in agentsGoalState)
                {
                    ++i;
                    if (i < posList.Count)
                    {
                        sim.updateAgentState((int)a.id, posList[i], orientList[i], a.Velocity, (a.Position - posList[i]) / deltaTime);

                        if (a.GetComponent<ViewInteractionField>() != null) 
                        {

                            InteractionField IF = ((SimIF)sim).getIFofAgent((int)a.id, a.GetComponent<ViewInteractionField>().NumIF, a.GetComponent<ViewInteractionField>().IsVelocity);
                            if (IF != null)
                            {
                                a.setIFDisplay(IF, ((SimIF)sim).getNumberVelocityIF(true, (int)a.id), ((SimIF)sim).getNumberOrientationIF(true, (int)a.id));
                            }

                        }
                        //if (a.gameObject.GetComponent("Interactor Object") as InteractorObject != null)
                        //{
                        //    GameObject interactorObject = GameObject.Find("Interactor Object");
                        //    InteractorObject interactor = (InteractorObject)interactorObject.GetComponent(typeof(InteractorObject));

                        //    //if (interactor.IsActivate == true)
                        //    //{
                        //    //    ((SimIF)sim).updateFieldsAgent(0, posList[i], orientList[i]);
                        //    //}
                        //}
                        ////if (sim is SimIF && a.Activate )
                        ////{
                        ////    ((SimIF)sim).updateFieldsAgent((int)a.id, posList[i], orientList[i]);
                        ////}
                    }

                }

                
                sim.doStep(deltaTime);
            }

            //Caution ! this line enables the use of a control sim (e.g. umans to avoid collisions) in combination with a control law that controls automatically the player (e.g. move forward)
            //This means that if you want a free control of the player, the player cannot have a control sim (only a control law such as LawControllerSpeedAngle
            int simId = agentToSim[0];
            if (simId >= 0)
            {
                // In the case of Interaction Fields, the player requires a control sim, even in the case of a desired manual control on it with e.g. LawControllerSpeedAngle
                // Therefore, the following code CAN NOT be executed in case of Interaction Fields (IF)
                if (!(simulators[simId] is SimIF))
                {
                    playerGoalState.simOverride(simulators[simId].getAgentPos2d(0), simulators[simId].getAgentSpeed2d(0));
                   
                }
            }
            
            foreach (Agent a in agentsGoalState)
            {
                simId = agentToSim[(int)a.id];
                if (simId >= 0)
                {
                    
                   // a.simOverride(simulators[simId].getAgentPos2d((int)a.id) + new Vector3(0, a.transform.position.y, 0), simulators[simId].getAgentSpeed2d((int)a.id));

                    //if (simulators[simId] is SimIF) //if we are using IF, then we want to send all the predictions
                    //{
                    //    a.simOverride(((SimIF)simulators[simId]).getAgentPoss2d((int)a.id) , simulators[simId].getAgentSpeed2d((int)a.id));

                    //    a.simOverrideOrient(((SimIF)simulators[simId]).getAgentOrients2d((int)a.id));
                    //    //a.simOverrideOrient(((SimIF)simulators[simId]).getAgentOrien  t2d((int)a.id));
                    //}
                    //else
                    //{
                    //    a.simOverride(new Vector3[] { simulators[simId].getAgentPos2d((int)a.id) + new Vector3(0, a.transform.position.y, 0) }, simulators[simId].getAgentSpeed2d((int)a.id));
                    //}
                    if (simulators[simId] is SimIF) //if we are using IF, then we want to send all the predictions
                    {
                        a.simOverride(((SimIF)simulators[simId]).getAgentPoss2d((int)a.id), simulators[simId].getAgentSpeed2d((int)a.id));
                        a.simOverrideOrient(((SimIF)simulators[simId]).getAgentOrients2d((int)a.id));

                    }
                    else
                    {
                        Vector3[] positions = { simulators[simId].getAgentPos2d((int)a.id) + new Vector3(0, a.transform.position.y, 0) };
                        a.simOverride(new Vector3[] { simulators[simId].getAgentPos2d((int)a.id) + new Vector3(0, a.transform.position.y, 0) }, simulators[simId].getAgentSpeed2d((int)a.id));

                    }

                }
            }
        }


        public void updateAgents(SimIF sim, float deltaTime, List<Agent> agentsGoalState, int FutureStepsCount)
        {           
            sim.TimeSinceLastSim += deltaTime;
            if (sim.TimeSinceLastSim < 0.33)
            {
                return;
            }

            // First we clear the AgentData
            for (int i = 0; i < agentsGoalState.Count; i++)
            {
                agentsGoalState[i].cleanPositions();
                agentsGoalState[i].cleanOrientations();
            }
            for (int j =0; j < FutureStepsCount; j++)
            {
                foreach (Agent a in agentsGoalState)
                {
                    a.addToPositions(sim.getAgentPos2d((int)a.id) + new Vector3(0, a.transform.position.y, 0));
                    a.addToOrientations(sim.getAgentOrient2d((int)a.id));
                    a.addToVelocities(sim.getAgentSpeed2d((int)a.id));
                    
                    //sim.updateAgentState((int)a.id, a.Positions[j] + a.Velocities[j]*deltaTime, a.Positions[j] +a.Velocities[j]*deltaTime);
                }
                sim.doStep(deltaTime);
                
            }
            // As we have executed an update, we reset TimeSinceLastSim
            sim.TimeSinceLastSim = 0f;
        }
    }

}