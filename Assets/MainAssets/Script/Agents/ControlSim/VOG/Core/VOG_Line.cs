/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau, Z. Ren
**  Contact: crowd_group@inria.fr
*/

/*
 * VOG : Velocity Obstacles & Groups
 * Crowd Simulation mixing Velocity Constraints from RVO with Group Constraints
 * 
 * This simulator is from the paper : 
 * Group Modeling: a Unified Velocity-based Approach
 * By Z. Ren, P. Charalambous, J. Bruneau, Q. Peng1and and J. Pettr� 
 *
 * The current code is a modified version of the original code from Z. Ren
 * for a better integration with CrowdMP architecture and needs
 */
using UnityEngine;
using System.Collections;

namespace VOG
{
	/** ORCA Line.
	 * Simply holds a point and a direction, nothing fancy.
	 * 
	 * \astarpro 
	 */
	public struct Line {
		public Vector2 point;
		public Vector2 dir;
	}
}