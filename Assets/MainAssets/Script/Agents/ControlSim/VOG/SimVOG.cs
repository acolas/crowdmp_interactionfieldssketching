﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrowdMP.Core
{
    public class SimVOG : ControlSim
    {
        int ConfigId;
        VOG.Simulator sim;

        List<int> UIDtoSIMID;

        public SimVOG(int id)
        {
            ConfigId = id;
            sim = new VOG.Simulator(0, false);
            sim.simulateGroup = true;
            UIDtoSIMID = new List<int>();
        }

        public void removeAgent(int uid)
        {
            updateAgentState(uid, new Vector3(-10000, -10000, -10000), new Vector3(0, 0, 0));
            sim.RemoveAgent(sim.GetAgents()[uid]);

            UIDtoSIMID[uid] = -1;
            for (int i = uid + 1; i < UIDtoSIMID.Count; ++i)
                --UIDtoSIMID[i];
        }

        public void addAgent(Vector3 position, TrialControlSim infos)
        {
            VOGConfig VOGinfos = (VOGConfig)infos;
            VOG.Agent a = new VOG.Agent(position);
            a.agentID = sim.GetAgents().Count;
            a.MaxSpeed = VOGinfos.maxSpeed;
            a.NeighbourDist = VOGinfos.neighborDist;
            a.AgentTimeHorizon = VOGinfos.timeHorizon;
            a.ObstacleTimeHorizon = VOGinfos.timeHorizonObst;
            a.Height = 1.7f;
            a.Radius = VOGinfos.radius;
            a.MaxNeighbours = VOGinfos.maxNeighbors;
            
            a.groupID = VOGinfos.group.groupID;
            a.formConstraint = VOGinfos.group.useFormation;
            a.groupNeighbourNum = VOGinfos.group.neighbourNum;
            a.neighbourRadius = VOGinfos.group.neighbourDist;
            a.groupNeighbourDist = VOGinfos.group.neighbourDetectionDist;
            a.weightForDesiredVel = VOGinfos.group.weightPrefVel;
            a.weightForGroup = VOGinfos.group.weightGroup;
            a.groupHorizonTime = VOGinfos.group.horizonTime;

            UIDtoSIMID.Add(sim.GetAgents().Count);
            sim.AddAgent(a);
        }

        public void addNonResponsiveAgent(Vector3 position, float radius)
        {
            VOG.Agent a = new VOG.Agent(position);

            a.agentID = sim.GetAgents().Count;
            a.groupID = -1;
            a.MaxSpeed = 2;
            a.NeighbourDist = 0;
            a.AgentTimeHorizon = 5;
            a.ObstacleTimeHorizon = 5;
            a.Height = 1.7f;
            a.Radius = radius;
            a.MaxNeighbours = 0;

            UIDtoSIMID.Add(sim.GetAgents().Count);
            sim.AddAgent(a);
        }

        public void addObstacles(Obstacles obst)
        {
            foreach (ObstCylinder pillar in obst.Pillars)
            {
                addNonResponsiveAgent(pillar.position, pillar.radius);
            }

            foreach (ObstWall wall in obst.Walls)
            {
                List<RVO.Vector2> poly = new List<RVO.Vector2>();

                Vector3 center = (wall.A + wall.B + wall.C + wall.D) / 4;
                if (ObstWall.isClockwise(center, wall.A, wall.B) > 0)
                {
                    sim.AddObstacle(wall.A, wall.B, 2);
                    sim.AddObstacle(wall.B, wall.C, 2);
                    sim.AddObstacle(wall.C, wall.D, 2);
                    sim.AddObstacle(wall.D, wall.A, 2);
                }
                else
                {
                    sim.AddObstacle(wall.A, wall.D, 2);
                    sim.AddObstacle(wall.D, wall.C, 2);
                    sim.AddObstacle(wall.C, wall.B, 2);
                    sim.AddObstacle(wall.B, wall.A, 2);
                }
            }
        }

        public void clear()
        {
            sim = new VOG.Simulator(0, false);
        }

        public void doStep(float deltaTime)
        {
            sim.Update(deltaTime);
        }

        public Vector3 getAgentPos2d(int uid)
        {
            List<VOG.Agent> aList = sim.GetAgents();

            Vector3 pos = aList[UIDtoSIMID[uid]].InterpolatedPosition;
            pos.y = 0;

            return pos;
        }

        public Vector3 getAgentSpeed2d(int uid)
        {
            List<VOG.Agent> aList = sim.GetAgents();

            Vector3 vel = aList[UIDtoSIMID[uid]].newVelocity;
            vel.y = 0;

            return vel;
        }

        public int getConfigId()
        {
            return ConfigId;
        }

        public void updateAgentState(int uid, Vector3 position, Vector3 goal)
        {
            List<VOG.Agent> aList = sim.GetAgents();

            aList[UIDtoSIMID[uid]].Teleport(position);
            aList[UIDtoSIMID[uid]].DesiredVelocity = goal;
        }

        public void updateAgentState(int uid, Vector3 position, Vector3 orientation, Vector3 velocity, Vector3 goal)
        {
            throw new System.NotImplementedException();
        }
    }
}
