﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

namespace CrowdMP.Core
{

    public class VOGConfig : TrialControlSim
    {
        [XmlAttribute("SimulationID")]
        public int id;
        [XmlAttribute]
        public float neighborDist;
        [XmlAttribute]
        public int maxNeighbors;
        [XmlAttribute]
        public float timeHorizon;
        [XmlAttribute]
        public float timeHorizonObst;
        [XmlAttribute]
        public float radius;
        [XmlAttribute]
        public float maxSpeed;

        public VOGConfigGroup group;

        public int getConfigId()
        {
            return id;
        }

        public ControlSim createControlSim(int id)
        {

            return new SimVOG(id);
        }

        public VOGConfig()
        {
            id = 0;
            neighborDist = 5;
            maxNeighbors = 3;
            timeHorizon = 5;
            timeHorizon = 2;
            radius = 0.33f;
            maxSpeed = 2;

            group = new VOGConfigGroup();
        }

    }

    [System.Serializable]
    public class VOGConfigGroup
    {
        [XmlAttribute]
        public int groupID;
        [XmlAttribute]
        public bool useFormation;
        [XmlAttribute]
        public int neighbourNum;
        [XmlAttribute]
        public float neighbourDist;
        [XmlAttribute]
        public float neighbourDetectionDist;
        [XmlAttribute]
        public float horizonTime;
        [XmlAttribute]
        public float weightPrefVel;
        [XmlAttribute]
        public float weightGroup;


        public VOGConfigGroup()
        {
            groupID = -1;
            useFormation=true;
            neighbourNum=2;
            neighbourDist=1f;
            neighbourDetectionDist=80;
            weightPrefVel=0.15f;
            weightGroup=0.20f;
            horizonTime=2f;
        }

        public VOGConfigGroup(VOGConfigGroup vogG)
        {
            groupID = vogG.groupID;
            useFormation = vogG.useFormation;
            neighbourNum = vogG.neighbourNum;
            neighbourDist = vogG.neighbourDist;
            neighbourDetectionDist = vogG.neighbourDetectionDist;
            weightPrefVel = vogG.weightPrefVel;
            weightGroup = vogG.weightGroup;
            horizonTime = vogG.horizonTime;
        }
    }
}
