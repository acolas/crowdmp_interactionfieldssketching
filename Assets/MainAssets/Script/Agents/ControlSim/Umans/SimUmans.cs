﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program.If not, see<https://www.gnu.org/licenses/>.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using UnityEngine;

namespace CrowdMP.Core
{

    public class SimUmans : ControlSim
    {
        // ---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // ---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // EXPORT C++ FUNCTIONS FROM LIB
        [DllImport("UMANS-Library")]
        public static extern IntPtr UMANS_CreateSimObject(string configFileName, int numberOfThreads);
        [DllImport("UMANS-Library")]
        private static extern void UMANS_DestroySimObject(IntPtr obj);
        [DllImport("UMANS-Library")]
        public static extern void UMANS_doStep(IntPtr obj, float deltaTime);


        [DllImport("UMANS-Library")]
        public static extern void UMANS_addObstacle(IntPtr obj, float s_startx, float s_starty, float s_endx, float s_endy);
        [DllImport("UMANS-Library")]
        private static extern bool UMANS_removeAgent(IntPtr obj, int s_indPedestrian);
        [DllImport("UMANS-Library")]
        public static extern int UMANS_addAgent(IntPtr obj, float pos_x, float pos_y, float radius, float prefSpeed, float maxSpeed, float maxAcceleration, int policyID, int group, float goalX, float goalY);
        [DllImport("UMANS-Library")]
        public static extern int UMANS_addObst(IntPtr obj, float[] Px, float[] Py, int numPoint);
        //[DllImport("UMANS-Library")]
        //private static extern bool UMANS_IFtoAgent(IntPtr obj, int s_indPedestrian, int IF, int[] linkedAgent, int numLinkedAgent, int[] linkedObst, int numLinkedObst);
        //[DllImport("UMANS-Library")]
        //private static extern bool UMANS_IFtoObst(IntPtr obj, int s_indObst, int IF, int[] linkedAgent, int numLinkedAgent, int[] linkedObst, int numLinkedObst);


        [DllImport("UMANS-Library")]
        public static extern bool UMANS_setPosition(IntPtr obj, int s_indPedestrian, float s_x, float s_y);
        [DllImport("UMANS-Library")]
        public static extern bool UMANS_setVelocity(IntPtr obj, int s_indPedestrian, float s_x, float s_y);
        [DllImport("UMANS-Library")]
        private static extern bool UMANS_setGoal(IntPtr obj, int s_indPedestrian, float s_x, float s_y, float speed);
        [DllImport("UMANS-Library")]
        private static extern bool UMANS_setOrientation(IntPtr obj, int s_indPedestrian, float s_x, float s_y);
        [DllImport("UMANS-Library")]
        public static extern float UMANS_getAgentPositionX(IntPtr obj, int s_indPedestrian);
        [DllImport("UMANS-Library")]
        public static extern float UMANS_getAgentPositionY(IntPtr obj, int s_indPedestrian);
        [DllImport("UMANS-Library")]
        public static extern float UMANS_getAgentVelX(IntPtr obj, int s_indPedestrian);
        [DllImport("UMANS-Library")]
        public static extern float UMANS_getAgentVelY(IntPtr obj, int s_indPedestrian);
        [DllImport("UMANS-Library")]
        public static extern float UMANS_getAgentOrientationX(IntPtr obj, int s_indPedestrian);
        [DllImport("UMANS-Library")]
        public static extern float UMANS_getAgentOrientationY(IntPtr obj, int s_indPedestrian);
        // EXPORT C++ FUNCTIONS FROM LIB
        // ---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // ---------------------------------------------------------------------------------------------------------------------------------------------------------------


        public List<int> UIDtoSIMID;
        public int ConfigId;
        public IntPtr sim;
        public float TimeSinceLastSim;


        public SimUmans(int id)
        {
            TimeSinceLastSim = 0f;
            ConfigId = id;
            UIDtoSIMID = new List<int>();
            string file = "Assets\\MainAssets\\Script\\Agents\\ControlSim\\Umans\\Config.xml";
            try
            {
                Console.Write("creation Umans");
                sim = UMANS_CreateSimObject(file, 1);

            }
            catch (InvalidCastException)
            {
                Debug.LogError("error during creation of umans sim");
            }
        }

        public SimUmans(int id, string path)
        {
            ConfigId = id;
            UIDtoSIMID = new List<int>();
           try
            {
                Console.Write("creation Umans");
                sim = UMANS_CreateSimObject(path, 1);

            }
            catch (InvalidCastException)
            {
                Debug.LogError("error during creation of umans sim");
            }
        }

        ~SimUmans()
        {
            if (sim != null)
            {
                try
                {
                    UMANS_DestroySimObject(sim);
                }
                catch (InvalidCastException)
                {
                    Debug.LogError("error during destruction of umans sim");
                }
            }
        }

        public void removeAgent(int uid)
        {
            UMANS_removeAgent(sim, UIDtoSIMID[uid]);
        }

        public virtual void addAgent(Vector3 position, TrialControlSim infos)
        {
            UmansConfig Uinfos = (UmansConfig)infos;
            Debug.Log("addition of an agent UMANS SIM");
            int i = -1;
            if (sim != null)
            {
                try
                {
                    i = UMANS_addAgent(sim, -position.x, position.z, Uinfos.radius, Uinfos.prefVel, Uinfos.maxVel, Uinfos.maxAcc, (int)Uinfos.policy, -1, Uinfos.goalX, Uinfos.goalY);
                    UIDtoSIMID.Add(i);
                }
                catch (InvalidCastException)
                {
                    Debug.LogError("error when adding agent in umans sim");
                }
            }
            if (i < 0)
                ToolsDebug.logFatalError("Error during agent creation in UMANS");
        }

        public virtual void addNonResponsiveAgent(Vector3 position, float radius)
        {
            Debug.Log("Add Umans non responsive agent");
            int i = -1;
            if (sim != null)
            {
                try
                {
                    i = UMANS_addAgent(sim, -position.x, position.z, radius, 0, 0, 0, (int)UmansConfig.Policy.BLANK, -1, -position.x, position.z);
                    UIDtoSIMID.Add(i);
                }
                catch (InvalidCastException)
                {
                    Debug.LogError("error when adding non responsive agent");
                }
            }
            if (i < 0)
                ToolsDebug.logFatalError("Error during non responsive agent creation in UMANS");
        }

        public virtual void addObstacles(Obstacles obst)
        {
            foreach (ObstCylinder pillar in obst.Pillars)
            {
                try
                {
                    UMANS_addAgent(sim, -pillar.position.x, pillar.position.z, pillar.radius, 0, 0, 0, (int)UmansConfig.Policy.BLANK, -1, -pillar.position.x, pillar.position.z);
                }
                catch (InvalidCastException)
                {
                    ToolsDebug.logFatalError("Error while adding obstacles to Umans");
                }
            }

            foreach (ObstWall wall in obst.Walls)
            {
                Vector3 center = (wall.A + wall.B + wall.C + wall.D) / 4;
                float[] Px = new float[4];
                float[] Py = new float[4]; ;
                if (ObstWall.isClockwise(center, wall.A, wall.B) > 0)
                {
                    Px[0] = -wall.A.x; Px[1] = -wall.B.x; Px[2] = -wall.C.x; Px[3] = -wall.D.x;
                    Py[0] = wall.A.z; Py[1] = wall.B.z; Py[2] = wall.C.z; Py[3] = wall.D.z;
                }
                else
                {
                    Px[0] = -wall.A.x; Px[1] = -wall.D.x; Px[2] = -wall.C.x; Px[3] = -wall.B.x;
                    Py[0] = wall.A.z; Py[1] = wall.D.z; Py[2] = wall.C.z; Py[3] = wall.B.z;
                }

                int nbObst = UMANS_addObst(sim, Px, Py, 4);
                ToolsDebug.log(string.Concat("obst:", nbObst.ToString()));
            }
        }

        public void clear()
        {
            try
            {
                if (sim != null)
                    UMANS_DestroySimObject(sim);
                sim = UMANS_CreateSimObject("test", 1);
            }
            catch (InvalidCastException)
            {
                Debug.LogError("error during clearing of umans sim");
            }
        }

        public virtual void doStep(float deltaTime)
        {
            try
            {
                if (sim != null)
                    UMANS_doStep(sim, deltaTime);
            }
            catch (InvalidCastException)
            {
                Debug.LogError("error when doing step in umans sim");
            }
        }

        public Vector3 getAgentPos2d(int uid)
        {
            try
            {
                if (sim != null)

                    return new Vector3(-UMANS_getAgentPositionX(sim, UIDtoSIMID[uid]), 0, UMANS_getAgentPositionY(sim, UIDtoSIMID[uid]));
            }
            catch (InvalidCastException)
            {
                Debug.LogError("error when getting agent2D pos");
            }

            return new Vector3();
        }
        public Vector3 getAgentOrient2d(int uid)
        {
            try
            {
                if (sim != null)

                    return new Vector3(-UMANS_getAgentOrientationX(sim, UIDtoSIMID[uid]), 0, UMANS_getAgentOrientationY(sim, UIDtoSIMID[uid]));
            }
            catch (InvalidCastException)
            {
                Debug.LogError("error when getting agent orient2D in IF");
            }

            return new Vector3();
        }

        public Vector3 getAgentSpeed2d(int uid)
        {
            try
            {
                if (sim != null)

                    return new Vector3(-UMANS_getAgentVelX(sim, UIDtoSIMID[uid]), 0, UMANS_getAgentVelY(sim, UIDtoSIMID[uid]));
            }
            catch (InvalidCastException)
            {
                Debug.LogError("error when getting agent2D speed");
            }


            return new Vector3();
        }

        public int getConfigId()
        {
            return ConfigId;
        }

        public void updateAgentState(int uid, Vector3 position, Vector3 orientation, Vector3 velocity, Vector3 goal)
        {
            try
            {
                if (sim != null)
                {
                    if(uid == 0)
                    {
                        bool test = UMANS_setPosition(sim, UIDtoSIMID[uid], -position.x, position.z);
                        bool test2 = UMANS_setGoal(sim, UIDtoSIMID[uid], -position.x - 10 * goal.x, position.z + 10 * goal.z, goal.magnitude);
                        bool test4 = UMANS_setOrientation(sim, UIDtoSIMID[uid], -orientation.x, orientation.z);
                        bool test3 = UMANS_setVelocity(sim, UIDtoSIMID[uid], -velocity.x, velocity.z);
                    }
                    else
                    {
                        bool test2 = UMANS_setGoal(sim, UIDtoSIMID[uid], -position.x - 10 * goal.x, position.z + 10 * goal.z, goal.magnitude);
                        bool test4 = UMANS_setOrientation(sim, UIDtoSIMID[uid], -orientation.x, orientation.z);
                    }
                    
                }
            }
            catch (InvalidCastException)
            {
                ToolsDebug.logFatalError("Error while updating AgentState pos, orient, vel, goal");
            }

        }


        public void updateAgentState(int uid, Vector3 position, Vector3 goal)
        {
            try
            {
                if (sim != null)
                {
                    if (uid < UIDtoSIMID.Count)
                    {
                        bool test = UMANS_setPosition(sim, UIDtoSIMID[uid], -position.x, position.z);
                        bool test2 = UMANS_setGoal(sim, UIDtoSIMID[uid], -position.x - 10 * goal.x, position.z + 10 * goal.z, goal.magnitude);
                    }
                }
            }
            catch (InvalidCastException)
            {
                ToolsDebug.logFatalError("Error while updating AgentState pos, goal");
            }

        }


        //public void linkIFtoAgent(int idAgent, int IF, int[] linkedAgent, int[] linkedObst)
        //{
        //    UMANS_IFtoAgent(sim, idAgent, IF, linkedAgent, linkedAgent.Length, linkedObst, linkedObst.Length);
        //}
        //public void linkIFtoObst(int idObsT, int IF, int[] linkedAgent, int[] linkedObst)
        //{
        //    UMANS_IFtoObst(sim, idObsT, IF, linkedAgent, linkedAgent.Length, linkedObst, linkedObst.Length);
        //}
    }
}
