﻿  /*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program.If not, see<https://www.gnu.org/licenses/>.
**  
**  Authors: Julien Bruneau, Adèle Colas
**  Contact: crowd_group@inria.fr
*/
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

namespace CrowdMP.Core
{

    public class UmansConfig : TrialControlSim
    {

        public enum Policy
        {
            BLANK = 0,
            ORCA = 1,
            RVO = 2,
            DUTRA = 3,
            FOEAvoidance = 4,
            KARAMOUZAS = 5,
            MOUSSAID = 6,
            PARIS = 7,
            PLEdestrians = 8,
            PowerLaw = 9,
            SocialForces = 10,
            VANTOLL = 11,
            IF = 12,
            IFandCO = 13,
            IFalone = 14,
            IFandGR = 15,
            IFandRVO = 16,
            Player = 9999
        }

        [XmlAttribute("SimulationID")]
        public int id;
        [XmlAttribute]
        public Policy policy;
        [XmlAttribute]
        public float radius;
        [XmlAttribute]
        public float prefVel;
        [XmlAttribute]
        public float maxVel;
        [XmlAttribute]
        public float maxAcc;
        [XmlAttribute]
        public float goalX;
        [XmlAttribute]
        public float goalY;

        public int getConfigId()
        {
            return (int)id;
        }

        public virtual ControlSim createControlSim(int id)
        {           
            return new SimUmans(id);
        }

        public UmansConfig()
        {
            id = 0;
            policy=Policy.ORCA;
            radius=0.33f;
            prefVel = 1.33f;
            maxVel=2f;
            maxAcc=5f;
            goalX = 0;
            goalY = 0;
            id = 0;

        }
    }


}
