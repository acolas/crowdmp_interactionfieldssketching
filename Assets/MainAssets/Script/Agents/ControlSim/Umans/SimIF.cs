﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program.If not, see<https://www.gnu.org/licenses/>.
**  
**  Authors: Adèle Colas
**  Contact: crowd_group@inria.fr
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using UnityEngine;
using System.Xml;

namespace CrowdMP.Core
{

    public class SimIF : SimUmans
    {
        // ---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // ---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // EXPORT C++ FUNCTIONS FROM LIB
        //[DllImport("UMANS-Library")]
        //private static extern IntPtr UMANS_CreateSimObject(string configFileName, int numberOfThreads);
        //[DllImport("UMANS-Library")]
        //private static extern void UMANS_DestroySimObject(IntPtr obj);
        //[DllImport("UMANS-Library")]
        //private static extern void UMANS_doStep(IntPtr obj, float deltaTime);


        //[DllImport("UMANS-Library")]
        //private static extern void UMANS_addObstacle(IntPtr obj, float s_startx, float s_starty, float s_endx, float s_endy);
        //[DllImport("UMANS-Library")]
        //private static extern bool UMANS_removeAgent(IntPtr obj, int s_indPedestrian);
        //[DllImport("UMANS-Library")]
        //private static extern int UMANS_addAgent(IntPtr obj, float pos_x, float pos_y, float radius, float prefSpeed, float maxSpeed, float maxAcceleration, int policyID);
        //[DllImport("UMANS-Library")]
        //private static extern int UMANS_addObst(IntPtr obj, float[] Px, float[] Py, int numPoint);
        [DllImport("UMANS-Library")]
        private static extern bool UMANS_IFtoAgent(IntPtr obj, int s_indPedestrian, int IF, int[] linkedAgent, int numLinkedAgent, int[] linkedObst, int numLinkedObst);
        [DllImport("UMANS-Library")]
        private static extern bool UMANS_IFtoObst(IntPtr obj, int s_indObst, int IF, int[] linkedAgent, int numLinkedAgent, int[] linkedObst, int numLinkedObst);


        //[DllImport("UMANS-Library")]
        //private static extern bool UMANS_setPosition(IntPtr obj, int s_indPedestrian, float s_x, float s_y);

        //[DllImport("UMANS-Library")]
        //private static extern bool UMANS_setVelocity(IntPtr obj, int s_indPedestrian, float s_x, float s_y);
        //[DllImport("UMANS-Library")]
        //private static extern bool UMANS_setGoal(IntPtr obj, int s_indPedestrian, float s_x, float s_y, float speed);

        //[DllImport("UMANS-Library")]
        //private static extern float UMANS_getAgentPositionX(IntPtr obj, int s_indPedestrian);
        //[DllImport("UMANS-Library")]
        //private static extern float UMANS_getAgentPositionY(IntPtr obj, int s_indPedestrian);
        //[DllImport("UMANS-Library")]
        //private static extern float UMANS_getAgentVelX(IntPtr obj, int s_indPedestrian);
        //[DllImport("UMANS-Library")]
        //private static extern float UMANS_getAgentVelY(IntPtr obj, int s_indPedestrian);

        [DllImport("UMANS-Library")]
        private static extern float UMANS_getAgentPosPredictionX(IntPtr obj, int s_indPedestrian, int numPred);
        [DllImport("UMANS-Library")]
        private static extern float UMANS_getAgentPosPredictionY(IntPtr obj, int s_indPedestrian, int numPred);
        [DllImport("UMANS-Library")]
        private static extern float UMANS_getAgentOrientPredictionX(IntPtr obj, int s_indPedestrian, int numPred);
        [DllImport("UMANS-Library")]
        private static extern float UMANS_getAgentOrientPredictionY(IntPtr obj, int s_indPedestrian, int numPred);


        //Fields specific methodes to draw the fields


        [DllImport("UMANS-Library")]
        private static extern int UMANS_getLengthVelocityInteractionFields(IntPtr obj, int s_indPedestrian, bool isAgent); // get the number of velocity fields Agent of id s_indPedestrian has
        [DllImport("UMANS-Library")]
        private static extern int UMANS_getLengthOrientationInteractionFields(IntPtr obj, int s_indPedestrian, bool isAgent);// get the number of orientation fields Agent of id s_indPedestrian has
        [DllImport("UMANS-Library")]
        private static extern int UMANS_fieldsOrientationID(IntPtr obj, int s_indPedestrian, int numField, bool isAgent);//get the id in the simulator of a field a ccording to the list of field of the dll 
        [DllImport("UMANS-Library")]
        private static extern int UMANS_fieldsVelocityID(IntPtr obj, int s_indPedestrian, int numField, bool isAgent);//get the id in the simulator of a field a ccording to the list of field of the dll 
        [DllImport("UMANS-Library")]
        private static extern float UMANS_fieldParameterValue(IntPtr obj, int s_indPedestrian, int IDField);//get the parameter value in the simulator of the field IDField 
        [DllImport("UMANS-Library")]
        private static extern float UMANS_fieldAngleValue(IntPtr obj, int s_indPedestrian, int IDField);//get the rotation angle value in the simulator of the field IDField
        [DllImport("UMANS-Library")]
        private static extern float UMANS_fieldsOrientationAngleValue(IntPtr obj, int s_indPedestrian, int IDField, bool isAgent);//get the rotation angle value in the simulator of the orientation field IDField 
        [DllImport("UMANS-Library")]
        private static extern float UMANS_fieldsVelocityAngleValue(IntPtr obj, int s_indPedestrian, int IDField, bool isAgent);//get the rotation angle value in the simulator of the velocity field IDField
        [DllImport("UMANS-Library")]
        private static extern float UMANS_fieldsOrientationParameterValue(IntPtr obj, int s_indPedestrian, int IDField, bool isAgent);//get the parameter value in the simulator of the orientation field IDField 
        [DllImport("UMANS-Library")]
        private static extern float UMANS_fieldsVelocityParameterValue(IntPtr obj, int s_indPedestrian, int IDField, bool isAgent);//get the parameter value in the simulator of the velocity field IDField 


        // EXPORT C++ FUNCTIONS FROM LIB
        // ---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // ---------------------------------------------------------------------------------------------------------------------------------------------------------------


        private Dictionary<int, InteractionField> InteractionFieldsDictionnaire;
        private Dictionary<int, Vector3> ObstacleDictionnaire;

        public Dictionary<int, InteractionField> InteractionFields
        {
            get
            {
                return InteractionFieldsDictionnaire;
            }

        }

        public Dictionary<int, Vector3> ObstacleDictionnaire1 { get => ObstacleDictionnaire; set => ObstacleDictionnaire = value; }

        public SimIF(int id) : base(id)
        {

        }

        public SimIF(int id, string path):base(id, path)
        {
            InteractionFieldsDictionnaire = new Dictionary<int, InteractionField>();
            ObstacleDictionnaire = new Dictionary<int, Vector3>();
            XmlDocument configDoc = new XmlDocument();
            configDoc.Load(path);
            Debug.Log(path);
            String fieldspath = configDoc.DocumentElement.SelectSingleNode("/Simulation/InteractionFields").Attributes["file"].Value; /*Find the path of the IF config*/
            loadInteractionFieldIn(fieldspath);
            

        }

        ~SimIF()
        {
            InteractionFieldsDictionnaire.Clear();
            ObstacleDictionnaire.Clear();
        }
        public override void addAgent(Vector3 position, TrialControlSim infos)
        {

            UmansConfig Uinfos = (UmansConfig)infos;
            int i = -1;

            if (sim != null)
            {
                
                try
                {
                    
                    if(Uinfos is IFUmansConfig)
                    {
                        IFUmansConfig IFUinfos = (IFUmansConfig)infos;
                        i = UMANS_addAgent(sim, -position.x, position.z, IFUinfos.radius, IFUinfos.prefVel, IFUinfos.maxVel, IFUinfos.maxAcc, (int)IFUinfos.policy, IFUinfos.group, IFUinfos.goalX, IFUinfos.goalY);
                       

                        foreach (InteractionField IF in IFUinfos.InteractionFields)
                        {
                            //Debug.Log("we add IF id: "+IF.id +" of agent id: " + i );
                            int[] agents = new int[0];
                            int[] obsts = new int[0];
                            if (IF.LinkedObjectID == 9999)
                            {
                                assignIFtoAgent(i, IF.id, agents, obsts);
                            }
                            else
                            {
                                if (IF.typeLinkedAgent)
                                    agents = new int[1];
                                else
                                    obsts = new int[1];
                                assignIFtoAgent(i, IF.id, agents, obsts);
                            }

                        }
                    }
                    else
                    {
                        i = UMANS_addAgent(sim, -position.x, position.z, Uinfos.radius, Uinfos.prefVel, Uinfos.maxVel, Uinfos.maxAcc, (int)Uinfos.policy, -1, Uinfos.goalX, Uinfos.goalY);
                    }
                    
                    UIDtoSIMID.Add(i);

                }
                catch (InvalidCastException)
                {
                    Debug.LogError("error when adding agent in IF");
                }
            }
            if (i < 0)
                ToolsDebug.logFatalError("Error during agent creation in IF");
        }


        public void addIFNonResponsiveAgent(Vector3 position, float radius, TrialControlSim infos)
        {
            IFUmansConfig Uinfos = (IFUmansConfig)infos;

            int i = 2;
            if (sim != null)
            {
                try
                {
                    i = UMANS_addAgent(sim, -position.x, position.z, radius, 0, 0, 0, (int)UmansConfig.Policy.BLANK, Uinfos.group, Uinfos.goalX, Uinfos.goalY);
                    //Debug.Log("hey we add an non responsive agent: " + i + " at " + -position.x + ", " + position.z);
                    UIDtoSIMID.Add(i);


                    foreach (InteractionField IF in Uinfos.InteractionFields)
                    {
                        
                        int[] agents = new int[0];
                        int[] obsts = new int[0];
                        if (IF.LinkedObjectID == 9999)
                        {
                            assignIFtoAgent(i, IF.id, agents, obsts);
                        }
                        else
                        {
                            if (IF.typeLinkedAgent)
                                agents = new int[1];
                            else
                                obsts = new int[1];
                            assignIFtoAgent(i, IF.id, agents, obsts);
                        }

                    }

                }
                catch (InvalidCastException)
                {
                    Debug.LogError("error when adding non responsive agent in IF");
                }
            }
            if (i < 0)
                ToolsDebug.logFatalError("Error during non responsive agent creation in IF UMANS");
        }

        public override void addObstacles(Obstacles obst)
        {
            foreach (ObstCylinder pillar in obst.Pillars)
            {
                try
                {
                    int i = UMANS_addAgent(sim, -pillar.position.x, pillar.position.z, pillar.radius, 0, 0, 0, (int)UmansConfig.Policy.BLANK, -1, -pillar.position.x, pillar.position.z);
                    //ObstacleDictionnaire.Add(i, pillar.position); I am not sure how it would work so for now IF skethcing is only available with wall  an not pillar
                    if (pillar.VelocityInteractionFields.Count != 0)
                    {

                        for (int j = 0; j < pillar.VelocityInteractionFields.Count; j++)
                        {
                            InteractionField IF = pillar.VelocityInteractionFields[j];
                            int[] agents = new int[0];
                            int[] obsts = new int[0];
                            if (IF.LinkedObjectID == 9999)
                            {
                                assignIFtoObst(i, IF.id, agents, obsts);
                            }
                            else
                            {
                                if (IF.typeLinkedAgent)
                                    agents = new int[1];
                                else
                                    obsts = new int[1];
                                assignIFtoObst(i, IF.id, agents, obsts);
                            }

                            pillar.VelocityInteractionFields[j] = InteractionFields[IF.id];
                        }
                    }
                    if (pillar.OrientationInteractionFields.Count != 0)
                    {

                        for (int j = 0; j < pillar.OrientationInteractionFields.Count; j++)
                        {
                            InteractionField IF = pillar.OrientationInteractionFields[j];
                            int[] agents = new int[0];
                            int[] obsts = new int[0];
                            if (IF.LinkedObjectID == 9999)
                            {
                                assignIFtoObst(i, IF.id, agents, obsts);
                            }
                            else
                            {
                                if (IF.typeLinkedAgent)
                                    agents = new int[1];
                                else
                                    obsts = new int[1];
                                assignIFtoObst(i, IF.id, agents, obsts);
                            }

                            pillar.OrientationInteractionFields[j] = InteractionFields[IF.id];
                        }
                    }


                }

                catch (InvalidCastException)
                {
                    ToolsDebug.logFatalError("Error while adding obstacles to Umans");
                }

            }

            foreach (ObstWall wall in obst.Walls)
            {

                try
                {
                    Vector3 center = (wall.A + wall.B + wall.C + wall.D) / 4;
                    float[] Px = new float[4];
                    float[] Py = new float[4]; ;
                    if (ObstWall.isClockwise(center, wall.A, wall.B) > 0)
                    {
                        Px[0] = -wall.A.x; Px[1] = -wall.B.x; Px[2] = -wall.C.x; Px[3] = -wall.D.x;
                        Py[0] = wall.A.z; Py[1] = wall.B.z; Py[2] = wall.C.z; Py[3] = wall.D.z;
                    }
                    else
                    {
                        Px[0] = -wall.A.x; Px[1] = -wall.D.x; Px[2] = -wall.C.x; Px[3] = -wall.B.x;
                        Py[0] = wall.A.z; Py[1] = wall.D.z; Py[2] = wall.C.z; Py[3] = wall.B.z;
                    }

                    
                    int nbObst = UMANS_addObst(sim, Px, Py, 4);

                    ObstacleDictionnaire.Add(nbObst, center);
                    if (wall.VelocityInteractionFields.Count != 0)
                    {
                        for(int j =0; j<wall.VelocityInteractionFields.Count; j++)
                        {
                            InteractionField IF = wall.VelocityInteractionFields[j];
                            int[] agents = new int[0];
                            int[] obsts = new int[0];
                            if (IF.LinkedObjectID == 9999)
                            {
                                assignIFtoObst(nbObst, IF.id, agents, obsts);
                            }
                            else
                            {
                                if (IF.typeLinkedAgent)
                                    agents = new int[1];
                                else
                                    obsts = new int[1];
                                assignIFtoObst(nbObst, IF.id, agents, obsts);
                            }
                            wall.VelocityInteractionFields[j]= InteractionFields[IF.id];
                        }
                       
                    }
                    if (wall.OrientationInteractionFields.Count != 0)
                    {
                        for (int j = 0; j < wall.OrientationInteractionFields.Count; j++)
                        {
                            InteractionField IF = wall.OrientationInteractionFields[j];
                            int[] agents = new int[0];
                            int[] obsts = new int[0];
                            if (IF.LinkedObjectID == 9999)
                            {
                                assignIFtoObst(nbObst, IF.id, agents, obsts);
                            }
                            else
                            {
                                if (IF.typeLinkedAgent)
                                    agents = new int[1];
                                else
                                    obsts = new int[1];
                                assignIFtoObst(nbObst, IF.id, agents, obsts);
                            }
                            wall.OrientationInteractionFields[j] = InteractionFields[IF.id];
                        }

                    }
                }
                catch (InvalidCastException)
                {
                    ToolsDebug.logFatalError("Error while adding obstacles to Umans");
                }
            }
        }


        public Vector3[] getAgentPoss2d(int uid)
        {

            try
            {
                if (sim != null)
                {


                    List<Vector3> predictions = new List<Vector3>();
                    predictions.Add( new Vector3(UMANS_getAgentPositionX(sim, UIDtoSIMID[uid]), 0, UMANS_getAgentPositionY(sim, UIDtoSIMID[uid])));
                    for (int i =0; i<10; i++)
                    {
                       
                        predictions.Add(new Vector3(UMANS_getAgentPosPredictionX(sim, UIDtoSIMID[uid], i), 0, UMANS_getAgentPosPredictionY(sim, UIDtoSIMID[uid], i)));
                    }

                    return predictions.ToArray();
                }

            }
            catch (InvalidCastException)
            {
                Debug.LogError("error when getting agent 2Dpos in IF");
            }
            Vector3[] ArrEmpty = { };
            return ArrEmpty;
        }

        public int getNumberVelocityIF(bool isAgent, int IDsource)
        {
            var result = UMANS_getLengthVelocityInteractionFields(sim, IDsource, isAgent);
            if (result <= 0)
            {
                result = 0;
            }

            return  result;
        }

        public int getNumberOrientationIF(bool isAgent, int IDsource)
        {
            var result = UMANS_getLengthOrientationInteractionFields(sim, IDsource, isAgent);
            if (result <= 0)
            {
                result = 0;
            }

            return result;
        }

        public void updateFieldsAgent(int IDsource, Vector3 posSource, Vector3 orientSource)
        {

            //Debug.Log("DEBUG: " + UMANS_fieldsVelocityParameterValue(sim, 2, 1));
            for (int i = 0; i < UMANS_getLengthVelocityInteractionFields(sim, IDsource, true); i++)
            {

                int IDField = UMANS_fieldsVelocityID(sim, IDsource, i,true);


                float ParameterField = UMANS_fieldsVelocityParameterValue(sim, IDsource,i, true);
                float angleField = UMANS_fieldsVelocityAngleValue(sim, IDsource, i, true);
                InteractionFieldsDictionnaire[IDField].Angle = angleField;
                InteractionFieldsDictionnaire[IDField].Parameter = ParameterField;
                //Draw.DrawField(InteractionFieldsDictionnaire[IDField], posSource, ParameterField, angleField);

            }
            for (int i = 0; i < UMANS_getLengthOrientationInteractionFields(sim, IDsource, true); i++)
            {
                int IDField = UMANS_fieldsOrientationID(sim, IDsource, i, true);
                float ParameterField = UMANS_fieldsOrientationParameterValue(sim, IDsource, i, true);
                float angleField = UMANS_fieldsOrientationAngleValue(sim, IDsource, i, true);
                InteractionFieldsDictionnaire[IDField].Angle = angleField;
                InteractionFieldsDictionnaire[IDField].Parameter = ParameterField;
 
                 //Draw.DrawField(InteractionFieldsDictionnaire[IDField], posSource, ParameterField, angleField);

            }

        }

        public InteractionField getIFofAgent(int idAgent, int NumIF, bool isVelocity)
        {
            if (isVelocity)
            {
               
                int IDField = UMANS_fieldsVelocityID(sim, idAgent, NumIF, true);
                float ParameterField = UMANS_fieldsVelocityParameterValue(sim, idAgent, NumIF, true);
                float angleField = UMANS_fieldsVelocityAngleValue(sim, idAgent, NumIF, true);
                InteractionFieldsDictionnaire[IDField].Angle = angleField;
                InteractionFieldsDictionnaire[IDField].setParameter(ParameterField);
                return InteractionFieldsDictionnaire[IDField];
                
            }
            else
            {
                int IDField = UMANS_fieldsOrientationID(sim, idAgent, NumIF, true);
                float ParameterField = UMANS_fieldsOrientationParameterValue(sim, idAgent, NumIF, true);
                float angleField = UMANS_fieldsOrientationAngleValue(sim, idAgent, NumIF, true);
                InteractionFieldsDictionnaire[IDField].Angle = angleField;
                
                
                InteractionFieldsDictionnaire[IDField].setParameter(ParameterField);
                
                 
                return InteractionFieldsDictionnaire[IDField];
            }
        }

        public InteractionField getIFofObstacle(int idObst, int NumIF, bool isVelocity)
        {
            if (isVelocity)
            {
                int IDField = UMANS_fieldsVelocityID(sim, idObst, NumIF, false);
                float ParameterField = UMANS_fieldsVelocityParameterValue(sim, idObst, NumIF, false);
                float angleField = UMANS_fieldsVelocityAngleValue(sim, idObst, NumIF, false);
                InteractionFieldsDictionnaire[IDField].Angle = angleField;
                InteractionFieldsDictionnaire[IDField].setParameter(ParameterField);
                return InteractionFieldsDictionnaire[IDField];

            }
            else
            {
                int IDField = UMANS_fieldsOrientationID(sim, idObst, NumIF, false);
 
                float ParameterField = UMANS_fieldsOrientationParameterValue(sim, idObst, NumIF, false);
                float angleField = UMANS_fieldsOrientationAngleValue(sim, idObst, NumIF, false);
                InteractionFieldsDictionnaire[IDField].Angle = angleField;
                InteractionFieldsDictionnaire[IDField].setParameter(ParameterField);
                return InteractionFieldsDictionnaire[IDField];
            }
        }
        public void updateFieldsObstalce()
        {
            foreach(int IDsource in ObstacleDictionnaire.Keys)
            {
                for (int i = 0; i < UMANS_getLengthVelocityInteractionFields(sim, IDsource, false); i++)
                {

                    int IDField = UMANS_fieldsVelocityID(sim, IDsource, i, false);
                    float ParameterField = UMANS_fieldsVelocityParameterValue(sim, IDsource, i, false);
                    float angleField = UMANS_fieldsVelocityAngleValue(sim, IDsource, i, false);
                    InteractionFieldsDictionnaire[IDField].Angle = angleField;
                    InteractionFieldsDictionnaire[IDField].setParameter(ParameterField);
                   // Draw.DrawField(InteractionFieldsDictionnaire[IDField], ObstacleDictionnaire[IDsource], ParameterField, angleField);

                }
                for (int i = 0; i < UMANS_getLengthOrientationInteractionFields(sim, IDsource, false); i++)
                {
                    int IDField = UMANS_fieldsOrientationID(sim, IDsource, i, false);
                    float ParameterField = UMANS_fieldsOrientationParameterValue(sim, IDsource, i, false);
                    float angleField = UMANS_fieldsOrientationAngleValue(sim, IDsource, i, false);
                    InteractionFieldsDictionnaire[IDField].Angle = angleField;
                    InteractionFieldsDictionnaire[IDField].setParameter(ParameterField);
                   // Draw.DrawField(InteractionFieldsDictionnaire[IDField], ObstacleDictionnaire[IDsource], ParameterField, angleField);

                }
           }
       }



        public override void doStep(float deltaTime)
        {
            try
            {
                if (sim != null)
                {
                    base.doStep(deltaTime);

                }

            }
            catch (InvalidCastException)
            {
                Debug.LogError("error when doing step in IF");
            }
        }
        
        public Vector3[] getAgentOrients2d(int uid)
        {
            try
            {
                List<Vector3> predictions = new List<Vector3>();
                predictions.Add(new Vector3(-UMANS_getAgentOrientationX(sim, UIDtoSIMID[uid]), 0, UMANS_getAgentOrientationY(sim, UIDtoSIMID[uid])));
                if (sim != null)
                {
                    for (int i = 0; i < 10; i++)
                    {
                        predictions.Add(new Vector3(-UMANS_getAgentOrientPredictionX(sim, UIDtoSIMID[uid], i), 0, UMANS_getAgentOrientPredictionY(sim, UIDtoSIMID[uid], i)));
                    }
                    return predictions.ToArray();
                }

            }
            catch (InvalidCastException)
            {
                Debug.LogError("error when getting agent orient2D in IF");
            }
            Vector3[] ArrEmpty = { };
            return ArrEmpty;
        }

        public void assignIFtoAgent(int idAgent, int IF, int[] linkedAgent, int[] linkedObst)
        {
           
            UMANS_IFtoAgent(sim, idAgent, IF, linkedAgent, linkedAgent.Length, linkedObst, linkedObst.Length);
        }
        public void assignIFtoObst(int idObsT, int IF, int[] linkedAgent, int[] linkedObst)
        {
            UMANS_IFtoObst(sim, idObsT, IF, linkedAgent, linkedAgent.Length, linkedObst, linkedObst.Length);
        }

        //Build Interaction Field Matrix and assign them to the field for the purpose of sketching them in 3D
        public void loadInteractionFieldIn(string pathIF)
        {
            try{
                

                XmlDocument configFields = new XmlDocument();
                configFields.Load(pathIF);
                

                foreach (XmlNode field in configFields.DocumentElement.ChildNodes)
                {
                    if (field.Name == "InteractionField")
                    {
                        int id = Int32.Parse(field.Attributes["id"].Value);

                        InteractionField IF = new InteractionField(id);

                        foreach (XmlNode matrix in field.ChildNodes)
                        {
                            
                            if (matrix.Name == "Matrix")
                            {
                                String path = "Assets/MainAssets/Script/Agents/ControlSim/Umans/Fields/"+ matrix.Attributes["file"].Value;
                                float parameter = 0;
                                if(matrix.Attributes["parameterValue"] != null)
                                {
                                    parameter = float.Parse(matrix.Attributes["parameterValue"].Value);
                                }

                                InteractionFieldMatrix m = new InteractionFieldMatrix(path);
                                
                                IF.addMatrix(parameter, m);
                            }
                        }
                        
                        
                        InteractionFieldsDictionnaire.Add(id, IF);
                        //IF.displayedIF.initDisplay(maxRow, maxCol, Color.blue, 0.02f, IF.getMatrices()[0], id);
                    }

                }
            }
            catch (FormatException e){
                Console.WriteLine(e.Message);
            }

        }
        //public void activateFieldsDisplay(int numField,bool isVelocity, int IdAgent)
        //{
        //    if(!isVelocity)
        //    {
        //        int IDField = UMANS_fieldsOrientationID(sim, IdAgent, numField, true);
        //        InteractionFieldsDictionnaire[IDField].IsSelected = true;
        //        InteractionFieldsDictionnaire[IDField].setSelected();
        //    }
        //    else
        //    {
        //        int IDField = UMANS_fieldsVelocityID(sim, IdAgent, numField, true);
        //        InteractionFieldsDictionnaire[IDField].IsSelected = true;
        //        InteractionFieldsDictionnaire[IDField].setSelected();
        //    }

        //}

    }
}
