using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CrowdMP.Core
{
    public class UIManager : MonoBehaviour
    {
        private int numIFVel;
        private int numIFOr;
        private List<float> parameters;
        private bool sketching;


        public List<float> Parameters { get => parameters; set => parameters = value; }
        public int NumIFVel { get => numIFVel; set => numIFVel = value; }
        public int NumIFOr { get => numIFOr; set => numIFOr = value; }
        public bool Sketching { get => sketching; set => sketching = value; }


        public void Start()
        {
           // initUI();  

        }

        

        public void initUI()
        {
            
            sketching = false;

            Slider sliderNumberIF = transform.Find("numberIFSlider").GetComponent<Slider>();
            sliderNumberIF.minValue = 0;
            //if (!orientation.isOn)
            //{
            //    if (NumIFVel > 0)
            //    {
            //        this.GetComponentInParent<ViewInteractionField>().IsVelocity = true;
            //        sliderNumberIF.gameObject.SetActive(true);
            //        sliderNumberIF.maxValue = NumIFOr - 1;
            //    }
            //    else
            //    {
            //        sliderNumberIF.gameObject.SetActive(false);
            //    }

            //}
            //else
            //{
            
            if(numIFOr <= 0)
            {
                transform.Find("Orientation").GetComponent<Toggle>().gameObject.SetActive(false);
            }
            else
            {
                if (numIFVel > 0)
                {
                    
                    sliderNumberIF.gameObject.SetActive(true);
                    sliderNumberIF.maxValue = NumIFVel - 1;
                }
                else
                {
                    sliderNumberIF.gameObject.SetActive(false);
                }
            }

            //}

        }


        public void Update()
        {
            if(Parameters != null)
            {
                if(parameters.Count != 0)
                {
                    Slider sliderParam = transform.Find("ParametricIFSlider").GetComponent<Slider>();
                    sliderParam.minValue = 0;
                    sliderParam.maxValue = Parameters.Count - 1;
                }

            }
            if (sketching)
            {
                transform.Find("ParametricIFSlider").GetComponent<Slider>().gameObject.SetActive(true);
            }
            else
            {
                transform.Find("ParametricIFSlider").GetComponent<Slider>().gameObject.SetActive(false);
            }

        
        }

        public float getParameter()
        {
            if (parameters.Count != 0)
            {

                Slider sliderParam = transform.Find("ParametricIFSlider").GetComponent<Slider>();
                return parameters[(int)sliderParam.value];
            }
            return 0f;
        }
        public void changeParameter()
        {
            if (parameters.Count != 0)
            {
                
                Slider sliderParam = transform.Find("ParametricIFSlider").GetComponent<Slider>();
                Debug.Log("paramter value " + (int)sliderParam.value);
                sliderParam.gameObject.GetComponentInChildren<Text>().text = " " + parameters[(int)sliderParam.value];
                GetComponentInParent<ViewInteractionField>().IF1.Parameter = parameters[(int)sliderParam.value];
                GetComponentInParent<ViewInteractionField>().UpdateViewMatrix();
            }

        }

        public void changenNumIF()
        {
            Slider sliderNumberIF = transform.Find("numberIFSlider").GetComponent<Slider>();
            if (sliderNumberIF != null && this.GetComponentInParent<ViewInteractionField>()!=null)
            {
                sliderNumberIF.minValue = 0;
                if (this.GetComponentInParent<ViewInteractionField>().IsVelocity)
                {
                    if (NumIFVel > 0)
                    {
                        this.GetComponentInParent<ViewInteractionField>().IsVelocity = true;
                        this.GetComponentInParent<ViewInteractionField>().NumIF = (int)sliderNumberIF.value;
                    }


                }
                else
                {
                    if (numIFOr > 0)
                    {
                        this.GetComponentInParent<ViewInteractionField>().IsVelocity = false;
                        this.GetComponentInParent<ViewInteractionField>().NumIF = (int)sliderNumberIF.value;
                    }
                }
                GetComponentInParent<ViewInteractionField>().UpdateViewMatrix();
            }
            
        }

        public void OrienationToggleSwitch()
        {

            Slider sliderNumberIF = transform.Find("numberIFSlider").GetComponent<Slider>();
            if(sliderNumberIF != null && this.GetComponentInParent<ViewInteractionField>())
            {
                sliderNumberIF.minValue = 0;
                this.GetComponentInParent<ViewInteractionField>().IsVelocity = !transform.Find("Orientation").GetComponent<Toggle>().isOn;

                    if (NumIFVel > 0 && this.GetComponentInParent<ViewInteractionField>().IsVelocity)
                    {
                        sliderNumberIF.gameObject.SetActive(true);
                        sliderNumberIF.maxValue = NumIFVel - 1;
                    }
                    else if(NumIFOr>0 && !this.GetComponentInParent<ViewInteractionField>().IsVelocity)
                    {
                        sliderNumberIF.gameObject.SetActive(true);
                        sliderNumberIF.maxValue = NumIFOr - 1;
                        

                    }

                
                    else
                    {

                        sliderNumberIF.gameObject.SetActive(false);
                    }
                this.GetComponentInParent<ViewInteractionField>().switchColor();
                GetComponentInParent<ViewInteractionField>().UpdateViewMatrix();
            }
                

            

        }

        public void EraserToggleSwitch()
        {
            if (transform.Find("eraser").GetComponent<Toggle>() != null)
            {
                
                Toggle isErasing = transform.Find("eraser").GetComponent<Toggle>();
                try
                {
                    GetComponentInParent<ViewInteractionField>().vr_InteractionField.GetComponent<IFDisplayManager>().Eraser = isErasing.isOn;
                }

                catch
                {

                }
                    

            }



        }


        public void OnCloseClick()
        {
            sketching = false;
            this.GetComponentInParent<ViewInteractionField>().IsSelected = false;
            
            this.gameObject.SetActive(false);
        }


        public void OnDeleteZeroArea()
        {
            float parameter = getParameter();
            this.GetComponentInParent<ViewInteractionField>().eraseZeroAreas(parameter);
        }
        public void OnsketchClick()
        {
            Button sketch = transform.Find("sketch").GetComponent<Button>();

            if (GameObject.FindGameObjectWithTag("GameManager").GetComponent<MainManager>().sketchBreak)
            {
                GameObject.FindGameObjectWithTag("GameManager").GetComponent<MainManager>().sketchBreak = false;
                GetComponentInParent<ViewInteractionField>().vr_InteractionField.GetComponent<IFDisplayManager>().IsSel = false;
                sketch.gameObject.GetComponentInChildren<Text>().text = "Sketch";
                GetComponentInParent<ViewInteractionField>().UpdateViewMatrix();
                GetComponentInParent<ViewInteractionField>().UpatrixMatrixFile();
                GetComponentInParent<ViewInteractionField>().MatrixSwith = true;
                GetComponentInParent<ViewInteractionField>().vr_InteractionField.GetComponent<IFDisplayManager>().show();
               


            }
            else
            {
                GameObject.FindGameObjectWithTag("GameManager").GetComponent<MainManager>().sketchBreak = true;
                GetComponentInParent<ViewInteractionField>().vr_InteractionField.GetComponent<IFDisplayManager>().IsSel = true;
                sketch.gameObject.GetComponentInChildren<Text>().text = "Stop sketching";
                GetComponentInParent<ViewInteractionField>().UpdateViewMatrix();
                GetComponentInParent<ViewInteractionField>().vr_InteractionField.GetComponent<IFDisplayManager>().BreakPosition = GetComponentInParent<ViewInteractionField>().vr_InteractionField.transform.position ;
                GetComponentInParent<ViewInteractionField>().vr_InteractionField.GetComponent<IFDisplayManager>().BreakRotation = GetComponentInParent<ViewInteractionField>().vr_InteractionField.transform.rotation ;
                //GetComponentInParent<ViewInteractionField>().vr_InteractionField.GetComponent<IFDisplayManager>().BreakPosition = GetComponentInParent<ViewInteractionField>().vr_InteractionField.transform.position+ new Vector3(0,0.05f,0);
                
            }
            

        }
    }
}