﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program.If not, see<https://www.gnu.org/licenses/>.
**  
**  Authors: Adèle Colas
**  Contact: crowd_group@inria.fr
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using System.Xml.Serialization;
using System.Runtime.InteropServices;


namespace CrowdMP.Core
{
    public class ControlVector : MonoBehaviour
    {
        private LineRenderer lr;
        private List<GameObject> handles;
        public GameObject handle;

        public List<GameObject> Handles { get => handles; set => handles = value; }



        private void Awake()
        {
            lr = this.GetComponent<LineRenderer>();
            handles = new List<GameObject>();
        }

        public void SetUpLine(List<Vector3> points)
        {
            lr.positionCount = handles.Count; 

          
        }
        public Vector3 getLastHandle()
        {
            return handles[handles.Count - 1].transform.position;
        }


        public void addHandle(Vector3 posNewHandle)
        {
            if (Vector3.Distance(posNewHandle, getLastHandle()) > 0.1)
            {
                AddPoint(posNewHandle);
            }
        }

        public void removeHandle(GameObject h)
        {
            handles.Remove(h);
            if (handles.Count < 2)
            {
                Destroy(gameObject);
                this.transform.GetComponentInParent<IFDisplayManager>().destroyCurve(gameObject);
            }
        }

        private void Update()
        {
            checkHandles();
            if (handles.Count > 1)
            {
                lr.positionCount = handles.Count + 2;


                for (int i = 0; i < handles.Count - 1; i++)
                {
                    if (handles[i] != null)
                    {
                        lr.SetPosition(i, handles[i].transform.position);
                    }

                }
                lr.SetPosition(handles.Count - 1, Vector3.Lerp(handles[handles.Count - 2].transform.position, handles[handles.Count - 1].transform.position, 0.999f - 0.4f));
                lr.SetPosition(handles.Count, Vector3.Lerp(handles[handles.Count - 2].transform.position, handles[handles.Count - 1].transform.position, 1 - 0.4f));
                lr.SetPosition(handles.Count + 1, handles[handles.Count - 1].transform.position);
                lr.widthCurve = new AnimationCurve(
                                  new Keyframe(0, 0.1f)
                                  , new Keyframe(0.999f - 0.4f, 0.1f)  // neck of arrow
                                  , new Keyframe(1 - 0.4f, 0.2f)  // max width of arrow head
                                  , new Keyframe(1, 0f));  // tip of arrow
            }
            else
            {
                lr.positionCount = handles.Count ;
                for (int i = 0; i < handles.Count; i++)
                {
                    lr.SetPosition(i, handles[i].transform.position);
                }
            }
            this.transform.GetComponentInParent<ViewInteractionField>().UpdateafterSketch();
        }
        

        private void checkHandles()
        {
            for (int i = 0; i < handles.Count; i++)
            {
                if (handles[i] == null)
                {
                    removeHandle(handles[i]);
                }
            }
        }

        public void AddPoint(Vector3 pt)
        {
            handle = Instantiate(handle,  pt , Quaternion.identity, gameObject.transform.parent.transform) ;
            handle.transform.eulerAngles = new Vector3(0, 180f, 0);
            handle.name = "handle_ " + Handles.Count;
            Handles.Add(handle);

        }

        public void Fill(Vector2D[] points)
        {
            
            for (int i = 0; i < points.Length; i++)
            {

                handle = Instantiate(handle, new Vector3(0,0,0), Quaternion.identity, gameObject.transform);
                handle.transform.localPosition = new Vector3(points[i].x, 0.1f, points[i].y);

                handle.name = "handle_ " + Handles.Count;
                Handles.Add(handle);

            }
        }

        public void closeCurve()
        {
            lr.endWidth = 0.01f;
            Handles[Handles.Count - 1].transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);

        }

        public Vector2D[] pointsCurve(Vector2D offset)
        {
            checkHandles();
            Vector2D[] resultsPoints = new Vector2D[Handles.Count];

            for (int i =0; i< resultsPoints.Length; i++)
            {
                //rotate the handles around the source.
                //Vector3 dir = handles[i].transform.position - new Vector3(offset.x, 0, offset.y); // get point direction relative to pivot
                //dir = Quaternion.Euler(new Vector3(0,180,0)+transform.parent.rotation.eulerAngles) * dir; // rotate it
                //point = dir + pivot; 
                //return point; // return it
                Vector3 handlePose = handles[i].transform.position;
                
                //Vector3 handlePose = dir + new Vector3(offset.x, 0, offset.y); // calculate rotated point
                resultsPoints[i] = new Vector2D((handlePose - new Vector3(-offset.x, 0, -offset.y)).x, (handlePose - new Vector3(-offset.x, 0, -offset.y)).z);
               
            }
            return resultsPoints;
        }

    }

}
