﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime;
using UnityEngine;

namespace CrowdMP.Core
{


    /// <summary>A 2D vector, used for representing positions, velocities, etc.</summary>

    public class Vector2D
    {

        /// <summary>The x component of this vector.</summary>
        public float x;
        /// <summary>The y component of this vector.</summary>
        public float y;

        /// <summary>Creates a Vector2D with both components set to zero.</summary>
        public Vector2D() {
            x = 0.0f;
            y = 0.0f;
        }

        public Vector2D(Vector2 vect)
        {
            x = vect.x;
            y = vect.y;
        }
        /// <summary>Creates a Vector2D with the given x and y components.</summary>
        /// <param name="x">The desired x component of the vector.</param>
        /// <param name="y">The desired y component of the vector.</param>
        public Vector2D(float xVal, float yVal) {
            x = xVal;
            y = yVal;
        }
        	/// <summary>Checks and returns whether this Vector2D is the zero vector.</summary>
	/// <returns>true if both x and y are 0; false otherwise.</returns>
        public bool isZero()
	    {
		    return x == 0 && y == 0;
	    }
        /// <summary>Computes and returns the magnitude of this Vector2D.</summary>
        /// <returns>The vector's magnitude, i.e. sqrt(x*x + y*y).</returns>
         public float magnitude() { return (float)Math.Sqrt(x * x + y * y); }


        /// <summary>Computes and returns the squared magnitude of this Vector2D.</summary>
        /// <returns>The vector's squared magnitude, i.e. x*x + y*y.</returns>
        public float sqrMagnitude() { return x * x + y * y; }


        /// <summary>Normalizes this Vector2D to unit length, by dividing both x and y by the magnitude.</summary>
        public void normalize()
        {
            float mag = magnitude();
            if (mag > 0)
            {
                x /= mag;
                y /= mag;
            }
        }

        /// <summary>Computes and returns a Vector2D that is the normalized version of the current vector.</summary>
        /// <returns>A Vector2D with the same direction as the current vector, but with unit length.</returns>
        public Vector2D getnormalized()
        {
            Vector2D Result= new Vector2D(x, y);
            Result.normalize();
            return Result;
        }

        /// <summary>Computes and returns the dot product of this Vector2D with another Vector2D.</summary>
        /// <param name="other">Another vector.</param>
        /// <returns>The dot product of the current Vector2D and 'other', i.e. x*other.x + y*other.y.</returns>
        public float dot(Vector2D other)
        {
            return x * other.x + y * other.y;
        }

        /// <summary>Adds another Vector2D to the current Vector2D.</summary>
        /// <param name="rhs">Another vector.</param>
        /// <returns>A reference to the current Vector2D after the operation has been performed.</returns>
        public static Vector2D operator +(Vector2D rhs, Vector2D rhsBis)
        {
            float a = rhsBis.x + rhs.x;float b = rhsBis.y + rhs.y;
            return new Vector2D(a,b);
        }

        /// <summary>Subtracts another Vector2D to the current Vector2D.</summary>
        /// <param name="rhs">Another vector.</param>
        /// <returns>A reference to the current Vector2D after the operation has been performed.</returns>
        public static Vector2D operator -(Vector2D rhs, Vector2D rhsBis)
        {
            float a =  rhs.x - rhsBis.x; float b =  rhs.y - rhsBis.y ;
            return new Vector2D(a, b);
            
        }



        public static Vector2D operator / (Vector2D lhs, float rhs)
        {
            return (1.0f / rhs) * lhs;
        }

        public static bool operator ==(Vector2D p,  Vector2D q)
        {
            return p.x == q.x && p.y == q.y;
        }

        public static bool operator !=( Vector2D p,  Vector2D q)
        {
            return p.x != q.x || p.y != q.y;
        }

        public static  Vector2D operator *(float lhs,  Vector2D rhs)
        {
            return new Vector2D(rhs.x* lhs, rhs.y* lhs);
        }

        public static  Vector2D operator *( Vector2D lhs, float rhs)
        {
            return rhs * lhs;
        }


        public  float distance(Vector2D p, Vector2D q)
        {
            return (p - q).magnitude();
        }

        public static float distanceSquared(Vector2D p, Vector2D q)
        {
            return (p - q).sqrMagnitude();
        }

        public static Vector2D rotateCounterClockwise(Vector2D v, float radians)
        {
            double Cos = Math.Cos(radians), Sin = Math.Sin(radians);
            return new Vector2D((float)(Cos * v.x - Sin * v.y), (float)(Sin * v.x + Cos * v.y));
        }

        public static Vector2D rotateCounterClockwiseAroundPoint(Vector2D v, Vector2D pivot, float radians)
        {
            return pivot + rotateCounterClockwise(v - pivot, radians);
        }

        // region [Angle-related functions]



        public static float angle(Vector2D va, Vector2D vb)
        {
        double lengths = va.magnitude() * vb.magnitude();
        if (lengths == 0)
            return 0.0f;

        double frac = (va.dot(vb) / lengths);

        // check if frac is out of range (this can happend due to numerical imprecision)
        if (frac < -1 || frac > 1)
            return 0.0f;

        return (float)Math.Acos(frac);
        }

        public static float cosAngle(Vector2D va, Vector2D vb)
        {
            double lengths = va.magnitude() * vb.magnitude();
            if (lengths == 0)
                return 0.0f;

            return (float)(va.dot(vb) / lengths);
        }

        public static bool isClockwise(Vector2D vector1,  Vector2D vector2)
        {
            float cross = vector1.x * vector2.y - vector1.y * vector2.x;
            return cross < 0;
        }

        public static bool isCounterClockwise(Vector2D vector1, Vector2D vector2)
        {
            float cross = vector1.x * vector2.y - vector1.y * vector2.x;
            return cross > 0;
        }

        public static float counterClockwiseAngle( Vector2D va, Vector2D vb)
        {
            float ang = angle(va, vb);
            if (isClockwise(va, vb))
                return -ang;
            return ang;
        }

        public static float clockwiseAngle( Vector2D va,  Vector2D vb)
        {
            float ang = angle(va, vb);
            if (isCounterClockwise(va, vb))
                return -ang;
            return ang;
        }





        public Vector2D clampVector(Vector2D v, float maxLength)
        {
            //if (maxLength == Math.Max)
            //    return v;

            float mag = v.magnitude();
            if (mag <= maxLength)
                return v;
            return v / mag * maxLength;
        }


   

    }


}
