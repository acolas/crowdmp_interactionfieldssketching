﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Runtime;
using System.IO;
using System;
using UnityEngine;

namespace CrowdMP.Core
{

    public class InteractionFieldMatrix
    {
        public Vector2D[,] matrix = new Vector2D[20, 20];
        private List<Vector2D[]> controlCurves;
        private Vector2D sourcePos;
        private string path { get; set; }
        public float unitH { get; set; }
        public float unitW { get; set; }
        private List<int[]> zeros;
        private Link link { get; set; }
        public List<Vector2D[]> ControlCurves { get => controlCurves; set => controlCurves = value; }
        public List<int[]> Zeros { get => zeros; set => zeros = value; }

        public struct Link
        {

            public string linkType;
            public Vector2D basePosition_otherObject;
            public int idOther;
            public Link(string type, Vector2D basePosOth, int id)
            {
                linkType = type;
                basePosition_otherObject = basePosOth;
                idOther = id;
            }
            public Link(string type, Vector2D basePosOth)
            {
                linkType = type;
                basePosition_otherObject = basePosOth;
                idOther = 0;
            }
        }

        public InteractionFieldMatrix(int rows, int cols)
        {
            matrix = new Vector2D[rows, cols];
            Zeros = new List<int[]>();
            ControlCurves = new List<Vector2D[]>();
        }
        public InteractionFieldMatrix( string path_)
        {

            path = path_;
            Zeros = new List<int[]>();
            ControlCurves = new List<Vector2D[]>();
            readFieldFileControlVect();
            InterpolateMatrix();
            
        }

        
       
        public Vector2D getSourcePosition() { return sourcePos; }

        

        public static InteractionFieldMatrix operator +(InteractionFieldMatrix A, InteractionFieldMatrix B)
        {
            if ((A.matrix.GetLength(0) == B.matrix.GetLength(0)) && (A.matrix.GetLength(1) == B.matrix.GetLength(1)))
            {
                for (int i = 0; i < A.matrix.GetLength(0); ++i)
                {
                    for (int j = 0; j < A.matrix.GetLength(1); ++j)
                    {
                        A.matrix[i, j] += B.matrix[i, j];
                    }
                }
                return A;
            }
            else
            {
                Debug.LogError("Impossible to add 2 matrix of different size");
                return null;
            }
        }


        public static InteractionFieldMatrix operator -(InteractionFieldMatrix A, InteractionFieldMatrix B)
        {
            if ((A.matrix.GetLength(0) == B.matrix.GetLength(0)) && (A.matrix.GetLength(1) == B.matrix.GetLength(1)))
            {
                for (int i = 0; i < A.matrix.GetLength(0); ++i)
                {
                    for (int j = 0; j < A.matrix.GetLength(1); ++j)
                    {
                        A.matrix[i, j] -= B.matrix[i, j];
                    }
                }
                return A;
            }
            else
            {
                Debug.LogError("Impossible to substract 2 matrix of different size");
                return null;
            }
        }

        public void changeSize(int newRow, int newCol)
        {
            matrix = new Vector2D[newRow, newCol];
        }

        public bool isNul()
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (!(matrix[i, j].isZero()))
                    {
                        return false;

                    }
                }
            }
            return true;
        }
        void setToNul()
        {

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    matrix[i, j]= new Vector2D(0f,0f);
                }
            }
        }
        public int getColomns()
        {
            return matrix.GetLength(1);
        }
        public int getRows()
        {
            return matrix.GetLength(0);
        }
        /**
     * @brief interpolation inverse distance with continuous distance:more accurate
     *
     * @param p parameter of the inverse distance
     * @param unitH_ height of one cell
     * @param unitW_ width of one cell
     * @param originPos position of the top left corner of the matrix in the scene
     * @param vecPos positions of the control vectors
     * @param controlVectors control vectors
     * @param magnitude desired magnitude for the obtained vectors
     */
        public void interpolationInverseDistanceEuclidian(float p, float unitH_, float unitW_, Vector2D originPos, Vector2D[] vecPos, Vector2D[] controlVectors, float magnitude)
        {


            if (p < 0 || p > 2)
            {

                Debug.LogError("Out of range value for p value interpolation matrix Inverse distance");
            }
            else if (controlVectors == null || controlVectors.Length == 0)
            {

                setToNul();
            }
            else
            {
                originPos = originPos + new Vector2D(unitW_ / 2.0f, -unitH_ / 2.0f);
                for (int i = 0; i < matrix.GetLength(0); i++)
                {
                    for (int j = 0; j < matrix.GetLength(1); j++)
                    {
                        Vector2D pos = new Vector2D(originPos.x + j * unitW_, originPos.y - i * unitH_);
                        Vector2D num =new Vector2D(0f,0f);
                        float denom = 0f;

                        for (int k = 0; k < controlVectors.Length; k++)
                        {

                            Vector2D vectorControl = controlVectors[k];
                            Vector2D vectControlPosition = vecPos[k];
                            float w = (float)Math.Pow(1 / (vectControlPosition - pos).magnitude(), p);
                            denom += w;
                            num = num + w * vectorControl;
                        }
                        Vector2D res = (num / denom).getnormalized() * magnitude;
                        matrix[i, j] = res;
                    }
                }

            }
        }

        /**
* @brief interpolation inverse distance with continuous distance:more accurate
*
* @param p parameter of the inverse distance
* @param unitH_ height of one cell
* @param unitW_ width of one cell
* @param originPos position of the top left corner of the matrix in the scene
* @param vecPos positions of the control vectors
* @param controlVectors control vectors
*/
        public void interpolationInverseDistanceEuclidian(float p, float unitH_, float unitW_, Vector2D[] vecPos, Vector2D[] controlVectors)
        {



            if (p < 0 || p > 2)
            {

                Debug.LogError("Out of range value for p value interpolation matrix Inverse distance");
            }
            else if (controlVectors == null || controlVectors.Length == 0)
            {

                setToNul();
            }
            else
            {

                //Vector2D originPos = new Vector2D(unitW_ / 2.0f, -unitH_ / 2.0f);
                for (int i = 0; i < matrix.GetLength(0); i++)
                {
                    for (int j = 0; j < matrix.GetLength(1); j++)
                    {
                        
                        Vector2D res = new Vector2D(0, 0);

                        if (!isinZero(new int[2] { i, j })){
                            
                            Vector2D pos = new Vector2D((j + 0.5f) * unitW_, (getRows() - i - 0.5f) * unitH_);
                            Vector2D num = new Vector2D(0f, 0f);
                            float denom = 0f;

                            for (int k = 0; k < controlVectors.Length; k++)
                            {

                                Vector2D vectorControl = controlVectors[k];
                                Vector2D vectControlPosition = vecPos[k];
                                float w = (float)Math.Pow(1 / (vectControlPosition - pos).magnitude(), p);
                                denom += w;
                                num = num + w * vectorControl;
                            }
                            res = (num / denom);


                        }

                        matrix[i, j] = res;

                    }
                }

            }
        }
        /**
 * @brief get the vector at a certain position after Inversedistance itnerpolation interpolation: not used
 * the magnitude is chosen by the control vectors
 * @param p parameter of the interpolation
 * @param position position of the vector
 * @param vectPos positons of the control vectors
 * @param vectControl control vectors
 * @return const Vector2D value pf the final vector
 */
        public Vector2D interpolationInverseDistanceEuclidianAtPosition(float p, Vector2D position, List<Vector2D> vectPos, List<Vector2D> vectControl)
        {
            Vector2D num = new Vector2D(0f, 0f);
            float denom = 0f;
            for (int k = 0; k < vectControl.Count; k++)
            {

                Vector2D vectorControl = vectControl[k];
                Vector2D vectControlPosition = vectPos[k];

                float mag = (float)(vectorControl.magnitude());


                float w = (float)Math.Pow(1 / (float)((vectControlPosition - position).magnitude()), p);
                denom += w;
                //                        Vector2D vect = mat[ind[0]][ind[1]];

                num = num + w * vectorControl;
            }
            return num / denom;

        }

        /**
         * @brief same with given magnitude
         *
         * @param float
         * @param position
         * @param vectPos
         * @param vectControl
         * @param magnitude
         * @return const Vector2D
         */
        public Vector2D interpolationInverseDistanceEuclidianAtPosition(float p, Vector2D position, List<Vector2D> vectPos, List<Vector2D> vectControl, float magnitude)
        {
            Vector2D num = new Vector2D(0f, 0f);
            float denom = 0f;
            for (int k = 0; k < vectControl.Count; k++)
            {

                Vector2D vectorControl = vectControl[k];
                Vector2D vectControlPosition = vectPos[k];

                float mag = (float)(vectorControl.magnitude());


                float w = (float)Math.Pow(1 / (float)((vectControlPosition - position).magnitude()), p);
                denom += w;
                //                        Vector2D vect = mat[ind[0]][ind[1]];

                num = num + w * vectorControl;
            }
            return (num / denom).getnormalized() * magnitude;

        }

        public void InterpolateMatrix()
        {
            List<Vector2D> vecPos = new List<Vector2D>();
            List<Vector2D> vecValues = new List<Vector2D>();

            foreach(Vector2D[] curve in controlCurves){
                for(int i =0; i<curve.Length-1; i++)
                {
                    vecPos.Add(curve[i]);
                    Vector2D vecValue = curve[i + 1] - curve[i];
                    vecValues.Add(vecValue);
                }
            }
            interpolationInverseDistanceEuclidian(1.9f, unitH, unitW,vecPos.ToArray(), vecValues.ToArray());
        }

        /**
         * @brief get the vector at a certain position after Inversedistance itnerpolation interpolation: not used
         * the magnitude is chosen by the control vectors
         * @param p parameter of the interpolation
         * @param position position of the vector
         * @param vectPos positons of the control vectors
         * @param vectControl control vectors
         * @return const Vector2D value pf the final vector
         */
        public Vector2D interpolationInverseDistanceEuclidianAtPosition(float p, Vector2D Pos, Vector2D[] vecPos, Vector2D[] controlVectors)
        {

            if (p < 0 || p > 2)
            {

                Debug.LogError("Out of range value for p value interpolation matrix Inverse distance");
            }
            else if (controlVectors == null || controlVectors.Length == 0)
            {

                return new Vector2D(0f, 0f);
            }
            Vector2D num = new Vector2D(0f,0f);
            float denom = 0;
            for (int k = 0; k < controlVectors.Length; k++)
            {

                Vector2D vectorControl = controlVectors[k];
                Vector2D vectControlPosition = vecPos[k];
                float w = (float)Math.Pow(1 / (vectControlPosition - Pos).magnitude(), p);
                denom += w;
                num = num + w * vectorControl;
            }
            return num / denom;

        }
        
        public bool readFieldFileMatrix()
        {

            string[] lines = System.IO.File.ReadAllLines(path);


            String[] lineParsed = lines[0].Split(";");
            

            try
            {
                int r = Int32.Parse(lineParsed[0]);
                int c = Int32.Parse(lineParsed[1]);
                unitH = (float)Convert.ToDouble(lineParsed[2]);
                unitW = (float)Convert.ToDouble(lineParsed[3]);
                matrix = new Vector2D[r, c];
                
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
            }


            lineParsed = lines[1].Split(";");
            float xS = (float)Convert.ToDouble(lineParsed[0]);
            float yS = (float)Convert.ToDouble(lineParsed[1]);
            sourcePos = new Vector2D(xS, yS);
            if (lineParsed.Length > 2)
            {
                if (lineParsed[2] == "r")
                {
                    link = new Link(lineParsed[2], new Vector2D((float)Convert.ToDouble(lineParsed[3]), (float)Convert.ToDouble(lineParsed[4])));
                }
                //the 2 other floats of this lines are the x and y position of the linked objcet, what do we do with it?
            }

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    lineParsed = lines[2+ 4*i + j].Split(";");
                    try
                    {
                        float x = (float)Convert.ToDouble(lineParsed[0]);
                        float y = (float)Convert.ToDouble(lineParsed[1]);
                        matrix[i, j] = new Vector2D(x, y);
                        if (matrix[i, j].isZero())
                        {
                            int[] z = new int[2] { i, j };
                            Zeros.Add(z);
                        }
                        
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
            return true;
        }

        public bool readFieldFileControlVect()
        {
            string[] lines = File.ReadAllLines(path);
            String[] lineParsed = lines[0].Split(";");
            try
            {
                int r = Int32.Parse(lineParsed[0]);
                int c = Int32.Parse(lineParsed[1]);
                unitH = (float)Convert.ToDouble(lineParsed[2]);
                unitW = (float)Convert.ToDouble(lineParsed[3]);
                matrix = new Vector2D[r, c];

            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
            }
            lineParsed = lines[1].Split(";");
            float xS = (float)Convert.ToDouble(lineParsed[0]);
            float yS = (float)Convert.ToDouble(lineParsed[1]);
            sourcePos = new Vector2D(xS, yS);
            if (lineParsed.Length > 2)
            {
                if (lineParsed[2] == "r")
                {
                    link = new Link(lineParsed[2], new Vector2D((float)Convert.ToDouble(lineParsed[3]), (float)Convert.ToDouble(lineParsed[4])));
                }
                //the 2 other floats of this lines are the x and y position of the linked objcet, what do we do with it?
            }
            int numberControlCurve = Int32.Parse(lines[2]);

            for(int i =0; i<numberControlCurve; i++)
            {
                lineParsed = lines[3 + i].Split(";");
                float coeff = (float)Convert.ToDouble(lineParsed[0]);
                Vector2D[] curve = new Vector2D[lineParsed.Length-1];
                for(int j=0; j< lineParsed.Length-1; j++)
                {
                    try
                    {
                        float X = (float)Convert.ToDouble(lineParsed[1+j].Split(" ")[0]); //offset of 1 if there is a coeff
                        float Y = (float)Convert.ToDouble(lineParsed[1+j].Split(" ")[1]);
                        Vector2D pt = new Vector2D(X, Y);

                        curve[j] = pt;
                        //curve[j] = Vector2D.rotateCounterClockwiseAroundPoint(pt, sourcePos, (float)Math.PI);
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    
                }
                controlCurves.Add(curve);
            }

            for(int k =0; k < lines.Length - numberControlCurve - 3; k++)
            {
                lineParsed = lines[3 + numberControlCurve + k].Split(";");
                try
                {
                    int indR = Int32.Parse(lineParsed[0]);
                    int indC = Int32.Parse(lineParsed[1]);
                    int[] z = new int[2] { indR, indC };
                    Zeros.Add(z);

                }

                catch (FormatException e)
                {
                    Console.WriteLine(e.Message);
                }

            }
            return true;

        }

        public bool writeFieldFileasMatrix()
        {
            string[] lines = new string[matrix.GetLength(0) * matrix.GetLength(1) + 2];
            String firstLine = matrix.GetLength(0).ToString() + ";" + matrix.GetLength(1).ToString() + ";" + unitH.ToString() + ";" + unitH.ToString();
            String secondLine = sourcePos.x.ToString() + ";" + sourcePos.y.ToString();

            if (link.linkType != null)
            {
                secondLine += "r" + ";" + link.basePosition_otherObject.x + ";" + link.basePosition_otherObject.y;
            }
            lines[0] = firstLine;
            lines[1] = secondLine;
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    lines[2 + i] = matrix[i, j].x + ";" + matrix[i, j].y;
                }

            }
            File.WriteAllLines(path, lines);
            return true;
        }

        public bool writeFieldFileasControlVects()
        {
            List<string> lines = new List<string>();
            String firstLine = matrix.GetLength(0).ToString() + ";" + matrix.GetLength(1).ToString() + ";" + unitH.ToString() + ";" + unitH.ToString();
            String secondLine = sourcePos.x.ToString() + ";" + sourcePos.y.ToString();

            if (link.linkType != null)
            {
                secondLine += "r" + ";" + link.basePosition_otherObject.x + ";" + link.basePosition_otherObject.y;
            }
            lines.Add(firstLine);
            lines.Add(secondLine);
            lines.Add(ControlCurves.Count.ToString());
            for (int i =0; i< ControlCurves.Count; i++)
            {               
                String line = "1";
                for (int j =0; j<ControlCurves[i].Length; j++)
                {
                    line+= ";" + ControlCurves[i][j].x.ToString() + " " + ControlCurves[i][j].y.ToString();
                }
                lines.Add(line);
            }
            foreach(var zer in Zeros)
            {
                lines.Add(zer[0] + ";" + zer[1]);
            }

            File.WriteAllLines(path, lines.ToArray());
            return true;
        }


        public bool isinZero(int[] ind)
        {
            foreach(int[] index in zeros)
            {
                if(index[0] == ind[0] && index[1] == ind[1])
                {
                    return true;
                }
            }
            return false;
        }
//! [3] //! [4]
    }


}

