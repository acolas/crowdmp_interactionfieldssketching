/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program.If not, see<https://www.gnu.org/licenses/>.
**  
**  Authors: Ad�le Colas
**  Contact: crowd_group@inria.fr
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using System.Xml.Serialization;
using System.Runtime.InteropServices;



namespace CrowdMP.Core
{
    public class ViewInteractionField : MonoBehaviour
    {
        // Start is called before the first frame update

        [XmlIgnore()]
        public GameObject[,] matrixView;
        public GameObject vr_InteractionField;
        public GameObject UI_IFediting;
        [XmlIgnore()]
        private bool isSelected;
        [XmlIgnore()]
        private InteractionField IF;
        [XmlIgnore()]
        private bool matrixSwith;
        [XmlIgnore()]
        private bool sketchChanged;
        

        private bool isVelocity;

        private int numIF;





        public bool IsSelected { get => isSelected; set => isSelected = value; }
        public InteractionField IF1 { get => IF; set => IF = value; }
        public bool MatrixSwith { get => matrixSwith; set => matrixSwith = value; }
        public bool SketchChanged { get => sketchChanged; set => sketchChanged = value; }
        public bool IsVelocity { get => isVelocity; set => isVelocity = value; }
        public int NumIF { get => numIF; set => numIF = value; }

        void Start()
        {
            vr_InteractionField = Instantiate(vr_InteractionField, new Vector3(0, 0, 0), Quaternion.identity);
            vr_InteractionField.transform.parent = this.gameObject.transform;
            vr_InteractionField.gameObject.tag = "IF";

            //vr_InteractionField.transform.Find("Cylinder").gameObject.SetActive(false);
           
            UI_IFediting = Instantiate(UI_IFediting, new Vector3(0, 0, 0), Quaternion.identity);
            UI_IFediting.transform.parent = this.gameObject.transform;
            
            IsVelocity = true;
            numIF = 0;
            
          
            
        }
        public void setSelected()
        {

            vr_InteractionField.SetActive(IsSelected);
            for (int i = 0; i < matrixView.GetLength(0); i++)
            {
                for (int j = 0; j < matrixView.GetLength(1); j++)
                {
                    matrixView[i, j].SetActive(IsSelected);

                }
            }
        }

        public void initDisplay(float width, int numberIFVel_, int numberIFOr_)
        {

            Color color = Color.blue;
            InteractionFieldMatrix matrix = IF.getMatrices().Values.ToList()[0];
            vr_InteractionField.name = "IF_" + IF.id;
            vr_InteractionField.transform.position = new Vector3(0, 0, 0);
            LineRenderer br = vr_InteractionField.GetComponent<LineRenderer>();
            br.material = new Material(Shader.Find("Sprites/Default"));
            br.startColor = color;
            br.endColor = color;
            br.startWidth = width;
            br.endWidth = width;
            br.SetPositions(new Vector3[4] {new Vector3(0,0,0), new Vector3(matrix.getColomns()*matrix.unitW,0,0), new Vector3(matrix.getColomns() * matrix.unitW, 0, matrix.getRows() * matrix.unitH),
                new Vector3(0, 0, matrix.getRows() * matrix.unitH)});
            

            matrixView = new GameObject[IF.getMaxRow(), IF.getMaxColumn() ];
            for (int i = 0; i < matrixView.GetLength(0); i++)
            {
                for (int j = 0; j < matrixView.GetLength(1); j++)
                {


                    GameObject myLine = new GameObject("vector_" + IF.id + "_" + i + "-" + j);

                    myLine.transform.parent = vr_InteractionField.transform;
                    myLine.transform.position = new Vector3(0, 0, 0);
                    myLine.gameObject.isStatic = true;
                    myLine.AddComponent<LineRenderer>();
                    LineRenderer lr = myLine.GetComponent<LineRenderer>();
                    lr.material = new Material(Shader.Find("Sprites/Default"));

                    lr.startColor = color;
                    lr.endColor = color;
                    lr.startWidth = width;
                    lr.endWidth = width;
                    lr.positionCount = 4;
                    
                    lr.useWorldSpace = false;

                    matrixView[i, j] = myLine;
                    if (i < matrix.getRows() && j < matrix.getColomns())
                    {
                        Vector3 start = new Vector3((j + 0.5f) * matrix.unitW, 0, (matrix.getRows() - i - 0.5f) * matrix.unitH);
                        Vector3 end = new Vector3(matrix.matrix[i, j].x, 0, matrix.matrix[i, j].y);
                        lr.widthCurve = new AnimationCurve(
                            new Keyframe(0, 0.1f)
                            , new Keyframe(0.999f - 0.4f, 0.1f)  // neck of arrow
                            , new Keyframe(1 - 0.4f, 0.2f)  // max width of arrow head
                            , new Keyframe(1, 0f));  // tip of arrow
                        lr.SetPositions(new Vector3[] {
                      start
                      , Vector3.Lerp(start, end, 0.999f - 0.4f)
                      , Vector3.Lerp(start, end, 1 - 0.4f)
                      , end });
                        
                        
                    }

                    //myLine.transform.eulerAngles = new Vector3(0, -180f, 0);


                }
            }
            UI_IFediting.GetComponent<UIManager>().NumIFOr = numberIFOr_;
            UI_IFediting.GetComponent<UIManager>().NumIFVel = numberIFVel_;
            UI_IFediting.GetComponent<UIManager>().initUI();
            UI_IFediting.SetActive(false);
            //vr_InteractionField.GetComponent<IFDisplayManager>().hide();

        }
        //public void setActivate()
        //{
        //    UI_IFediting.GetComponent
        //}
        public void setDesactivate()
        {
            UI_IFediting.SetActive(false);
        }
        public void updateToCurrentMatrix()
        {

            // find the matrix to use, or the two matrices to interpolate between

            //field.getMatricesForInterpolation(source, matrix0, matrix1, mu, world, linkObjects);
            InteractionFieldMatrix m1 = new InteractionFieldMatrix(20, 20);
            InteractionFieldMatrix m2 = new InteractionFieldMatrix(20, 20);
            float mu = 0;
            IF1.getInterpolationMatrices(ref m1, ref m2, ref mu);
            //// if there is only one matrix to use, visualize it in a straight-forward way

            // otherwise, draw an "interpolated matrix" that lies between matrix0 and matrix1.
            // The grid rectangle is an interpolation of matrix0 and matrix1, which may have different sizes.
            // We cannot draw one of these matrices directly. Instead, we should create a new grid and fill it with interpolated values.

            // - the rotation is interpolated between matrix0 and matrix1
            //float angle = (1 - mu) * angle0 + mu * angle1;

            // - the grid's origin is interpolated as well
            
            Vector2D sourcePosition;
            int cols;
            int rows;
            float cellWidth;
            float cellHeight;
            

            if (m2 != null)
            {
                sourcePosition = (1 - mu) * m1.getSourcePosition() + mu * m2.getSourcePosition();
                float h = (1 - mu) * m1.unitH * m1.getRows() + mu * m2.unitH * m2.getRows();
                float w = (1 - mu) * m1.unitW * m1.getColomns() + mu * m2.unitW * m2.getColomns();
                cols = (int)Math.Round((1 - mu) * m1.getColomns() + mu * m2.getColomns());
                rows = (int)Math.Round((1 - mu) * m1.getRows() + mu * m2.getRows());
                cellHeight = (1 - mu) * m1.unitH + mu * m2.unitH;
                cellWidth = (1 - mu) * m1.unitW + mu * m2.unitW;


                // fill in all grid points
                for (int i = 0; i < matrixView.GetLength(0); i++)
                {
                    for (int j = 0; j < matrixView.GetLength(1); j++)
                    {
                        if (j < cols && i < rows)
                        {
                            Vector2D basePosition =new Vector2D((j + 0.5f) * cellWidth, (rows - i - 0.5f) * cellHeight) - sourcePosition;
                            //Vector2D rotatedPosition = Vector2D.rotateCounterClockwiseAroundPoint(basePosition, sourcePosition, -angle);
                            //Vector2D worldPosition = rotatedPosition + translation;
                            matrixView[i, j].SetActive(true);
                            // compute the interpolated IF vector at this position
                            bool inside;
                            Vector2D v0 = new Vector2D(0, 0);
                            Vector2D v1 = new Vector2D(0, 0);
                            if (i < m1.getRows() && j < m1.getColomns())
                            {
                                v0 = m1.matrix[i, j];
                            }
                            if (i < m2.getRows() && j < m2.getColomns())
                            {
                                v1 = m2.matrix[i, j];
                            }

                            Vector2D IFVector = (1 - mu) * v0 + mu * v1;
                            Vector2D finalVector = IFVector.getnormalized() * Math.Min((1 - mu) * m1.unitW + mu * m2.unitW, (1 - mu) * m1.unitH + mu * m2.unitH) / 2;
                            Vector3 start = new Vector3(basePosition.x, 0f, basePosition.y);
                            Vector3 end =  new Vector3((basePosition + finalVector).x, 0f, (basePosition + finalVector).y);
                            LineRenderer lr = matrixView[i, j].GetComponent<LineRenderer>();

                            //lr.positionCount = 4;

                            lr.widthCurve = new AnimationCurve(
                                new Keyframe(0, 0.1f)
                                , new Keyframe(0.999f - 0.4f, 0.1f)  // neck of arrow
                                , new Keyframe(1 - 0.4f, 0.2f)  // max width of arrow head
                                , new Keyframe(1, 0f));  // tip of arrow
                            lr.SetPositions(new Vector3[] {
                              start
                              , Vector3.Lerp(start, end, 0.999f - 0.4f)
                              , Vector3.Lerp(start, end, 1 - 0.4f)
                              , end });

                            

                        }

                        else
                        {
                            matrixView[i, j].SetActive(false);
                        }
                        
                    }


                }
                  
            }
            else
            {
                

                sourcePosition = m1.getSourcePosition();
                
                cols = m1.getColomns();
                rows = m1.getRows();
                cellHeight = m1.unitH;
                cellWidth = m1.unitW;
                // fill in all grid points
                for (int i = 0; i < matrixView.GetLength(0); i++)
                {
                    for (int j = 0; j < matrixView.GetLength(1); j++)
                    {
                        if (j < cols && i < rows)
                        {
                            Vector2D basePosition = new Vector2D((j + 0.5f) * m1.unitW, (rows - i - 0.5f) * m1.unitH) - sourcePosition;
                            //Vector2D rotatedPosition = Vector2D.rotateCounterClockwiseAroundPoint(basePosition, sourcePosition, -angle);
                            matrixView[i, j].SetActive(true);
                            
                            Vector2D vector = m1.matrix[i, j];
                            Vector2D finalVector = vector.getnormalized() * Math.Min(cellWidth, cellHeight) / 2;
                            Vector3 start = new Vector3(basePosition.x, 0f, basePosition.y);
                            Vector3 end = new Vector3((basePosition + finalVector).x, 0f,(basePosition + finalVector).y);
                            LineRenderer lr = matrixView[i, j].GetComponent<LineRenderer>();
                            lr.widthCurve = new AnimationCurve(
                                new Keyframe(0, 0.1f)
                                , new Keyframe(0.999f - 0.4f, 0.1f)  // neck of arrow
                                , new Keyframe(1 - 0.4f, 0.2f)  // max width of arrow head
                                , new Keyframe(1, 0f));  // tip of arrow
                            lr.SetPositions(new Vector3[] {
                              start
                              , Vector3.Lerp(start, end, 0.999f - 0.4f)
                              , Vector3.Lerp(start, end, 1 - 0.4f)
                              , end });
                        }


                        else
                        {
                            matrixView[i, j].SetActive(false);
                        }

                        
                    }
                }

                
            }

            LineRenderer br = vr_InteractionField.GetComponent<LineRenderer>();
            br.SetPositions(new Vector3[4] {new Vector3(-sourcePosition.x,0,-sourcePosition.y), new Vector3(cols * cellWidth-sourcePosition.x,0,-sourcePosition.y), new Vector3(cols * cellWidth- sourcePosition.x, 0, rows * cellHeight-sourcePosition.y),
                new Vector3(-sourcePosition.x, 0,rows * cellHeight-sourcePosition.y)});
           
            vr_InteractionField.GetComponent<BoxCollider>().size = new Vector3(cols * cellWidth, 0, rows * cellHeight);
            
            // create the boundary points
            //grid.boundary = new List<Vector2D>();
            //Vector2D corner = new Vector2D(0, 0);
            //grid.boundary.Add(Vector2D.rotateCounterClockwiseAroundPoint(corner, sourcePosition, -angle) + translation);
            //grid.boundary.Add(Vector2D.rotateCounterClockwiseAroundPoint(corner + new Vector2D(w, 0), sourcePosition, -angle) + translation);
            //grid.boundary.Add(Vector2D.rotateCounterClockwiseAroundPoint(corner + new Vector2D(w, h), sourcePosition, -angle) + translation);
            //grid.boundary.Add(Vector2D.rotateCounterClockwiseAroundPoint(corner + new Vector2D(0, h), sourcePosition, -angle) + translation);




        }

        public void eraseZeroAreas(float parameter)
        {
            
            InteractionFieldMatrix m1 = IF.getMatrices()[parameter];
            m1.Zeros.Clear();
            UpdateafterSketch();
            m1.InterpolateMatrix();
            updateToCurrentMatrix();
        }

        public void switchColor()
        {
            Color color = Color.blue;

            if (!isVelocity)
            {
                color = Color.green;
            }
            vr_InteractionField.GetComponent<LineRenderer>().startColor = color;
            vr_InteractionField.GetComponent<LineRenderer>().endColor = color;
            for (int i = 0; i < matrixView.GetLength(0); i++)
            {
                for (int j = 0; j < matrixView.GetLength(1); j++)
                {
                    matrixView[i, j].GetComponent<LineRenderer>().startColor = color;
                    matrixView[i, j].GetComponent<LineRenderer>().endColor = color;
                }
            }
        }

        public void updateMatrix(Vector3 sourcePositionCurrent)
        {
            if (matrixSwith)
            {
                updateToCurrentMatrix();
                vr_InteractionField.GetComponent<CapsuleCollider>().transform.position = sourcePositionCurrent;
                vr_InteractionField.transform.Find("Cylinder").position = sourcePositionCurrent;
                UI_IFediting.GetComponent<UIManager>().Parameters = IF.getMatrices().Keys.ToList();
                

            }
            
            //if (GameObject.FindGameObjectWithTag("GameManager").GetComponent<MainManager>().sketchBreak && sketchChanged)
            //{
            //   // List<Vector2D[]> curves = GetComponentInChildren<IFDisplayManager>().getCurves(sourcePositionCurrent);

            //    this.IF.getMatrices()[IF.Parameter].ControlCurves = curves;

            //}

            vr_InteractionField.transform.position = new Vector3(sourcePositionCurrent.x, 0.05f, sourcePositionCurrent.z);
           
            vr_InteractionField.transform.eulerAngles = new Vector3(0,(float)(-IF1.Angle*180/Math.PI), 0);
            //vr_InteractionField.GetComponent<IFDisplayManager>().drawCurves(IF.getMatrices()[0].ControlCurves, IF.getMatrices()[0].getSourcePosition());


        }

        public void deleteVector(Vector2D basePosInWorld)
        {
            IF.Parameter = UI_IFediting.GetComponent<UIManager>().getParameter();
            InteractionFieldMatrix m1 = IF.getMatrices()[IF.Parameter];

            Vector2D sourcePosition = m1.getSourcePosition();
            
            int cols = m1.getColomns();
            int rows = m1.getRows();
            float cellHeight = m1.unitH;
            float cellWidth = m1.unitW;

            int i = System.Convert.ToInt32(Math.Round((-basePosInWorld.y - 0.5 + rows)/cellHeight - sourcePosition.y));
            int j = System.Convert.ToInt32(Math.Round((basePosInWorld.x - 0.5f) / cellWidth + sourcePosition.x));
            int[] indexes = new int[2] { i, j };
            if (!m1.isinZero(indexes))
            {
                m1.Zeros.Add(indexes);
            }
            
            

            UpdateafterSketch();
            m1.InterpolateMatrix();
            updateToCurrentMatrix();
        }

        //public void initDisplay(int r, int c, Color color, float width)
        //{

        //    vr_InteractionField = Resources.Load<GameObject>("vr_InteractionField");
        //    vr_InteractionField.name = "IF_" + id;
        //    vr_InteractionField.transform.position = new Vector3(0, 0, 0);
        //    vr_InteractionField.AddComponent<LineRenderer>();
        //    LineRenderer br = vr_InteractionField.GetComponent<LineRenderer>();
        //    br.material = new Material(Shader.Find("Sprites/Default"));
        //    br.startColor = color;
        //    br.endColor = color;
        //    br.startWidth = width;
        //    br.endWidth = width;
        //    br.positionCount = 5;
        //    
        //    vr_InteractionField.SetActive(true);

        //    matrixView = new GameObject[r,c];
        //    for(int i=0; i<matrixView.GetLength(0); i++)
        //    {
        //        for(int j =0; j<matrixView.GetLength(1); j++)
        //        {


        //            GameObject myLine = new GameObject("vector_"+id+"_"+i+"-"+j);

        //            myLine.transform.parent = vr_InteractionField.transform;
        //            myLine.transform.position = new Vector3(0, 0, 0);
        //            myLine.AddComponent<LineRenderer>();
        //            LineRenderer lr = myLine.GetComponent<LineRenderer>();
        //            lr.material = new Material(Shader.Find("Sprites/Default"));

        //            lr.startColor = color;
        //            lr.endColor = color;
        //            lr.startWidth = width;
        //            lr.endWidth = width;
        //            lr.positionCount = 4;
        //            lr.useWorldSpace = false;
        //            myLine.SetActive(false);
        //            matrixView[i, j] = myLine;
        //            if( i < matrices[0].getRows() &&  j < matrices[0].getColomns())
        //            {
        //                Vector3 start = new Vector3((j + 0.5f) * matrices[0].unitW, 0, (matrices[0].getRows() - i - 0.5f) * matrices[0].unitH);
        //                Vector3 end = new Vector3(matrices[0].matrix[i, j].x, 0, matrices[0].matrix[i, j].y);
        //                lr.widthCurve = new AnimationCurve(
        //                    new Keyframe(0, 0.1f)
        //                    , new Keyframe(0.999f - 0.4f, 0.1f)  // neck of arrow
        //                    , new Keyframe(1 - 0.4f, 0.2f)  // max width of arrow head
        //                    , new Keyframe(1, 0f));  // tip of arrow
        //                lr.SetPositions(new Vector3[] {
        //              start
        //              , Vector3.Lerp(start, end, 0.999f - 0.4f)
        //              , Vector3.Lerp(start, end, 1 - 0.4f)
        //              , end });
        //                lr.alignment = LineAlignment.TransformZ;
        //                lr.transform.eulerAngles = Vector3.up;

        //            }




        //        }
        //    }

        //}
        // Update is called once per frame
        public void UpatrixMatrixFile()
        {
            vr_InteractionField.GetComponent<IFDisplayManager>().destroyCurves();
            IF.Parameter = UI_IFediting.GetComponent<UIManager>().getParameter();
            IF.getMatrices()[IF.Parameter].writeFieldFileasControlVects();
        }

        public void UpdateViewMatrix()
        {

            IF.Parameter = UI_IFediting.GetComponent<UIManager>().getParameter();
            updateToCurrentMatrix();
            vr_InteractionField.transform.position = new Vector3(vr_InteractionField.GetComponent<CapsuleCollider>().transform.position.x, 0.05f, vr_InteractionField.GetComponent<CapsuleCollider>().transform.position.z);
            if (GameObject.FindGameObjectWithTag("GameManager").GetComponent<MainManager>().sketchBreak && vr_InteractionField.GetComponent<IFDisplayManager>().IsSel)
            {
                
                vr_InteractionField.GetComponent<IFDisplayManager>().destroyCurves();
                
                vr_InteractionField.GetComponent<IFDisplayManager>().drawCurves(IF.getMatrices()[IF.Parameter].ControlCurves, IF.getMatrices()[IF.Parameter].getSourcePosition());
               
            }
            else
            {
                vr_InteractionField.GetComponent<IFDisplayManager>().destroyCurves();
               
            }
        }

        public void UpdateafterSketch()
        {
            if (GameObject.FindGameObjectWithTag("GameManager").GetComponent<MainManager>().sketchBreak)
            {
                IF.Parameter = UI_IFediting.GetComponent<UIManager>().getParameter();
                IF.getMatrices()[IF.Parameter].ControlCurves = vr_InteractionField.GetComponent<IFDisplayManager>().getCurves(IF.getMatrices()[IF.Parameter].getSourcePosition());
                IF.getMatrices()[IF.Parameter].InterpolateMatrix();
                updateToCurrentMatrix();


            }

        }



        void LateUpdate()
        {
            
           
        }
    }
}
