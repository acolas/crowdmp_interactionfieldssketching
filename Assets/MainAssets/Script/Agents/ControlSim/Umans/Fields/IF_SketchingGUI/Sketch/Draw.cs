﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Xml.Serialization;
using System.Runtime;
//using System.Drawing;
using UnityEngine;

namespace CrowdMP.Core
{
    using IFVisualizationGridPoint = System.Collections.Generic.KeyValuePair<Vector2D, Vector2D>;
    public static class Draw
    {

        
        public struct IFVisualizationGrid
        {
            public List<List<IFVisualizationGridPoint>>gridPoints;
            public List<Vector2D> boundary;
        };
        public static void DrawLine(Vector3 start, Vector3 end, Color color, float width, float duration = 0.1f)
        {
            GameObject myLine = new GameObject("line");
            myLine.transform.position = start;
            myLine.AddComponent<LineRenderer>();
            LineRenderer lr = myLine.GetComponent<LineRenderer>();
            lr.material = new Material(Shader.Find("Sprites/Default"));
            lr.startColor = color;
            lr.endColor = color;
            lr.startWidth = width;
            lr.endWidth = width;
            lr.SetPosition(0, start);
            lr.SetPosition(1, end);
            GameObject.Destroy(myLine,duration);
        }

        public static void DrawVector(Vector3 start, Vector3 end, Color color, float duration = 0.1f)
        {
            float angle = (float)Math.Atan2(-(end - start).z, (end - start).x);
            //Vector3 destArrowP1 = end + new Vector3((float)Math.Sin(angle - (float)(Math.PI / 3)) * 0.07f, 0f, (float)Math.Cos(angle - (float)(Math.PI / 3)) * 0.07f);
            //Vector3 destArrowP2 = end + new Vector3((float)Math.Sin(angle - Math.PI + (float)(Math.PI / 3)) * 0.07f, 0f, (float)Math.Cos(angle - (float)(Math.PI + Math.PI / 3)) * 0.07f);

            Vector2D dir = new Vector2D((end - start).x, (end - start).z).getnormalized() * 0.05f;
            float arrowheadAngle = (float)(Math.PI + 0.4f);

            Vector3 destArrowP1 = new Vector3(Vector2D.rotateCounterClockwise(dir, arrowheadAngle).x, 0f, Vector2D.rotateCounterClockwise(dir, arrowheadAngle).y);
            Vector3 destArrowP2 = new Vector3(Vector2D.rotateCounterClockwise(dir, -arrowheadAngle).x, 0f, Vector2D.rotateCounterClockwise(dir, -arrowheadAngle).y);
            DrawLine(end, end + destArrowP1, Color.blue, 0.02f, duration);
            DrawLine(end, end + destArrowP2, Color.blue, 0.02f, duration);
            DrawLine(start, end, color, 0.02f, duration);
            
            
        }

        public static void updateArrow(Vector3 start, Vector3 end, ref GameObject arrow)
        {


            LineRenderer lr = arrow.GetComponent<LineRenderer>();

            //lr.positionCount = 4;

            lr.widthCurve = new AnimationCurve(
                new Keyframe(0, 0.1f)
                , new Keyframe(0.999f - 0.4f, 0.1f)  // neck of arrow
                , new Keyframe(1 - 0.4f, 0.2f)  // max width of arrow head
                , new Keyframe(1, 0f));  // tip of arrow
            lr.SetPositions(new Vector3[] {
              start
              , Vector3.Lerp(start, end, 0.999f - 0.4f)
              , Vector3.Lerp(start, end, 1 - 0.4f)
              , end });


        }
        public static void drawArrow(Vector3 start, Vector3 end, Color color, float width)
        {
            float angle = (float)Math.Atan2(-(end - start).z, (end - start).x);
            //Vector3 destArrowP1 = end + new Vector3((float)Math.Sin(angle - (float)(Math.PI / 3)) * 0.07f, 0f, (float)Math.Cos(angle - (float)(Math.PI / 3)) * 0.07f);
            //Vector3 destArrowP2 = end + new Vector3((float)Math.Sin(angle - Math.PI + (float)(Math.PI / 3)) * 0.07f, 0f, (float)Math.Cos(angle - (float)(Math.PI + Math.PI / 3)) * 0.07f);

            Vector2D dir = new Vector2D((end - start).x, (end - start).z).getnormalized() * 0.05f;
            float arrowheadAngle = (float)(Math.PI + 0.4f);

            Vector3 destArrowP1 = new Vector3(Vector2D.rotateCounterClockwise(dir, arrowheadAngle).x, 0f, Vector2D.rotateCounterClockwise(dir, arrowheadAngle).y);
            Vector3 destArrowP2 = new Vector3(Vector2D.rotateCounterClockwise(dir, -arrowheadAngle).x, 0f, Vector2D.rotateCounterClockwise(dir, -arrowheadAngle).y);
            GameObject myLine = new GameObject("line");
            myLine.transform.position = start;
            myLine.AddComponent<LineRenderer>();
            LineRenderer lr = myLine.GetComponent<LineRenderer>();
            lr.material = new Material(Shader.Find("Sprites/Default"));
            lr.positionCount = 4;
            lr.startColor = color;
            lr.widthCurve = new AnimationCurve(
                new Keyframe(0, 0.4f)
                , new Keyframe(0.999f - 0.4f, 0.4f)  // neck of arrow
                , new Keyframe(1 - 0.4f, 1f)  // max width of arrow head
                , new Keyframe(1, 0f));  // tip of arrow
            lr.SetPositions(new Vector3[] {
              start
              , Vector3.Lerp(start, end, 0.999f - 0.4f)
              , Vector3.Lerp(start, end, 1 - 0.4f)
              , end });
            GameObject.Destroy(myLine, 0.1f);

        }


        public static void updateBorder(Vector3[] corners,ref GameObject border)
        {
            LineRenderer lr = border.GetComponent<LineRenderer>();
            lr.SetPositions(corners);
        }


        public static void DrawField(ViewInteractionField field, Vector3 currentSourcePosition_, float parameter, float angle)
        {
            Vector2D currentSourcePosition = new Vector2D(currentSourcePosition_.x, currentSourcePosition_.z);
            InteractionFieldMatrix m1 = new InteractionFieldMatrix(20, 20);
            InteractionFieldMatrix m2 = new InteractionFieldMatrix(20, 20);
            float mu = 0;
            field.IF1.getInterpolationMatrices(ref m1, ref m2, ref mu);
            IFVisualizationGrid grid = BuildVisualizationGrid(currentSourcePosition, m1, m2, mu, angle);//put the right angles

            for (int i = 0; i < field.matrixView.GetLength(0); i++)
            {
                for (int j = 0; j < field.matrixView.GetLength(1) ; j++)
                {
                    if (grid.gridPoints.Count > i && grid.gridPoints.ElementAt(0).Count > j) {
                        

                        List<IFVisualizationGridPoint> cells = grid.gridPoints.ElementAt(i);
                        IFVisualizationGridPoint cell = cells.ElementAt(j);
                        if (cell.Key.isZero())
                            continue;
                        Vector3 startPoint = new Vector3(cell.Key.x, 0.1f, cell.Key.y);
                        Vector3 endPoint = new Vector3(((cell.Key + cell.Value)).x, 0.1f, ((cell.Key + cell.Value).y));
                        //drawArrow(startPoint, endPoint, Color.red, 0.2f);
                        updateArrow(startPoint, endPoint, ref field.matrixView[i, j]);
                        //field.matrixView[i, j].SetActive(true);
                    }
                    else
                    {
                        //field.matrixView[i, j].SetActive(false);
                    }
                }

            }
            //draw the grid boundary
            int num = grid.boundary.Count() * 2;
            Vector3[] corners = new Vector3[5];
            for (int i = 0; i < grid.boundary.Count(); i++)
            {

                corners[i] = new Vector3(grid.boundary.ElementAt(i).x, 0.1f, grid.boundary.ElementAt(i).y);

            }
            corners[4] = new Vector3(grid.boundary.ElementAt(0).x, 0.1f, grid.boundary.ElementAt(0).y);
            updateBorder(corners, ref field.vr_InteractionField);

            //foreach (List<IFVisualizationGridPoint> cells in grid.gridPoints)
            //{
            //    foreach (IFVisualizationGridPoint cell in cells){
            //        //skip any zero vectors
            //        if (cell.Key.isZero())
            //            continue;
            //        Vector3 startPoint = new Vector3(cell.Key.x, 0, cell.Key.y);
            //        Vector3 endPoint = new Vector3(((cell.Key + cell.Value)).x, 0f, ((cell.Key + cell.Value).y));
            //        float mag = (endPoint - startPoint).magnitude;                    
            //    }
            //}

            ////draw the grid boundary
            //for (int i = 0; i < grid.boundary.Count(); i++)
            //{
            //    Vector3 point1 = new Vector3(grid.boundary[i].x, 0f, grid.boundary[i].y);
            //    Vector3 point2 = new Vector3((grid.boundary[(i + 1) % grid.boundary.Count]).x, 0f, (grid.boundary[(i + 1) % grid.boundary.Count]).y);

            //    DrawLine(point1, point2, Color.blue, 0.05f);
               
            //}

        }





        public static IFVisualizationGrid BuildVisualizationGrid( Vector2D currentSourcePosition, InteractionFieldMatrix matrix0, InteractionFieldMatrix matrix1, float mu, float angle)
        {
            // find the matrix to use, or the two matrices to interpolate between

            //field.getMatricesForInterpolation(source, matrix0, matrix1, mu, world, linkObjects);

            //// if there is only one matrix to use, visualize it in a straight-forward way
            if (matrix1 == null)
                return buildVisualizationGrid_singleMatrix(matrix0, angle, currentSourcePosition);

            // otherwise, draw an "interpolated matrix" that lies between matrix0 and matrix1.
            // The grid rectangle is an interpolation of matrix0 and matrix1, which may have different sizes.
            // We cannot draw one of these matrices directly. Instead, we should create a new grid and fill it with interpolated values.

            // - the rotation is interpolated between matrix0 and matrix1
            //float angle = (1 - mu) * angle0 + mu * angle1;

            // - the grid's origin is interpolated as well
            Vector2D sourcePosition = (1 - mu) * matrix0.getSourcePosition() + mu * matrix1.getSourcePosition();

            Vector2D translation = currentSourcePosition - sourcePosition;

            // - the grid's width/height is interpolated as well
            float h = (1 - mu) * matrix0.unitH * matrix0.getRows() + mu * matrix1.unitH * matrix1.getRows();
            float w = (1 - mu) * matrix0.unitW * matrix0.getColomns() + mu * matrix0.unitW * matrix0.getColomns();
            int cols = (int)Math.Round((1 - mu) * matrix0.getColomns() + mu * matrix1.getColomns());
            int rows = (int)Math.Round((1 - mu) * matrix0.getRows() + mu * matrix1.getRows());
            float cellWidth = w / cols;
            float cellHeight = h / rows;

            IFVisualizationGrid grid;
            grid.gridPoints =new List<List<IFVisualizationGridPoint>>();



            //grid.gridPoints.resize(rows, std::vector<IFVisualizationGridPoint>(cols));

            // fill in all grid points
            for (int i = 0; i < rows; i++)
            {
                List<IFVisualizationGridPoint> listJ = new List<IFVisualizationGridPoint> (cols);
                for (int j = 0; j < cols; j++)
                {

                    Vector2D basePosition = new Vector2D((j + 0.5f) * cellWidth, (rows - i - 0.5f) * cellHeight);
                    Vector2D rotatedPosition = Vector2D.rotateCounterClockwiseAroundPoint(basePosition, sourcePosition, -angle);
                    Vector2D worldPosition = rotatedPosition + translation;

                    // compute the interpolated IF vector at this position
                    bool inside;
                    Vector2D v0= new Vector2D(0, 0);
                    Vector2D v1 = new Vector2D(0, 0);
                    if (i<matrix0.getRows() && j < matrix0.getColomns()) {
                        v0 = matrix0.matrix[i, j];
                    }
                    if (i < matrix1.getRows() && j < matrix1.getColomns()) {
                        v1 = matrix1.matrix[i, j];
                    }

                    Vector2D IFVector = (1 - mu) * v0 + mu *v1;
                    Vector2D finalVector = IFVector.getnormalized() * Math.Min((1 - mu) * matrix0.unitW + mu * matrix1.unitW, (1 - mu) * matrix0.unitH + mu * matrix1.unitH) / 2;
                    listJ.Add(new KeyValuePair<Vector2D, Vector2D>(worldPosition, finalVector));
                }
                grid.gridPoints.Add(listJ);

            }




            // create the boundary points
            grid.boundary = new List<Vector2D>();
            Vector2D corner = new Vector2D(0, 0);
            grid.boundary.Add(Vector2D.rotateCounterClockwiseAroundPoint(corner, sourcePosition, -angle) + translation);
            grid.boundary.Add(Vector2D.rotateCounterClockwiseAroundPoint(corner + new Vector2D(w, 0), sourcePosition, -angle) + translation);
            grid.boundary.Add(Vector2D.rotateCounterClockwiseAroundPoint(corner + new Vector2D(w, h), sourcePosition, -angle) + translation);
            grid.boundary.Add(Vector2D.rotateCounterClockwiseAroundPoint(corner + new Vector2D(0, h), sourcePosition, -angle) + translation);

            return grid;

        }

        public static IFVisualizationGrid buildVisualizationGrid_singleMatrix(InteractionFieldMatrix matrix, float angle, Vector2D currentSourcePosition)
        {
            //const VectorMatrix* matrix_ = &field->matrix_;

            //float angle = getRotationAngleFromLinks(field, source, linkObjects, world);
            Vector2D sourcePosition = matrix.getSourcePosition();
            Vector2D translation = currentSourcePosition - sourcePosition;

            int cols = matrix.getColomns();
            int rows = matrix.getRows();
            //private static object transform;
            IFVisualizationGrid grid;
            grid.gridPoints = new List<List<IFVisualizationGridPoint>>();
            //grid.gridPoints.resize(matrix_->getRows(), std::vector<IFVisualizationGridPoint>(matrix_->getCols()));
            float Height = matrix.unitH * rows;
            float Width = matrix.unitW * cols;
            // fill in all grid points
            for (int i = 0; i<rows; i++)
            {   
                List<IFVisualizationGridPoint> listJ = new List<IFVisualizationGridPoint>(cols);
            	for (int j = 0; j<cols; j++)
            	{
            		Vector2D basePosition = new Vector2D((j + 0.5f) * matrix.unitW, (rows - i - 0.5f) * matrix.unitH);
                    Vector2D rotatedPosition = Vector2D.rotateCounterClockwiseAroundPoint(basePosition, sourcePosition, -angle);
                    Vector2D worldPosition = rotatedPosition + translation;
            		Vector2D baseIFVector =matrix.matrix[i, j];
                    Vector2D rotatedIFVector = Vector2D.rotateCounterClockwise(baseIFVector, -angle);
                    Vector2D finalVector = rotatedIFVector.getnormalized() * Math.Min(matrix.unitW, matrix.unitH) / 2;
                    listJ.Add(new KeyValuePair<Vector2D, Vector2D>( worldPosition, finalVector));
                }
                grid.gridPoints.Add(listJ);
            }

            // create the boundary points
            grid.boundary = new List<Vector2D>();
            Vector2D corner = new Vector2D(0, 0);
            grid.boundary.Add(Vector2D.rotateCounterClockwiseAroundPoint(corner, sourcePosition, -angle) + translation);
            grid.boundary.Add(Vector2D.rotateCounterClockwiseAroundPoint(corner + new Vector2D(Width, 0), sourcePosition, -angle) + translation);
            grid.boundary.Add(Vector2D.rotateCounterClockwiseAroundPoint(corner + new Vector2D(Width, Height), sourcePosition, -angle) + translation);
            grid.boundary.Add(Vector2D.rotateCounterClockwiseAroundPoint(corner + new  Vector2D(0, Height), sourcePosition, -angle) + translation);

            return grid;
        }
    } 
}
