using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrowdMP.Core
{
    public class InteractorObject : MonoBehaviour
    {
        private bool isActivate=false;

        public bool IsActivate { get => isActivate; set => isActivate = value; }

        // Start is called before the first frame update
        public void HoverEnter()
        {
            GetComponent<Renderer>().material.EnableKeyword("_EMISSION");

        }

        public void HoverOut()
        {
            GetComponent<Renderer>().material.DisableKeyword("_EMISSION");

        }

        public void selectOn()
        {
            Debug.Log("hover in");
            IsActivate = true;

        }

        public void selectOff()
        {
            Debug.Log("un select");

            GameObject agentMxM = GameObject.Find("MxMAgent");
            Agent agent = (Agent)agentMxM.GetComponent(typeof(Agent));
            agent.Activate = false;
        }
    }
}