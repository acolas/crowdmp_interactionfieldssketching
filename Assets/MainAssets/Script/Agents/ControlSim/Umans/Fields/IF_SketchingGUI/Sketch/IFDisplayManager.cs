/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program.If not, see<https://www.gnu.org/licenses/>.
**  
**  Authors: Ad�le Colas
**  Contact: crowd_group@inria.fr
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


namespace CrowdMP.Core
{
    public class IFDisplayManager : MonoBehaviour
    {
        private float timeStart;
        private List<GameObject> curves;
        public GameObject GuideCurvePref;
        private GameObject GuideCurve;
        private GameObject anchor;
        private bool isSel;
        private bool isRayCollind;
        private bool eraser =false ;
        public InputActionReference sketchByMove = null;
        public InputActionReference sketchByCurve = null;
        GameObject line;
        private Vector3 breakPosition;
        private Quaternion breakRotation;
        
        

        public bool IsSel { get => isSel; set => isSel = value; }
        public bool IsRayCollind { get => isRayCollind; set => isRayCollind = value; }
        public Vector3 BreakPosition { get => breakPosition; set => breakPosition = value; }
        public bool Eraser { get => eraser; set => eraser = value; }
        public Quaternion BreakRotation { get => breakRotation; set => breakRotation = value; }


        // Start is called before the first frame update
        void Start()
        {


            curves = new List<GameObject>();
            isSel = false;
            isRayCollind = false;
            line = new GameObject("eph�m�re");
            line.AddComponent<LineRenderer>();
            line.SetActive(false);
            Eraser = false;
            hide();
            timeStart = -500;
            this.transform.Find("Cylinder").gameObject.SetActive(false);
           
           
            //sketchByCurve.action.canceled += HoldMeHandSketch;

            //sketchByMove.action.performed += HoldMeMoveSketch;

            //sketchByMove.action.started += HoldMeMoveSketch;


        }


        

        // Update is called once per frame
        void Update()
        {
            //Debug.Log("phase " + sketchByCurve.action.phase);
            //Debug.Log("value right hand" + sketchByCurve.action.ReadValue<float>());
            
            if (GameObject.FindGameObjectWithTag("GameManager").GetComponent<MainManager>().sketchBreak)
            {
                //this.transform.rotation = Quaternion.Euler(0, 0, 0);
                //this.transform.position = new Vector3(this.transform.position.x, 0.1f, this.transform.position.z);
                //transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, transform.eulerAngles.z)
                this.transform.position = breakPosition;

                this.transform.rotation = breakRotation;
                //fixeRotation();
                


                if (!isSel)
                {
                    
                    hide();
                    
                    
                }
                else
                {
                    
                    show();
                    if (eraser)
                    {
                        HoldMeErase();
                    }
                    else
                    {
                        if (sketchByCurve.action.enabled)
                        {
                            HoldMeHandSketch();
                        }
                        if (sketchByMove.action.enabled)
                        {
                            HoldMeMoveSketch();
                        }
                    }
                }


                displayCylinder(true);
            }
            else
            {
                breakPosition = this.transform.position;
                breakRotation = this.transform.rotation;
                timeStart = -500;

                displayCylinder(false);
                sketchByCurve.action.Enable();
                sketchByMove.action.Enable();
            }

        }
        public void hide()
        {
            GetComponent<LineRenderer>().enabled = false;
            foreach (Transform child in transform)
            {
                if(child.name != "Cylinder")
                {
                    if (child.gameObject.GetComponent<LineRenderer>() != null)
                    {
                        child.gameObject.GetComponent<LineRenderer>().enabled = false;
                    }
                    else
                    {
                        child.gameObject.GetComponent<Renderer>().enabled = false;
                    }
                }

                
            }
        }
        public void show()
        {
            GetComponent<LineRenderer>().enabled = true;
            foreach (Transform child in transform)
            {
                if (child.name != "Cylinder")
                {
                    if (child.gameObject.GetComponent<LineRenderer>() != null)
                    {
                        child.gameObject.GetComponent<LineRenderer>().enabled = true;
                    }
                    else
                    {
                        child.gameObject.GetComponent<Renderer>().enabled = true;
                    }
                }

            }
        }
        public void fixeRotation()          
        {

            transform.rotation = Quaternion.Euler(0, 0, 0);

            foreach (Transform child in transform)
            {
                child.rotation = Quaternion.Euler(0, 0, 0);

            }
        }


        void HoldMeMoveSketch()
        {
            var phase = sketchByMove.action.phase;
            if (!GetComponentInParent<ViewInteractionField>().UI_IFediting.activeSelf)
            {
                anchor = GameObject.FindGameObjectWithTag("Player");
                
                if (anchor != null && GetComponent<BoxCollider>().bounds.Contains(new Vector3(anchor.transform.position.x, GetComponent<BoxCollider>().bounds.center.y, anchor.transform.position.z)))
                {

                    if (sketchByMove.action.ReadValue<float>() == 1f && timeStart == 0f)
                    {
                        
                        sketchByCurve.action.Disable();
                        StartSkecth();



                    }
                    else if (sketchByMove.action.ReadValue<float>() > 0f && timeStart > 0f)
                    {

                        
                        SketchCurve();

                    }
                    else if (sketchByMove.action.ReadValue<float>() == 0f && timeStart != 0)
                    {
                        

                        StopSketching();
                        sketchByCurve.action.Enable();

                    }
                }
                else
                {
                    //Debug.Log("anchor non existent or not contained in the field");
                }
            }
        }
        void HoldMeHandSketch()
        {
            if (!GetComponentInParent<ViewInteractionField>().UI_IFediting.activeSelf)
            {
                //var phase = sketchByCurve.action.phase;
            //Debug.Log("phase " + sketchByCurve.action.phase);
                anchor = GameObject.FindGameObjectWithTag("Pointer");
                if (anchor != null && IsRayCollind)
                {

                    if (sketchByCurve.action.ReadValue<float>() == 1f && timeStart == 0f )
                    {

                        sketchByMove.action.Disable();

                        StartSkecth();
                        
                    

                    }
                    else if (sketchByCurve.action.ReadValue<float>() > 0f && timeStart>0f)
                    {

                        SketchCurve();


                    }
                    else if (sketchByCurve.action.ReadValue<float>() ==0f && timeStart != 0)
                    {


                            StopSketching();
                            sketchByMove.action.Enable();
                            
                        


                    }
                    else
                    {
                        sketchByMove.action.Enable();
                    }
                }
                else
                {
                    //Debug.Log("anchor non existent or not contained in the field");
                }
            }


        }


        void HoldMeErase()
        {
            if (!GetComponentInParent<ViewInteractionField>().UI_IFediting.activeSelf)
            {
               
                var phase = sketchByCurve.action.phase;
                //Debug.Log("phase " + sketchByCurve.action.phase);
                anchor = GameObject.FindGameObjectWithTag("Pointer");
                if (anchor != null && IsRayCollind)
                {


                    if (sketchByCurve.action.ReadValue<float>() > 0f)
                    {
                        lookforCollision(anchor.transform.position);
                    }

                }
                else
                {
                    //Debug.Log("anchor non existent or not contained in the field");
                }
            }


        }


        public void lookforCollision(Vector3 posPoint)
        {

            foreach (Transform child in transform)
            {
                if (child.gameObject.GetComponent<LineRenderer>() != null)
                {
                    Vector3 PositionChild = child.GetComponent<LineRenderer>().GetPosition(0);

                   if((posPoint.x < PositionChild.x + 0.5f) && (posPoint.x > PositionChild.x - 0.5f)&& (posPoint.z < PositionChild.z + 0.5f) && (posPoint.z > PositionChild.z - 0.5f))
                    {
                        
                        Vector2D pos = new Vector2D(PositionChild.x,PositionChild.z);
                       
                        GetComponentInParent<ViewInteractionField>().deleteVector(pos);
                    }
                }
                //else if (child.gameObject.GetComponent<Renderer>() != null && child.gameObject.GetComponent<LineRenderer>() == null)
                //{
                //    Vector3 PositionChild = child.transform.position;
                //    if( (posPoint.x < PositionChild.x + 0.5f) && (posPoint.x > PositionChild.x - 0.5f) && (posPoint.z < PositionChild.z + 0.5f) && (posPoint.z > PositionChild.z - 0.5f))
                //    {
                //        foreach(var c in curves)
                //        {
                //            foreach(var h in c.GetComponent<ControlVector>().Handles)
                //            {
                //                if(gameObject == h)
                //                {
                //                    c.GetComponent<ControlVector>().Handles.Remove(h);
                //                    if (c.GetComponent<ControlVector>().Handles.Count < 1)
                //                    {
                //                        curves.Remove(c);
                //                    }
                //                    Destroy(c);
                //                    Destroy(gameObject);

                //                }
                //            }
                //        }


                //    }
                //}


            }

        }

        public void StartSkecth()
        {


                line.SetActive(true);
               

                GuideCurve = GameObject.Instantiate(GuideCurvePref);

                GuideCurve.transform.parent = this.gameObject.transform;
                GuideCurve.name = "GuideCurve_" + curves.Count;

                timeStart = Time.time;

                GuideCurve.GetComponent<ControlVector>().AddPoint(new Vector3(anchor.transform.position.x, 0.1f, anchor.transform.position.z));

            



        }

        public void SketchCurve()
        {



                    
                    //if (timeStart == 0f)
                    //{
                    //    //Debug.Log("sketching curve");
                    //    GuideCurve = GameObject.Instantiate(GuideCurve);
                    //    GuideCurve.transform.parent = this.gameObject.transform;
                    //    GuideCurve.name = "GuideCurve_" + curves.Count;
                    //    timeStart = Time.time;
                    //    GuideCurve.GetComponent<ControlVector>().AddPoint(new Vector3(anchor.transform.position.x, 0, anchor.transform.position.z));
                    //    line = new GameObject();
                    //}
                    if(Time.time - timeStart > 1)
                    {
                        timeStart = Time.time;

                        GuideCurve.GetComponent<ControlVector>().addHandle(new Vector3(anchor.transform.position.x, 0.1f, anchor.transform.position.z));

                    }
                    else if (timeStart > 0)
                    {
                       
                        
                        LineRenderer temporaryLine = line.GetComponent<LineRenderer>();
                        temporaryLine.positionCount = 2;
                        temporaryLine.SetPositions(new Vector3[] { GuideCurve.GetComponent<ControlVector>().getLastHandle(), new Vector3(anchor.transform.position.x, 0.1f, anchor.transform.position.z) });
                        temporaryLine.material = new Material(Shader.Find("Sprites/Default"));
                        temporaryLine.startColor = Color.green;
                        temporaryLine.endColor = Color.green;
                        temporaryLine.startWidth = 0.1f;
                        temporaryLine.endWidth = 0.1f;
                        
                    }

                

            


        }
            
        

        public void StopSketching()
        {

            if (GuideCurve != null)
            {


                if (GuideCurve.GetComponent<ControlVector>().Handles.Count > 0 && !curves.Contains(GuideCurve))
                {

                    GuideCurve.GetComponent<ControlVector>().addHandle(new Vector3(anchor.transform.position.x, 0.1f, anchor.transform.position.z));

                    //GuideCurve.GetComponent<ControlVector>().closeCurve();
                    curves.Add(GuideCurve);
                    line.SetActive(false);
                    
                }
                else if (GuideCurve.GetComponent<ControlVector>().Handles.Count < 2)
                {
                    foreach (var hand in GuideCurve.GetComponent<ControlVector>().Handles)
                    {
                        if (hand != null)
                        {
                            Destroy(hand);
                        }
                    }
                    

                }


                
            }
            timeStart = 0f;

        }




        public void select()
        {
            //var otherIFs = GameObject.FindGameObjectsWithTag("IF");
            //foreach (GameObject IF in otherIFs)
            //{
            //    IF.SetActive(false);
            //}
            
            GetComponentInParent<ViewInteractionField>().UI_IFediting.SetActive(true);
            



        }

        public void drawCurves(List<Vector2D[]> curvesinput, Vector2D sourcePosition)
        {
            curves = new List<GameObject>();

                for (int i = 0; i < curvesinput.Count; i++)
                {

                    GameObject objectCurve = Instantiate(GuideCurvePref);
                    objectCurve.transform.parent = transform;
                    objectCurve.name = "GuideCurve_" + i;

                    foreach (var pt in curvesinput[i])
                    {
                        Vector3 handlePos = new Vector3(-sourcePosition.x, 0, -sourcePosition.y) + new Vector3(pt.x, 0, pt.y) + new Vector3(this.transform.parent.transform.position.x, 0, this.transform.parent.transform.position.z);
                        // Vector3 handlePos = new Vector3(-sourcePosition.x, 0, -sourcePosition.y) + new Vector3(pt.x, 0, pt.y);
                        //Vector3 handlePos = new Vector3(-gameObject.GetComponent<CapsuleCollider>().transform.position.x, 0, -GetComponent<CapsuleCollider>().transform.position.y) + new Vector3(pt.x, 0, pt.y)  ;


                        objectCurve.GetComponent<ControlVector>().AddPoint(handlePos);
                        
                    //    objectCurve.GetComponent<ControlVector>().Handles[-1].transform.eulerAngles = new Vector3(0, (float)(180 - IF1.Angle * 180 / Math.PI), 0);
                    //objectCurve.GetComponent<ControlVector>().Handles.FindLast().transform.eulerAngles = new Vector3(0, (float)(180 - IF1.Angle * 180 / Math.PI), 0);
                }


                    //objectCurve.GetComponent<ControlVector>().closeCurve();
                    curves.Add(objectCurve);
                }
            
           
        }

        public void displayCylinder(bool display)
        {
            transform.Find("Cylinder").gameObject.SetActive(display);

        }

        public List<Vector2D[]> getCurves(Vector2D offset)
        {
            List<Vector2D[]> curvesoutput = new List<Vector2D[]>();
            foreach(var c in curves)
            {
                if(c != null)
                {
                    curvesoutput.Add(c.GetComponent<ControlVector>().pointsCurve(offset));
                }
                
            }
            return curvesoutput;

        }

        public void destroyCurves()
        {
            if(curves != null)
            {
                foreach (var curve in curves)
                {
                    destroyCurve(curve);

                }
            }

            

           
        }
        public void destroyCurve( GameObject curve)
        {
            if (curve != null)
            {
                foreach (var handle in curve.GetComponent<ControlVector>().Handles)
                {
                    if (handle != null)
                    {
                        Destroy(handle);
                    }

                }

                Destroy(curve);
            }
        }
        public void deselect()
        {
            //var otherIFs = GameObject.FindGameObjectsWithTag("IF");
            //foreach (GameObject IF in otherIFs)
            //{
            //    IF.SetActive(true);
            //}
            if (!isRayCollind)
            {
                hide();
                
            }
            hide();
            GetComponentInParent<ViewInteractionField>().UI_IFediting.SetActive(false);
            
        }



        public void OnHover()
        {
            isRayCollind = true;           
            show();
        }
        public void HoverExit()
        {
            isRayCollind = false;
            hide();
            
        }

    }
}