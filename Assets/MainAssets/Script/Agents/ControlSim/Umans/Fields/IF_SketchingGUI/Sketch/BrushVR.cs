﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using UnityEngine;
//using System.Xml.Serialization;
//using System.Runtime.InteropServices;


//namespace CrowdMP.Core
//{
//    public class BrushVR : MonoBehaviour
//    {
//        SteamVR_Behaviour_Pose pose;
//        SteamVR_Action_Boolean interactWithUI = SteamVR_Input.GetBooleanAction("InteractUI");

//        SteamVR_Action_Boolean GrabAction = SteamVR_Input.GetBooleanAction("GrabAction");

//        GameObject LeftInputSource;
//        Custom_LaserPointer scriptLaser;
//        //LineRenderer lineRenderer ;

//        List<LineRenderer> listLines = new List<LineRenderer>();
//        LineRenderer lineRenderer;
//        //public int lengthOfLineRenderer = 500;
//        int count = 0;
//        Color c1 = Color.green;
//        Color c2 = Color.green;
//        List<Vector3> posList = new List<Vector3>();

//        Stack<Vector3> myStack = new Stack<Vector3>();
//        Vector3[] array = new Vector3[10];
//        // Start is called before the first frame update
//        void Start()
//        {
//            LeftInputSource = GameObject.Find("RightHand");
//            scriptLaser = LeftInputSource.GetComponent<Custom_LaserPointer>();
//            if (pose == null)
//                pose = this.GetComponent<SteamVR_Behaviour_Pose>();
//            if (pose == null)
//                Debug.LogError("No SteamVR_Behaviour_Pose component found on this object");

//            if (interactWithUI == null)
//                Debug.LogError("No ui interaction action has been set on this component.");
//            lineRenderer = gameObject.AddComponent<LineRenderer>();
//            lineRenderer.material = new Material(Shader.Find("Sprites/Default"));
//            lineRenderer.widthMultiplier = 0.02f;
//            //lineRenderer.positionCount = lengthOfLineRenderer;

//            // A simple 2 color gradient with a fixed alpha of 1.0f.
//            float alpha = 1.0f;
//            /*    Gradient gradient = new Gradient();
//                gradient.SetKeys(
//                    new GradientColorKey[] { new GradientColorKey(c1, 1.0f) },
//                    new GradientAlphaKey[] { new GradientAlphaKey(alpha, 0.0f), new GradientAlphaKey(alpha, 1.0f) }
//                );
//                lineRenderer.colorGradient = gradient;*/
//            lineRenderer.SetColors(c1, c2);
//            listLines.Add(lineRenderer);
//        }

//        // Update is called once per frame
//        Vector3 previousHit;
//        RaycastHit hit;


//        void Update()
//        {
//            /*if(SteamVR_Input._default.inActions.GrabGrip.GetStateDown(hand.handType)){
//                //Debug.Log("grip");
//            }
//            if (GrabAction.GetStateUp(pose.inputSource))
//            {
//                Debug.Log("coucou");
//            }*/
//            if (interactWithUI.GetStateUp(pose.inputSource))
//            {
//                //Debug.Log("fin");
//                posList.Clear();
//                var go = new GameObject();
//                LineRenderer nextLine = go.AddComponent<LineRenderer>();

//                nextLine.transform.gameObject.name = "Annotation_BrushLine";
//                //    nextLine.transform.gameObject.AddComponent<BoxCollider>();

//                nextLine.material = new Material(Shader.Find("Sprites/Default"));
//                nextLine.widthMultiplier = 0.02f;
//                // A simple 2 color gradient with a fixed alpha of 1.0f.
//                /*    float alpha = 1.0f;
//                Gradient gradient = new Gradient();
//                gradient.SetKeys(
//                    new GradientColorKey[] { new GradientColorKey(c1, 1.0f) },
//                    new GradientAlphaKey[] { new GradientAlphaKey(alpha, 0.0f), new GradientAlphaKey(alpha, 1.0f) }
//                );
//                nextLine.colorGradient = gradient;*/
//                Debug.Log("left color:" + scriptLaser.currentColorFreeHand);
//                //nextLine.SetColors(RightInputSource.GetComponent<Custom_LaserPointer>().currentColorFreeHand, RightInputSource.GetComponent<Custom_LaserPointer>().currentColorFreeHand);
//                //nextLine.SetColors(c1, c2);
//                listLines.Add(nextLine);
//            }
//            if (interactWithUI != null && interactWithUI.GetState(pose.inputSource))
//            {
//                //Debug.Log("triggered");
//                // Does the ray intersect any objects excluding the player layer
//                if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit))
//                {
//                    if (scriptLaser.freeHandMode)
//                    {
//                        Debug.Log("freeHandMode BRUSH");
//                        Debug.Log("hit.transform.gameObject" + hit.transform.gameObject);
//                        if (hit.transform.gameObject.name == "Annotation_BrushPlane")
//                        {
//                            //count++;
//                            //previousHit=hit.point;
//                            //if(hit.point !=Vector3.zero){
//                            /*if(myStack.Count < 10){
//                             //if(array.Length < 10){
//                                myStack.Push(hit.point);
//                                //array.Add(hit.point);
//                            }
//                            else{
//                                 int i;
//                                 Vector3 vSuma= Vector3.zero;
//                                 Vector3[] arr = myStack.ToArray(); 
//                                 foreach(Vector3 v in arr) 
//                                {
//                                    vSuma += v;
//                                }
//                                // for (i=0; i<array.Length; i++){
//                                    // vSuma += array[i];
//                                 //}
//                                 vSuma= vSuma/arr.Length;
//                                 //vSuma= vSuma/array.Length;
//                                 posList.Add(vSuma);
//                                 //array.Pop();
//                                 myStack.Pop();
//                            }
//                            //}
//                            */
//                            posList.Add(hit.point);
//                            lineRenderer = listLines[listLines.Count - 1];

//                            lineRenderer.positionCount = posList.Count;
//                            lineRenderer.SetColors(LeftInputSource.GetComponent<Custom_LaserPointer>().currentColorFreeHand, LeftInputSource.GetComponent<Custom_LaserPointer>().currentColorFreeHand);
//                            lineRenderer.SetPositions(posList.ToArray());






//                            Debug.Log("coucou");



//                            /*if(count < lengthOfLineRenderer) {
//                                lineRenderer.SetPosition(count-1, previousHit);
//                            }
//                            else{
//                                count=0;
//                            }*/
//                        }
//                    }
//                    else if (scriptLaser.sphereMode)
//                    {
//                        Debug.Log("sphereMode BRUSH");

//                    }
//                }
//            }

//            if (interactWithUI != null && interactWithUI.GetStateUp(pose.inputSource))
//            {
//                //Debug.Log("cucucucucuc");

//                if (lineRenderer.transform.gameObject.GetComponent<BoxCollider>() == null)
//                {
//                    lineRenderer.transform.gameObject.AddComponent<BoxCollider>();
//                }
//                else
//                {
//                    Destroy(lineRenderer.transform.gameObject.GetComponent<BoxCollider>());
//                    lineRenderer.transform.gameObject.AddComponent<BoxCollider>();
//                }
//            }
//        }
//    }

//}
