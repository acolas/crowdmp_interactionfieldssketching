﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using System.Xml.Serialization;
using System.Runtime.InteropServices;



namespace CrowdMP.Core
{
    [System.Serializable]
    public class InteractionField
    {

        [XmlAttribute("id")]
        public int id;
        [XmlAttribute("LinkeObject")]
        public int LinkedObjectID;
        [XmlAttribute("typeLinkedAgent")]
        public bool typeLinkedAgent;
        [XmlIgnore()]
        private Dictionary<float,InteractionFieldMatrix> matrices;

        private float parameter;
        private float angle;
        private float mu;
        private bool parameterChanged;

        public float Mu { get => mu; set => mu = value; }
        public float Angle { get => angle; set => angle = value; }
        public float Parameter { get => parameter; set => parameter = value; }
        public bool ParameterChanged { get => parameterChanged; set => parameterChanged = value; }

        public InteractionField()
        {
            id = 0;
            typeLinkedAgent = true;
            LinkedObjectID = 9999;
            matrices = new Dictionary<float, InteractionFieldMatrix>();
            parameterChanged = false;
            Parameter = 0f;



        }

        public InteractionField(int id_)
        {
            id = id_;
            typeLinkedAgent = true;
            LinkedObjectID = 9999;
            matrices = new Dictionary<float, InteractionFieldMatrix>();
            parameterChanged = false;
            Parameter = 0f;



        }

        public void initDiplay()
        {
            int maxRow = 0; int maxCol = 0;
            foreach(InteractionFieldMatrix m in matrices.Values)
            {
                if (m.matrix.GetLength(0) > maxRow)
                {
                    maxRow = m.matrix.GetLength(0);
                }
                if (m.matrix.GetLength(1) > maxCol)
                {
                    maxCol = m.matrix.GetLength(1);
                }
            }

        }

        public int getMaxColumn()
        {
            int maxCol = 0;
            foreach (InteractionFieldMatrix m in matrices.Values)
            {

                if (m.matrix.GetLength(1) > maxCol)
                {
                    maxCol = m.matrix.GetLength(1);
                }
            }
            return maxCol;
        }
        public int getMaxRow()
        {
            int maxRow = 0;
            foreach (InteractionFieldMatrix m in matrices.Values)
            {
                if (m.matrix.GetLength(0) > maxRow)
                {
                    maxRow = m.matrix.GetLength(0);
                }

            }
            return maxRow;
        }

        public void setID(int id_)
        {
            id = id_;
        }

        public void addMatrix(float param, InteractionFieldMatrix m)
        {
            matrices.Add(param, m);
        }

        public Dictionary<float, InteractionFieldMatrix> getMatrices()
        {
            return matrices;
        }
        //public void initDisplay(int r, int c, Color color, float width)
        //{

        //    vr_InteractionField = Resources.Load<GameObject>("vr_InteractionField");
        //    vr_InteractionField.name = "IF_" + id;
        //    vr_InteractionField.transform.position = new Vector3(0, 0, 0);
        //    vr_InteractionField.AddComponent<LineRenderer>();
        //    LineRenderer br = vr_InteractionField.GetComponent<LineRenderer>();
        //    br.material = new Material(Shader.Find("Sprites/Default"));
        //    br.startColor = color;
        //    br.endColor = color;
        //    br.startWidth = width;
        //    br.endWidth = width;
        //    br.positionCount = 5;
        //    br.SetPositions( new Vector3[5] {new Vector3(0,0,0), new Vector3(matrices[0].getColomns()*matrices[0].unitW,0,0), new Vector3(matrices[0].getColomns() * matrices[0].unitW, 0, matrices[0].getRows() * matrices[0].unitH),
        //        new Vector3(0, 0, matrices[0].getRows() * matrices[0].unitH), new Vector3(matrices[0].getColomns() * matrices[0].unitW, 0, 0)});
        //    vr_InteractionField.SetActive(true);

        //    matrixView = new GameObject[r,c];
        //    for(int i=0; i<matrixView.GetLength(0); i++)
        //    {
        //        for(int j =0; j<matrixView.GetLength(1); j++)
        //        {


        //            GameObject myLine = new GameObject("vector_"+id+"_"+i+"-"+j);

        //            myLine.transform.parent = vr_InteractionField.transform;
        //            myLine.transform.position = new Vector3(0, 0, 0);
        //            myLine.AddComponent<LineRenderer>();
        //            LineRenderer lr = myLine.GetComponent<LineRenderer>();
        //            lr.material = new Material(Shader.Find("Sprites/Default"));

        //            lr.startColor = color;
        //            lr.endColor = color;
        //            lr.startWidth = width;
        //            lr.endWidth = width;
        //            lr.positionCount = 4;
        //            lr.useWorldSpace = false;
        //            myLine.SetActive(false);
        //            matrixView[i, j] = myLine;
        //            if( i < matrices[0].getRows() &&  j < matrices[0].getColomns())
        //            {
        //                Vector3 start = new Vector3((j + 0.5f) * matrices[0].unitW, 0, (matrices[0].getRows() - i - 0.5f) * matrices[0].unitH);
        //                Vector3 end = new Vector3(matrices[0].matrix[i, j].x, 0, matrices[0].matrix[i, j].y);
        //                lr.widthCurve = new AnimationCurve(
        //                    new Keyframe(0, 0.1f)
        //                    , new Keyframe(0.999f - 0.4f, 0.1f)  // neck of arrow
        //                    , new Keyframe(1 - 0.4f, 0.2f)  // max width of arrow head
        //                    , new Keyframe(1, 0f));  // tip of arrow
        //                lr.SetPositions(new Vector3[] {
        //              start
        //              , Vector3.Lerp(start, end, 0.999f - 0.4f)
        //              , Vector3.Lerp(start, end, 1 - 0.4f)
        //              , end });
        //                lr.alignment = LineAlignment.TransformZ;
        //                lr.transform.eulerAngles = Vector3.up;
                        
        //            }




        //        }
        //    }

        //}
        public void setParameter(float param)
        {
            if(param != parameter)
            {
                parameter = param;
                ParameterChanged = true;
            }
            else
            {
                parameterChanged = false;
            }
           
        }
        
        public void getInterpolationMatrices(ref InteractionFieldMatrix m1, ref InteractionFieldMatrix m2, ref float result_mu)
        {

            for(int i =0;i < getMatrices().Count; i++)
            {
                float paramMax = getMatrices().Keys.ToList()[i];   
                if (parameter <= paramMax)
                {
                    // check if we should interpolate between this matrix and the previous
                    if ((parameter < paramMax) && (i != 0))
                    {
                        float paramMin = getMatrices().Keys.ToList()[i-1];
                        InteractionFieldMatrix prevMat = matrices[paramMin];                       
                        m1 = prevMat;
                        m2 = matrices[paramMax];
                        result_mu = (parameter - paramMin) / (paramMax - paramMin);
                    }

                    // if there's no interpolation to be done, use only this matrix
                    else
                    {
                        m1 = matrices[paramMax];
                        m2 = null;
                        result_mu = 0;
                    }

                    return;
                }
            }

            // parameter is larger than the max; just use the last matrix
            m1 = matrices[matrices.Keys.Max()];
            m2 = null;
            result_mu = 0;

        }


        
        public float getParameterMatrix(InteractionFieldMatrix m)
        {
            foreach(KeyValuePair < float, InteractionFieldMatrix> pair in matrices)
            {
                if (EqualityComparer<InteractionFieldMatrix>.Default.Equals(pair.Value, m))
                {
                    return pair.Key;
                }
            }
            return default;
        }


        //public void updateField( Vector3 currentSourcePosition_, float parameter, float angle)
        //{
        //    Vector2D currentSourcePosition = new Vector2D(currentSourcePosition_.x, currentSourcePosition_.z);
        //    InteractionFieldMatrix m1 = new InteractionFieldMatrix(20, 20);
        //    InteractionFieldMatrix m2 = new InteractionFieldMatrix(20, 20);
        //    Vector2D sourcePosition, translation;
        //    float mu = 0;
        //    getInterpolationMatrices(ref m1, ref m2, ref mu, parameter);

        //    if (m2 != null)
        //    {
        //        float h = (1 - mu) * m1.unitH * m1.getRows() + mu * m2.unitH * m2.getRows();
        //        float w = (1 - mu) * m1.unitW * m1.getColomns() + mu * m2.unitW * m2.getColomns();
        //        int cols = (int)Math.Round((1 - mu) * m1.getColomns() + mu * m2.getColomns());
        //        int rows = (int)Math.Round((1 - mu) * m1.getRows() + mu * m2.getRows());
        //        float cellWidth = w / cols;
        //        float cellHeight = h / rows;
        //        // fill in all grid points
        //        for (int i = 0; i < matrixView.GetLength(0); i++)
        //        {
        //            for (int j = 0; j < matrixView.GetLength(1); j++)
        //            {
        //                if (j < cols && i < rows)
        //                {
        //                    Vector2D basePosition = new Vector2D((j + 0.5f) * cellWidth, (rows - i - 0.5f) * cellHeight);
        //                    //Vector2D rotatedPosition = Vector2D.rotateCounterClockwiseAroundPoint(basePosition, sourcePosition, -angle);
        //                    //Vector2D worldPosition = rotatedPosition + translation;

        //                    // compute the interpolated IF vector at this position
        //                    bool inside;
        //                    Vector2D v0 = new Vector2D(0, 0);
        //                    Vector2D v1 = new Vector2D(0, 0);
        //                    if (i < m1.getRows() && j < m1.getColomns())
        //                    {
        //                        v0 = m1.matrix[i, j];
        //                    }
        //                    if (i < m2.getRows() && j < m2.getColomns())
        //                    {
        //                        v1 = m2.matrix[i, j];
        //                    }

        //                    Vector2D IFVector = (1 - mu) * v0 + mu * v1;
        //                    Vector2D finalVector = IFVector.getnormalized() * Math.Min((1 - mu) * m1.unitW + mu * m2.unitW, (1 - mu) * m1.unitH + mu * m2.unitH) / 2;
        //                    Vector3 start = new Vector3(basePosition.x, matrixView[i, j].transform.position.y, basePosition.y);
        //                    Vector3 end = new Vector3(finalVector.x, matrixView[i, j].transform.position.y, finalVector.y);
        //                    LineRenderer lr = matrixView[i, j].GetComponent<LineRenderer>();

        //                    //lr.positionCount = 4;

        //                    lr.widthCurve = new AnimationCurve(
        //                        new Keyframe(0, 0.1f)
        //                        , new Keyframe(0.999f - 0.4f, 0.1f)  // neck of arrow
        //                        , new Keyframe(1 - 0.4f, 0.2f)  // max width of arrow head
        //                        , new Keyframe(1, 0f));  // tip of arrow
        //                    lr.SetPositions(new Vector3[] {
        //                  start
        //                  , Vector3.Lerp(start, end, 0.999f - 0.4f)
        //                  , Vector3.Lerp(start, end, 1 - 0.4f)
        //                  , end });
        //                }


        //                else
        //                {
        //                    matrixView[i, j].SetActive(false);
        //                }
        //            }


        //        }
        //    }
        //    else
        //    {

        //        sourcePosition = m1.getSourcePosition();
        //        translation = currentSourcePosition - sourcePosition;
        //        int cols = m1.getColomns();
        //        int rows = m1.getRows();
        //        float Height = m1.unitH * rows;
        //        float Width = m1.unitW * cols;
        //        // fill in all grid points
        //        for (int i = 0; i < matrixView.GetLength(0); i++)
        //        {
        //            for (int j = 0; j < matrixView.GetLength(1); j++)
        //            {
        //                if (j < cols && i < rows)
        //                {
        //                    Vector2D basePosition = new Vector2D((j + 0.5f) * m1.unitW, (rows - i - 0.5f) * m1.unitH);
        //                    //Vector2D rotatedPosition = Vector2D.rotateCounterClockwiseAroundPoint(basePosition, sourcePosition, -angle);
        //                    Vector2D worldPosition = rotatedPosition + translation;
        //                    Vector2D baseIFVector = matrix.matrix[i, j];
        //                    Vector2D rotatedIFVector = Vector2D.rotateCounterClockwise(baseIFVector, -angle);
        //                    Vector2D finalVector = rotatedIFVector.getnormalized() * Math.Min(matrix.unitW, matrix.unitH) / 2;
        //                }

        //            }
        //        }
        //    }
        //    Vector2D sourcePosition = (1 - mu) * m1.getSourcePosition() + mu * m2.getSourcePosition();

        //    Vector2D translation = currentSourcePosition - sourcePosition;

        //    // - the grid's width/height is interpolated as well


        //    //grid.gridPoints.resize(rows, std::vector<IFVisualizationGridPoint>(cols));






        //    // create the vr_InteractionField points
        //    grid.vr_InteractionField = new List<Vector2D>();
        //    Vector2D corner = new Vector2D(0, 0);
        //    grid.vr_InteractionField.Add(Vector2D.rotateCounterClockwiseAroundPoint(corner, sourcePosition, -angle) + translation);
        //    grid.vr_InteractionField.Add(Vector2D.rotateCounterClockwiseAroundPoint(corner + new Vector2D(w, 0), sourcePosition, -angle) + translation);
        //    grid.vr_InteractionField.Add(Vector2D.rotateCounterClockwiseAroundPoint(corner + new Vector2D(w, h), sourcePosition, -angle) + translation);
        //    grid.vr_InteractionField.Add(Vector2D.rotateCounterClockwiseAroundPoint(corner + new Vector2D(0, h), sourcePosition, -angle) + translation);

        //    return grid;
        //}

    }




}
