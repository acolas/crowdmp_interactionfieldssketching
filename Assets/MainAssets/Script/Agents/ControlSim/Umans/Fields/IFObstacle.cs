﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace CrowdMP.Core
{

    /// <summary>
    /// Script compoent to add to any obstacle (pillar or wall) you want to be source of the Interaction Field list below.
    /// </summary>
    public class IFObstacle : MonoBehaviour
    {

        public List<InteractionField> VelocityInteractionFields; //the list of Velocity Interaction Field's attributed to this obstacle. 
        public List<InteractionField> OrientationInteractionFields; //the list of Orientation Interaction Field's attributed to this obstacle. 

        

        private void Update()
        {
            updateIFDisplay();
        }


        public void updateIFDisplay()
        {

            ViewInteractionField viewIF = this.gameObject.GetComponent<ViewInteractionField>();
            if (viewIF != null)
            
            {
                
                if (viewIF.IF1 == null)
                {
                    if (this.gameObject.GetComponent<ViewInteractionField>().IsVelocity )
                    {
                        if( VelocityInteractionFields.Count > 0)
                        {
                            viewIF.IF1 = VelocityInteractionFields[0];

                        }

                    }
                    else
                    {
                        if (OrientationInteractionFields.Count > 0)
                        {
                            viewIF.IF1 = OrientationInteractionFields[0];

                        }

                    }
                    viewIF.initDisplay(0.05f, VelocityInteractionFields.Count, OrientationInteractionFields.Count);
                    viewIF.MatrixSwith = true;

                }
                else
                {
                    int numIF = viewIF.NumIF;
                    if (this.gameObject.GetComponent<ViewInteractionField>().IsVelocity && viewIF.IF1 == VelocityInteractionFields[numIF] && !viewIF.IF1.ParameterChanged)
                    {
                        viewIF.MatrixSwith = false;
                    }
                    else if(!this.gameObject.GetComponent<ViewInteractionField>().IsVelocity && viewIF.IF1 == OrientationInteractionFields[numIF] && !viewIF.IF1.ParameterChanged)
                    {
                        viewIF.MatrixSwith = false;
                    }
                    else
                    {
                        viewIF.MatrixSwith = true;
                    }

                }
                viewIF.updateMatrix(this.gameObject.transform.position);
                
            }
        }


    }

}