﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program.If not, see<https://www.gnu.org/licenses/>.
**  
**  Authors: Adèle Colas
**  Contact: crowd_group@inria.fr
*/
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

namespace CrowdMP.Core
{

    /// <summary>
    /// Config of the IF simulation. 
    /// </summary>
    public class IFUmansConfig : UmansConfig
    {

        [XmlArray("InteractionFields")]
        [XmlArrayItem("InteractionField")]
        public List<InteractionField> InteractionFields; // list of the fields the agent will be source of

        [XmlAttribute]
        public int group; // group's number of the agent

        [XmlElement("pathInteractionField")]
        public string pathIF;

        public override ControlSim createControlSim(int id)
        {

            return new SimIF(id, pathIF);
        }
        
        public IFUmansConfig():base()
        {
            policy = Policy.IF;
            InteractionFields = new List<InteractionField>();
            group = -1;//if no group -1
            pathIF = "Assets\\MainAssets\\Script\\Agents\\ControlSim\\Umans\\Config.xml";
        }


    }


}

