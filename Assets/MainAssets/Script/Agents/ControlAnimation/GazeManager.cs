using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GazeManager : MonoBehaviour
{
    Vector3 currentPos;
    Vector3 instantSpeed;
    float speed;
    bool desactive;

    HeadLookController gazeScript;

    // Start is called before the first frame update
    void Start()
    {
        currentPos = transform.position;
        gazeScript = GetComponent<HeadLookController>();
        desactive = gazeScript.isActive;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (gazeScript != null)
        {
            Vector3 newPos = transform.position;
            Vector3 diff = newPos - currentPos;
            float speed = Vector3.Magnitude(diff) / Time.deltaTime;
            
            currentPos = newPos;

            if (speed <1 && desactive)
            {
                //Debug.Log("bcame slow "+speed);
                gazeScript.isActive = true;
                desactive = false;
            }            
            else if (speed >=1 && !desactive)
            {
                //Debug.Log("bcame fast back "+speed);
                gazeScript.isActive = false;
                desactive = true;
            }
        }
    }
}