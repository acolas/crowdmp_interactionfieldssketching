/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using System;
using UnityEngine;

[System.Serializable]
public class BendingSegment
{
	public Transform firstTransform;
	public Transform lastTransform;
	public float thresholdAngleDifference = 0;
	public float bendingMultiplier = 0.6f;
	public float maxAngleDifference = 30;
	public float maxBendingAngle = 80;
	public float responsiveness = 5;
	internal float angleH;
	internal float angleV;
	internal Vector3 dirUp;
	internal Vector3 referenceLookDir;
	internal Vector3 referenceUpDir;
	internal int chainLength;
	internal Quaternion[] origRotations;

	public BendingSegment(Transform firstTransform, Transform lastTransform, float thresholdAngleDifference, float bendingMultiplier,
		float maxAngleDifference, float maxBendingAngle, float responsiveness)
	{
		this.firstTransform = firstTransform;
		this.lastTransform = lastTransform;
		this.thresholdAngleDifference = thresholdAngleDifference;
		this.bendingMultiplier = bendingMultiplier;
		this.maxAngleDifference = maxAngleDifference;
		this.maxBendingAngle = maxBendingAngle;
		this.responsiveness = responsiveness;
	}
}

[System.Serializable]
public class NonAffectedJoints
{
	public Transform joint;
	public float effect = 0;

	public NonAffectedJoints(Transform joint, float effect)
    {
		this.joint = joint;
		this.effect = effect;
	}
}

public class HeadLookController : MonoBehaviour
{
	public GameObject gazeTarget;
	public bool isActive;
	//GameObject convergenceEyeTargetObj;

	public Transform rootNode;
	public BendingSegment[] segments;
	public NonAffectedJoints[] nonAffectedJoints;

	public Vector3 headLookVector = Vector3.forward;
	public Vector3 headUpVector = Vector3.up;

	public float effect = 1;
	public bool overrideAnimation = true;

	//EYE
	public enum enumDirection
	{
		forward,
		right,
		up
	}

	public bool eyeMvtActive = true;
	public float convergenceFactor = 0.1f;

	public Vector3 localEyeDirectionVectorLeft;
	public Vector3 localEyeDirectionVectorRight;
	public enumDirection localHeadForwardDirection;
	public Vector3 localEyeForwardDirection;

	public GameObject Head;

	public Transform leftEyeTR;
	public Transform rightEyeTR;
	public Transform headTR;

	private static GameObject descendant;

	private Vector3 targetForHead;

	private Quaternion diffQuaternionForHeadOriention;
	private Quaternion diffQuaternionEyeToHeadLeft;
	private Quaternion diffQuaternionEyeToHeadRight;

	private float rightEyeFutureAngleYRotation;
	private float rightEyeFutureAngleXRotation;
	private float leftEyeFutureAngleYRotation;
	private float leftEyeFutureAngleXRotation;


	private float angleYBetweenHeadAndTarget = 0;
	private float angleXBetweenHeadAndTarget = 0;
	private Vector3 vectorHeadForwardTransform;
	private Vector3 vectorHeadTransformSave;
	private Vector3 correctedHeadTRUp;
	private Vector3 correctedHeadTRRight;
	private Vector3 correctedHeadTRForward;
	private Vector3 rightCamEyeVect;
	private Vector3 leftCamEyeVect;
	private Vector3 convergenceEyeTarget;
	private Vector3 dirNoseToFove;
	private GameObject leftEye;
	private GameObject rightEye;

	void Reset()
	{
		isActive = true;
		eyeMvtActive = true;
		overrideAnimation = true;
		rootNode = transform;

		BendingSegment[] bs = new BendingSegment[2];
		NonAffectedJoints[] naj = new NonAffectedJoints[2];

		GameObject Spine1 = ReturnDecendantOfParentContainsTwoPossibleNames(gameObject, "Spine1", "Spine01"); //because of Bip01 name for rocketbox
		Transform Spine1TR = null;
		if (Spine1 != null)
		{
			Spine1TR = Spine1.transform;
		}
		else
		{
			Debug.LogWarning("Segment about Spine 1 not found in agent when filling automatically Head Look Controller script in Editor");
		}

		GameObject Spine2 = ReturnDecendantOfParentWithContainsName(gameObject, "Spine", "2", true);
		Transform Spine2TR = null;
		if (Spine2 != null)
		{
			Spine2TR = Spine2.transform;
		}
		else
		{
			Debug.LogWarning("Segment about Spine 2 not found in agent when filling automatically Head Look Controller script in Editor");
		}

		bs[0] = new BendingSegment(Spine1TR, Spine2TR, 5, 0.01f, 50, 50, 5f);


		GameObject Neck = ReturnDecendantOfParentContainsTwoPossibleNames(gameObject, "Neck", "Neck01"); //because of Bip01 name for rocketbox
		Transform NeckTR = null;
		if (Neck != null)
		{
			NeckTR = Neck.transform;
		}
		else
		{
			Debug.LogWarning("Segment about Neck not found in agent when filling automatically Head Look Controller script in Editor");
		}

		Head = ReturnDecendantOfParentWithContainsName(gameObject, "Head", "", true);

		if (Head != null)
		{
			headTR = Head.transform;
		}
		else
		{
			Debug.LogWarning("Segment about Head not found in agent when filling automatically Head Look Controller script in Editor");
		}

		bs[1] = new BendingSegment(NeckTR, headTR, 5, 1f, 100, 60, 7);

		segments = bs;

		if (Head != null)
		{
			leftEye = ReturnDecendantOfParentWithContainsName(Head, "Eye", "R", false);
			if (leftEye != null)
			{
				leftEyeTR = leftEye.transform;
				naj[0] = new NonAffectedJoints(leftEyeTR, 0);
			}
			else
			{
				Debug.LogWarning("Segment about Left eye not found in agent when filling automatically Head Look Controller script in Editor");
			}

			rightEye = ReturnDecendantOfParentWithContainsName(Head, "Eye", "R", true);
			if (rightEye != null)
			{
				rightEyeTR = rightEye.transform;
				naj[1] = new NonAffectedJoints(rightEyeTR, 0);
			}
			else
			{
				Debug.LogWarning("Segment about Right eye not found in agent when filling automatically Head Look Controller script in Editor");
			}

			nonAffectedJoints = naj;

			Vector3 forwardAgent = this.transform.forward;
			float f = Vector3.Dot(forwardAgent, headTR.forward);
			float u = Vector3.Dot(forwardAgent, headTR.up);
			float r = Vector3.Dot(forwardAgent, headTR.right);

			if (f > r && f > u)
			{
				localHeadForwardDirection = enumDirection.forward;
			}
			else if (u > f && u > r)
			{
				localHeadForwardDirection = enumDirection.up;
			}
			else if (r > u && r > f)
			{
				localHeadForwardDirection = enumDirection.right;
			}
			else
			{
				Debug.LogError("local direction of head not found, fix before running");
				Debug.LogWarning("f" + f);
				Debug.LogWarning("r" + r);
				Debug.LogWarning("u" + u);
			}

			float fEye = Vector3.Dot(forwardAgent, leftEyeTR.forward);
			float uEye = Vector3.Dot(forwardAgent, leftEyeTR.up);
			float rEye = Vector3.Dot(forwardAgent, leftEyeTR.right);

			if (Math.Abs(fEye) > Math.Abs(rEye) && Math.Abs(fEye) > Math.Abs(uEye))
			{
				localEyeDirectionVectorLeft = Math.Sign(fEye) * leftEyeTR.forward; //new Vector3(0, 0, 1*Math.Sign(fEye));
				localEyeDirectionVectorRight = Math.Sign(fEye) * rightEyeTR.forward;
				localEyeForwardDirection = Vector3.forward * Math.Sign(fEye);
			}
			else if (Math.Abs(uEye) > Math.Abs(fEye) && Math.Abs(uEye) > Math.Abs(rEye))
			{
				localEyeDirectionVectorLeft = Math.Sign(uEye) * leftEyeTR.forward; //new Vector3(0, 1 * Math.Sign(uEye), 0);
				localEyeDirectionVectorRight = Math.Sign(uEye) * rightEyeTR.forward;
				localEyeForwardDirection = Vector3.up * Math.Sign(uEye);
			}
			else if (Math.Abs(rEye) > Math.Abs(uEye) && Math.Abs(rEye) > Math.Abs(fEye))
			{
				localEyeDirectionVectorLeft = Math.Sign(rEye) * leftEyeTR.forward; //new Vector3(1 * Math.Sign(rEye), 0, 0);
				localEyeDirectionVectorRight = Math.Sign(rEye) * rightEyeTR.forward;
				localEyeForwardDirection = Vector3.right * Math.Sign(rEye);
			}
			else
			{
				Debug.LogError("local direction of eye not found, fix before running if you need a rotation correction");
				localEyeDirectionVectorLeft = new Vector3(0, 0, 0);
				localEyeDirectionVectorRight = new Vector3(0, 0, 0);

				Debug.LogWarning("fEye" + fEye);
				Debug.LogWarning("rEye" + rEye);
				Debug.LogWarning("uEye" + uEye);
			}
		}
	}

	void Start()
	{		
		targetForHead = gazeTarget.transform.position;
		//convergenceEyeTargetObj = GameObject.CreatePrimitive(PrimitiveType.Cube);
		//convergenceEyeTargetObj.transform.localScale= new Vector3(0.1f, 0.1f, 0.1f);
		if (rootNode == null)
		{
			rootNode = transform;
		}

		Head = ReturnDecendantOfParentWithContainsName(gameObject, "Head", "", true);

		if (Head != null)
		{
			headTR = Head.transform;
			leftEye = ReturnDecendantOfParentWithContainsName(Head, "Eye", "R", false);
			if (leftEye != null)
			{
				leftEyeTR = leftEye.transform;
			}
			else
			{
				Debug.LogWarning("Segment about Left eye not found in agent when filling automatically Head Look Controller script in Editor");
			}

			rightEye = ReturnDecendantOfParentWithContainsName(Head, "Eye", "R", true);
			if (rightEye != null)
			{
				rightEyeTR = rightEye.transform;
			}
			else
			{
				Debug.LogWarning("Segment about Right eye not found in agent when filling automatically Head Look Controller script in Editor");
			}
		}
		else
		{
			Debug.LogWarning("Segment about Head not found in agent when filling automatically Head Look Controller script in Editor");
			return;
		}

		//get original difference of head for RocketBox characters
		switch (localHeadForwardDirection)
		{
			case enumDirection.forward:
				diffQuaternionForHeadOriention = Quaternion.FromToRotation(headTR.forward, this.transform.forward);
				break;
			case enumDirection.right:
				diffQuaternionForHeadOriention = Quaternion.FromToRotation(headTR.right, this.transform.forward);
				break;
			case enumDirection.up:
				diffQuaternionForHeadOriention = Quaternion.FromToRotation(headTR.up, this.transform.forward);
				break;
			default:
				Debug.LogWarning("no local head forward dir found");
				break;
		}

		correctedHeadTRUp = diffQuaternionForHeadOriention * headTR.up;
		correctedHeadTRRight = diffQuaternionForHeadOriention * headTR.right;
		correctedHeadTRForward = diffQuaternionForHeadOriention * headTR.forward;

		//diffQuaternionEyeToHeadLeft = Quaternion.FromToRotation(leftEyeTR.forward, correctedHeadTRForward);
		//diffQuaternionEyeToHeadRight = Quaternion.FromToRotation(rightEyeTR.forward, correctedHeadTRForward);
		//diffQuaternionEyeToHeadLeft = Quaternion.FromToRotation(correctedHeadTRForward, leftEyeTR.forward);
		//diffQuaternionEyeToHeadRight = Quaternion.FromToRotation(correctedHeadTRForward, rightEyeTR.forward);

		//diffQuaternionEyeToHeadLeft = Quaternion.FromToRotation(correctedHeadTRForward, -leftEyeTR.right);
		//diffQuaternionEyeToHeadLeft = Quaternion.FromToRotation(Vector3.forward, leftEyeTR.forward);
		diffQuaternionEyeToHeadLeft = Quaternion.FromToRotation(localEyeForwardDirection, Vector3.forward); 
		diffQuaternionEyeToHeadRight = Quaternion.FromToRotation(localEyeForwardDirection, Vector3.forward); 
		//diffQuaternionEyeToHeadRight = rightEyeTR.rotation;

		// Setup segments
		foreach (BendingSegment segment in segments)
		{
			Quaternion parentRot = segment.firstTransform.parent.rotation;
			Quaternion parentRotInv = Quaternion.Inverse(parentRot);
			segment.referenceLookDir =
				parentRotInv * rootNode.rotation * headLookVector.normalized;
			segment.referenceUpDir =
				parentRotInv * rootNode.rotation * headUpVector.normalized;
			segment.angleH = 0;
			segment.angleV = 0;
			segment.dirUp = segment.referenceUpDir;

			segment.chainLength = 1;
			Transform t = segment.lastTransform;
			while (t != segment.firstTransform && t != t.root)
			{
				segment.chainLength++;
				t = t.parent;
			}

			segment.origRotations = new Quaternion[segment.chainLength];
			t = segment.lastTransform;
			for (int i = segment.chainLength - 1; i >= 0; i--)
			{
				segment.origRotations[i] = t.localRotation;
				t = t.parent;
			}
		}
	}
	void IKBody()
	{
		if (Time.deltaTime == 0)
			return;

		// Remember initial directions of joints that should not be affected
		Vector3[] jointDirections = new Vector3[nonAffectedJoints.Length];
		for (int i = 0; i < nonAffectedJoints.Length; i++)
		{
			foreach (Transform child in nonAffectedJoints[i].joint)
			{
				jointDirections[i] = child.position - nonAffectedJoints[i].joint.position;
				break;
			}
		}

		// Handle each segment
		foreach (BendingSegment segment in segments)
		{
			Transform t = segment.lastTransform;
			if (overrideAnimation)
			{
				//Debug.Log("hello" + t);
				for (int i = segment.chainLength - 1; i >= 0; i--)
				{
					//Debug.Log("i  "+ i + t);
					t.localRotation = segment.origRotations[i];
					t = t.parent;
				}
			}

			Quaternion parentRot = segment.firstTransform.parent.rotation;
			Quaternion parentRotInv = Quaternion.Inverse(parentRot);

			// Desired look direction in world space
			Vector3 lookDirWorld = (targetForHead - segment.lastTransform.position).normalized;

			// Desired look directions in neck parent space
			Vector3 lookDirGoal = (parentRotInv * lookDirWorld);

			// Get the horizontal and vertical rotation angle to look at the target
			float hAngle = AngleAroundAxis(
				segment.referenceLookDir, lookDirGoal, segment.referenceUpDir
			);

			Vector3 rightOfTarget = Vector3.Cross(segment.referenceUpDir, lookDirGoal);

			Vector3 lookDirGoalinHPlane =
				lookDirGoal - Vector3.Project(lookDirGoal, segment.referenceUpDir);

			float vAngle = AngleAroundAxis(
				lookDirGoalinHPlane, lookDirGoal, rightOfTarget
			);

			// Handle threshold angle difference, bending multiplier,
			// and max angle difference here
			float hAngleThr = Mathf.Max(
				0, Mathf.Abs(hAngle) - segment.thresholdAngleDifference
			) * Mathf.Sign(hAngle);

			float vAngleThr = Mathf.Max(
				0, Mathf.Abs(vAngle) - segment.thresholdAngleDifference
			) * Mathf.Sign(vAngle);

			hAngle = Mathf.Max(
				Mathf.Abs(hAngleThr) * Mathf.Abs(segment.bendingMultiplier),
				Mathf.Abs(hAngle) - segment.maxAngleDifference
			) * Mathf.Sign(hAngle) * Mathf.Sign(segment.bendingMultiplier);

			vAngle = Mathf.Max(
				Mathf.Abs(vAngleThr) * Mathf.Abs(segment.bendingMultiplier),
				Mathf.Abs(vAngle) - segment.maxAngleDifference
			) * Mathf.Sign(vAngle) * Mathf.Sign(segment.bendingMultiplier);

			// Handle max bending angle here
			hAngle = Mathf.Clamp(hAngle, -segment.maxBendingAngle, segment.maxBendingAngle);
			vAngle = Mathf.Clamp(vAngle, -segment.maxBendingAngle, segment.maxBendingAngle);

			Vector3 referenceRightDir =
				Vector3.Cross(segment.referenceUpDir, segment.referenceLookDir);

			// Lerp angles
			segment.angleH = Mathf.Lerp(
				segment.angleH, hAngle, Time.deltaTime * segment.responsiveness
			);
			segment.angleV = Mathf.Lerp(
				segment.angleV, vAngle, Time.deltaTime * segment.responsiveness
			);

			// Get direction
			lookDirGoal = Quaternion.AngleAxis(segment.angleH, segment.referenceUpDir)
				* Quaternion.AngleAxis(segment.angleV, referenceRightDir)
				* segment.referenceLookDir;

			// Make look and up perpendicular
			Vector3 upDirGoal = segment.referenceUpDir;
			Vector3.OrthoNormalize(ref lookDirGoal, ref upDirGoal);

			// Interpolated look and up directions in neck parent space
			Vector3 lookDir = lookDirGoal;
			segment.dirUp = Vector3.Slerp(segment.dirUp, upDirGoal, Time.deltaTime * 5);
			Vector3.OrthoNormalize(ref lookDir, ref segment.dirUp);

			// Look rotation in world space
			Quaternion lookRot = (
				(parentRot * Quaternion.LookRotation(lookDir, segment.dirUp))
				* Quaternion.Inverse(
					parentRot * Quaternion.LookRotation(
						segment.referenceLookDir, segment.referenceUpDir
					)
				)
			);

			// Distribute rotation over all joints in segment
			Quaternion dividedRotation =
				Quaternion.Slerp(Quaternion.identity, lookRot, effect / segment.chainLength);
			t = segment.lastTransform;
			for (int i = 0; i < segment.chainLength; i++)
			{
				t.rotation = dividedRotation * t.rotation;
				t = t.parent;
			}
		}

		// Handle non affected joints
		for (int i = 0; i < nonAffectedJoints.Length; i++)
		{
			Vector3 newJointDirection = Vector3.zero;

			foreach (Transform child in nonAffectedJoints[i].joint)
			{
				newJointDirection = child.position - nonAffectedJoints[i].joint.position;
				break;
			}

			Vector3 combinedJointDirection = Vector3.Slerp(
				jointDirections[i], newJointDirection, nonAffectedJoints[i].effect
			);

			nonAffectedJoints[i].joint.rotation = Quaternion.FromToRotation(
				newJointDirection, combinedJointDirection
			) * nonAffectedJoints[i].joint.rotation;
		}
	}


    private void OnAnimatorMove()
	{
		if (GetComponent<Animator>() != null && GetComponent<Animator>().enabled)
		{
			transform.position = GetComponent<Animator>().rootPosition;
		}

		Vector3 nosPos = (leftEyeTR.position + rightEyeTR.position) / 2;

		correctedHeadTRUp = diffQuaternionForHeadOriention * headTR.up;
		correctedHeadTRRight = diffQuaternionForHeadOriention * headTR.right;
		correctedHeadTRForward = diffQuaternionForHeadOriention * headTR.forward;

		dirNoseToFove = nosPos - gazeTarget.transform.position;
		convergenceEyeTarget = gazeTarget.transform.position + dirNoseToFove * convergenceFactor;

		rightCamEyeVect = convergenceEyeTarget - rightEyeTR.position;
		leftCamEyeVect = convergenceEyeTarget - leftEyeTR.position;

		vectorHeadForwardTransform= headTR.forward;
		computeFutureEyesRotations();
		computeFutureHeadRotation();
	}

	void LateUpdate()
	{
		if (isActive)
		{
			if (GetComponent<Animator>() != null && GetComponent<Animator>().enabled)
			{
				headTR.forward = vectorHeadForwardTransform;
			}
			else
			{
				Vector3 nosPos = (leftEyeTR.position + rightEyeTR.position) / 2;

				correctedHeadTRUp = diffQuaternionForHeadOriention * headTR.up;
				correctedHeadTRRight = diffQuaternionForHeadOriention * headTR.right;
				correctedHeadTRForward = diffQuaternionForHeadOriention * headTR.forward;

				dirNoseToFove = nosPos - gazeTarget.transform.position;
				convergenceEyeTarget = gazeTarget.transform.position + dirNoseToFove * convergenceFactor;

				rightCamEyeVect = convergenceEyeTarget - rightEyeTR.position;
				leftCamEyeVect = convergenceEyeTarget - leftEyeTR.position;

				computeFutureEyesRotations();
				//Debug.Log("rightEyeFutureAngleYRotation" + rightEyeFutureAngleYRotation);
				computeFutureHeadRotation();
			}

			if (eyeMvtActive)
			{
				doEyesRotations();
			}


			IKBody();

			if (!isReachableEyeRotation())// && !hasReachedTargetWithHead())
			{
				targetForHead = gazeTarget.transform.position;
			}
		}
		//convergenceEyeTargetObj.transform.position = convergenceEyeTarget;
	}

	void computeFutureHeadRotation()
	{
		Vector3 headToTargetVect = convergenceEyeTarget - headTR.position;

		//Vector3 correctedHeadTRUp = diffQuaternionForHeadOriention * headTR.up;
		//Vector3 correctedHeadTRRight = diffQuaternionForHeadOriention * headTR.right;
		//Vector3 correctedHeadTRForward =diffQuaternionForHeadOriention * headTR.forward;

		switch (localHeadForwardDirection)
		{
			case enumDirection.forward:
				angleYBetweenHeadAndTarget = Vector3.SignedAngle(Vector3.ProjectOnPlane(correctedHeadTRForward, correctedHeadTRUp), Vector3.ProjectOnPlane(headToTargetVect, correctedHeadTRUp), correctedHeadTRUp);
				angleXBetweenHeadAndTarget = Vector3.SignedAngle(Vector3.ProjectOnPlane(correctedHeadTRForward, -correctedHeadTRRight), Vector3.ProjectOnPlane(headToTargetVect, -correctedHeadTRRight), -correctedHeadTRRight);
				break;
			case enumDirection.right:
				//TODO
				break;
			case enumDirection.up:
				angleYBetweenHeadAndTarget = Vector3.SignedAngle(Vector3.ProjectOnPlane(correctedHeadTRUp, -correctedHeadTRRight), Vector3.ProjectOnPlane(headToTargetVect, -correctedHeadTRRight), -correctedHeadTRRight);
				angleXBetweenHeadAndTarget = Vector3.SignedAngle(Vector3.ProjectOnPlane(correctedHeadTRUp, correctedHeadTRForward), Vector3.ProjectOnPlane(headToTargetVect, correctedHeadTRForward), correctedHeadTRForward);
				break;
			default:
				Debug.Log("no eyes only movement, head always turns in this configuration ; fix head forward search in reset()");
				break;
		}

		//if (Mathf.Abs(angleYBetweenHeadAndTarget) > 20)
		//{
		//	segments[0].responsiveness = 5;
		//	segments[1].responsiveness = 7;
		//}
		//else
		//{
		//	segments[0].responsiveness = 3;
		//	segments[1].responsiveness = 4;
		//}
	}

	private void computeFutureEyesRotations()
	{
		switch (localHeadForwardDirection)
		{
			case enumDirection.forward:
				//rightEyeFutureAngleYRotation = Vector3.SignedAngle(Vector3.ProjectOnPlane(headTR.forward, headTR.up), Vector3.ProjectOnPlane(rightCamEyeVect, headTR.up), headTR.up);
				//rightEyeFutureAngleXRotation = Vector3.SignedAngle(Vector3.ProjectOnPlane(headTR.forward, -headTR.right), Vector3.ProjectOnPlane(rightCamEyeVect, -headTR.right), -headTR.right);
				//leftEyeFutureAngleYRotation = Vector3.SignedAngle(Vector3.ProjectOnPlane(headTR.forward, headTR.up), Vector3.ProjectOnPlane(leftCamEyeVect, headTR.up), headTR.up);
				//leftEyeFutureAngleXRotation = Vector3.SignedAngle(Vector3.ProjectOnPlane(headTR.forward, -headTR.right), Vector3.ProjectOnPlane(leftCamEyeVect, -headTR.right), -headTR.right);
				rightEyeFutureAngleYRotation = Vector3.SignedAngle(Vector3.ProjectOnPlane(correctedHeadTRForward, correctedHeadTRUp), Vector3.ProjectOnPlane(rightCamEyeVect, correctedHeadTRUp), correctedHeadTRUp);
				rightEyeFutureAngleXRotation = Vector3.SignedAngle(Vector3.ProjectOnPlane(correctedHeadTRForward, -correctedHeadTRRight), Vector3.ProjectOnPlane(rightCamEyeVect, -correctedHeadTRRight), -correctedHeadTRRight);
				leftEyeFutureAngleYRotation = Vector3.SignedAngle(Vector3.ProjectOnPlane(correctedHeadTRForward, correctedHeadTRUp), Vector3.ProjectOnPlane(leftCamEyeVect, correctedHeadTRUp), correctedHeadTRUp);
				leftEyeFutureAngleXRotation = Vector3.SignedAngle(Vector3.ProjectOnPlane(correctedHeadTRForward, -correctedHeadTRRight), Vector3.ProjectOnPlane(leftCamEyeVect, -correctedHeadTRRight), -correctedHeadTRRight);
				break;
			case enumDirection.right:
				break;
			case enumDirection.up:
				rightEyeFutureAngleYRotation = Vector3.SignedAngle(Vector3.ProjectOnPlane(correctedHeadTRUp, -correctedHeadTRRight), Vector3.ProjectOnPlane(rightCamEyeVect, -correctedHeadTRRight), -correctedHeadTRRight);
				rightEyeFutureAngleXRotation = Vector3.SignedAngle(Vector3.ProjectOnPlane(correctedHeadTRUp, correctedHeadTRForward), Vector3.ProjectOnPlane(rightCamEyeVect, correctedHeadTRForward), correctedHeadTRForward);
				leftEyeFutureAngleYRotation = Vector3.SignedAngle(Vector3.ProjectOnPlane(correctedHeadTRUp, correctedHeadTRForward), Vector3.ProjectOnPlane(leftCamEyeVect, correctedHeadTRForward), correctedHeadTRForward);
				leftEyeFutureAngleXRotation = Vector3.SignedAngle(Vector3.ProjectOnPlane(correctedHeadTRUp, correctedHeadTRForward), Vector3.ProjectOnPlane(leftCamEyeVect, correctedHeadTRForward), correctedHeadTRForward);
				break;
			default:
				Debug.Log("no eyes only movement, head always turns in this configuration ; fix head forward search in reset()");
				break;
		}
	}
	
	private bool isReachableEyeRotation()
	{
		bool isOKForRightEye = rightEyeFutureAngleYRotation > -35 && rightEyeFutureAngleYRotation < 35
		&& rightEyeFutureAngleXRotation > -15 && rightEyeFutureAngleXRotation < 10;//35 -35 25 -30

		bool isOKForLeftEye = leftEyeFutureAngleYRotation > -35 && leftEyeFutureAngleYRotation < 35
		&& leftEyeFutureAngleXRotation > -15 && leftEyeFutureAngleXRotation < 10;//35 -35 25 -30

		return isOKForRightEye && isOKForLeftEye;
	}

	private bool hasReachedTargetWithHead()
	{
		return angleXBetweenHeadAndTarget < 10 && angleXBetweenHeadAndTarget > -10 && angleYBetweenHeadAndTarget < 10 && angleYBetweenHeadAndTarget > -10;
	}
	

	private void doEyesRotations()
	{

		leftEyeTR.rotation = Quaternion.LookRotation(convergenceEyeTarget - leftEyeTR.position, Vector3.up) * diffQuaternionEyeToHeadLeft;
		rightEyeTR.rotation = Quaternion.LookRotation(convergenceEyeTarget - rightEyeTR.position, Vector3.up) * diffQuaternionEyeToHeadRight;

		//RB & CC: lookAt
		//Vector3 targetDirection = rightEyeTR.position - targetForEye.transform.position;
		//rightEyeTR.forward= Vector3.RotateTowards(rightEyeTR.forward, targetDirection, 1f * Time.deltaTime, 0.0f);
		//rightEyeTR.LookAt(convergenceEyeTarget);
		//rightEyeTR.rotation = R.transform.rotation;
		//or rightEyeTR.LookAt(convergenceEyeTarget, new Vector3(0, 1, 0));
		// we move this function after the clamping 
		//rightEyeTR.rotation *= Quaternion.FromToRotation(localEyeDirectionVector , Vector3.forward);
		//rightEyeTR.rotation = R.transform.rotation;
		//rightEyeTR.rotation *= diffQuaternionEyeToHeadRight;
		//rightEyeTR.rotation *= headTR.rotation;

		//leftEyeTR.rotation *= Quaternion.FromToRotation(localEyeDirectionVector, Vector3.forward);
	}

	// The angle between dirA and dirB around axis
	public static float AngleAroundAxis(Vector3 dirA, Vector3 dirB, Vector3 axis)
	{
		// Project A and B onto the plane orthogonal target axis
		dirA = dirA - Vector3.Project(dirA, axis);
		dirB = dirB - Vector3.Project(dirB, axis);

		// Find (positive) angle between A and B
		float angle = Vector3.Angle(dirA, dirB);

		// Return angle multiplied with 1 or -1
		return angle * (Vector3.Dot(axis, Vector3.Cross(dirA, dirB)) < 0 ? -1 : 1);
	}
	
	public static GameObject ReturnDecendantOfParent(GameObject parent, string descendantName)
	{
		foreach (Transform child in parent.transform)
		{
			if (child.name == descendantName)
			{
				descendant = child.gameObject;
				break;
			}
			else
			{
				ReturnDecendantOfParent(child.gameObject, descendantName);
			}
		}
		return descendant;
	}

	public static GameObject ReturnDecendantOfParentContainsTwoPossibleNames(GameObject parent, string descendantNameA, string descendantNameB)
	{
		foreach (Transform child in parent.transform)
		{
			if (child.name.Contains(descendantNameA) || child.name.Contains(descendantNameB))
			{
				descendant = child.gameObject;
				break;
			}
			else
			{
				ReturnDecendantOfParentContainsTwoPossibleNames(child.gameObject, descendantNameA, descendantNameB);
			}
		}
		return descendant;
	}

	public static GameObject ReturnDecendantOfParentWithContainsName(GameObject parent, string descendantName, string descendantExtraPartOfName, bool extraToSearchOrAvoid)
	{
		foreach (Transform child in parent.transform)
		{
			if (child.name.Contains(descendantName))
			{
				bool contains = child.name.Contains(descendantExtraPartOfName);
				bool containOrAvoid = extraToSearchOrAvoid ? contains : !contains;
				if (descendantExtraPartOfName == "" || containOrAvoid)
				{
					descendant = child.gameObject;
					break;
				}
				else
				{
					ReturnDecendantOfParentWithContainsName(child.gameObject, descendantName, descendantExtraPartOfName, extraToSearchOrAvoid);
				}
			}
			else
			{
				ReturnDecendantOfParentWithContainsName(child.gameObject, descendantName, descendantExtraPartOfName, extraToSearchOrAvoid);
			}
		}
		return descendant;
	}
	
}