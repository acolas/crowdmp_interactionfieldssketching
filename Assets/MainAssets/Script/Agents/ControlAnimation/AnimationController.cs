﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


namespace CrowdMP.Core
{

    /// <summary>
    /// Control the animation from LocomotionFinal
    /// </summary>
    public class AnimationController : MonoBehaviour
    {

        private Vector3 _oldPosition;
        private Animator _objectAnimator;
        private Agent _agent;
        private uint agentID;
        private Vector3 _oldVelocity;
        private Quaternion _oldRotation;
        private float _speed = 0;

        public bool IsIdle { get { return _speed < 0.00001; } }

        // Use this for initialization
        void Start()
        {
            if (_objectAnimator != null)
                return;

            _agent = GetComponent<Agent>();

            if (_agent!=null)
                agentID = _agent.GetID();

            _oldPosition = this.transform.position;
            _objectAnimator = this.GetComponent<Animator>();
            _objectAnimator.applyRootMotion = false;
            _objectAnimator.speed = 0;
            _objectAnimator.SetFloat("Speed", 0);
            _objectAnimator.SetFloat("AngularSpeed", 0);

#if MIDDLEVR
        _objectAnimator.cullingMode = AnimatorCullingMode.CullUpdateTransforms;
        _objectAnimator.enabled = false;
#endif
        }

        public void temp()
        {
            //idleAnimationID = ((TrialAgent) LoaderConfig.agentsInfo[(int)agentID - 2]).idleAnimationID;
            //_objectAnimator.SetFloat("IdleAnimationID", idleAnimationID);
            //applauseAnimationID = ((TrialMultiControlLawAgent) LoaderConfig.agentsInfo[(int)agentID - 2]).applauseAnimationID;
            //lookAroundAnimationID = ((TrialMultiControlLawAgent) LoaderConfig.agentsInfo[(int)agentID - 2]).lookAroundAnimationID;
            //_objectAnimator.SetFloat("ApplauseAnimationID", applauseAnimationID);
            //_objectAnimator.SetFloat("LookAroundAnimationID", lookAroundAnimationID);
        }

        public void setAnimationType(int type)
        {
            if (_objectAnimator == null)
                Start();
            _objectAnimator.SetInteger("AnimationType", type);
        }

        public void setAnimOffset(float offset)
        {
            if (_objectAnimator == null)
                Start();
            _objectAnimator.SetFloat("CycleOffset", offset);
        }

        // Update is called once per frame
        void Update()
        {

            if (ToolsTime.DeltaTime != 0)
            {
                _objectAnimator.speed = 1;
                //If object is ready and the time isn't in pause, play the animation as fast as the virtual human speed.
                float animationSpeed;
                Vector3 position = _oldPosition - this.transform.position;
                float angle = Vector3.SignedAngle(_oldVelocity, this.transform.forward, this.transform.up);
                float angularSpeed = angle / ToolsTime.DeltaTime;


                animationSpeed = position.magnitude / ToolsTime.DeltaTime;
                _speed = animationSpeed;
                _objectAnimator.SetFloat("Speed", animationSpeed);
                _objectAnimator.SetFloat("AngularSpeed", angularSpeed, 100f, ToolsTime.DeltaTime);
                _objectAnimator.SetBool("One-Eighty", false);
                _objectAnimator.SetFloat("TurnAngle", 0);

                if (IsIdle)
                {
                    if (Mathf.Abs(angle) >= 45)
                    {
                        _objectAnimator.SetBool("One-Eighty", true);
                        _objectAnimator.SetFloat("TurnAngle", angle);
                    }
                }



            }
            else
            {
                // if the time is in pause, pause the animation.
                if (_objectAnimator != null)
                    _objectAnimator.speed = 0;
            }

            _oldPosition = this.transform.position;
            _oldRotation = this.transform.rotation;
            _oldVelocity = this.transform.forward;

#if MIDDLEVR
        _objectAnimator.Update(ToolsTime.DeltaTime);
#endif
        }

        private void LateUpdate()
        {
            //if (ToolsInput.GetKeyDown(KeyCode.Z))
            //{
            //    idleAnimationID--;
            //    if (idleAnimationID < -35)
            //        idleAnimationID = 12;
            //    ToolsDebug.log("idleAnimationID="+idleAnimationID);
            //    _objectAnimator.SetFloat("IdleAnimationID", idleAnimationID);
            //}
            //if (ToolsInput.GetKeyDown(KeyCode.X))
            //{
            //    idleAnimationID++;
            //    if (idleAnimationID > 12)
            //        idleAnimationID = -35;
            //    ToolsDebug.log("idleAnimationID=" + idleAnimationID);
            //    _objectAnimator.SetFloat("IdleAnimationID", idleAnimationID);
            //}
        }
    }
}

