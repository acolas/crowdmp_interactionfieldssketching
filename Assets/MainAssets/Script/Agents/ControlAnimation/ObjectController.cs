﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ObjectController : MonoBehaviour
{
    private Animator _objectAnimator;
    private float LastAnimationType = -1;

    // hand
    protected string NameHand;
    protected GameObject _rightHand;

    // Phone
    protected GameObject _phone;
    protected Vector3 _phonePos;
    protected Vector3 _phoneRot;

    // bottle 
    protected GameObject _bottle;
    protected Vector3 _bottlePos;
    protected Vector3 _bottleRot;

    // Start is called before the first frame update
    public virtual void Awake()
    {
        _objectAnimator = this.GetComponent<Animator>();
        RecursiveFindPositionChild(this.transform, this.NameHand);
    }

    public void InitObjects()
    {
        if (_rightHand == null)
            RecursiveFindPositionChild(this.transform, this.NameHand);
        InitPhone();
        InitBottleMan();
        ActiveObject();
    }

    protected void RecursiveFindPositionChild(Transform parent, string childName)
    {
        foreach (Transform child in parent)
        {
            if (child.name == childName)
            {
                _rightHand = child.gameObject;
            }
            else
            {
                RecursiveFindPositionChild(child, childName);
            }
        }
    }

    private void InitPhone()
    {
        if (_phone == null)
        {
            GameObject Objects = GameObject.FindGameObjectWithTag("3DObjects");
            GameObject PhoneObject = Objects.transform.Find("phone").gameObject;
            _phone = (GameObject)GameObject.Instantiate(PhoneObject);
        }
        _phone.transform.parent = _rightHand.transform;
        _phone.transform.localPosition = this._phonePos;
        Quaternion tmp = new Quaternion();
        tmp.eulerAngles = this._phoneRot;
        _phone.transform.localRotation = tmp;
    }

    private void InitBottleMan()
    {
        if (_bottle == null)
        {
            GameObject Objects = GameObject.FindGameObjectWithTag("3DObjects");
            GameObject BottleObject = Objects.transform.Find("bottle").gameObject;
            _bottle = (GameObject)GameObject.Instantiate(BottleObject);
        }
        _bottle.transform.parent = this._rightHand.transform;
        _bottle.transform.localPosition = this._bottlePos;
        Quaternion tmp = new Quaternion();
        tmp.eulerAngles = this._bottleRot;
        this._bottle.transform.localRotation = tmp;
    }

    public void ActiveObject()
    {
        if (_phone != null)
            _phone.SetActive(_objectAnimator.GetInteger("AnimationType") == 4);
        if (_bottle != null)
            _bottle.SetActive(_objectAnimator.GetInteger("AnimationType") == 5);
    }

    // Update is called once per frame
    public void Update()
    {
        /*if (Input.GetKeyDown(KeyCode.I))
            InitObjects();
        */
        if (LastAnimationType != _objectAnimator.GetInteger("AnimationType"))
        {
            LastAnimationType = _objectAnimator.GetInteger("AnimationType");
            ActiveObject();
        }
    }
}
