﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using System;
using System.Xml.Serialization;
using UnityEngine;

namespace CrowdMP.Core
{

    /// <summary>
    /// Law controlling the speed of an agent to keep moving forward
    /// </summary>
    public class LawMoveStraight : ControlLaw
    {
        [XmlAttribute]
        public float speedCurrent;
        [XmlAttribute]
        public float speedDefault;
        [XmlAttribute]
        public float accelerationMax;

        public LawMoveStraight()
        {
            speedCurrent = 1.33f;
            speedDefault = 1.33f;
            accelerationMax = 0.8f;
        }

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <param name="speed">speed of the forward movement</param>
        /// <param name="acceleration">acceleration use to reach the speed</param>
        public LawMoveStraight(float speed, float acceleration)
        {
            speedCurrent = 0;
            speedDefault = speed;
            accelerationMax = acceleration;

        }

        public bool computeGlobalMvt(float deltaTime, out Vector3 translation, out Vector3 rotation)
        {
            translation = new Vector3(0, 0, 0);
            rotation = new Vector3(0, 0, 0);
            float newSpeed = speedCurrent;

            /* Cannot control */
            if (speedCurrent < speedDefault)
                newSpeed = Math.Min(speedCurrent + deltaTime * accelerationMax, speedDefault);
            else
                newSpeed = Math.Max(speedCurrent - deltaTime * accelerationMax, speedDefault);
            translation.z = newSpeed * deltaTime;
            speedCurrent = newSpeed;

            return true;
        }

        public bool applyMvt(Agent a, Vector3 translation, Vector3 rotation)
        {
            //a.Rotate(rotation);
            a.Translate(translation);
            return true;
        }


        public void initialize(Agent a)
        {

        }
    }
}