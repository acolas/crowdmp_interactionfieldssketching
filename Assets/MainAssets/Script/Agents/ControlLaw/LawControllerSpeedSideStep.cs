﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using System;
using System.Xml.Serialization;
using UnityEngine;

namespace CrowdMP.Core
{

    /// <summary>
    /// Law controlling speed and side speed of an agent from a device input (Made for player)
    /// </summary>
    public class LawControllerSpeedSideStep : ControlLaw
    {
        [XmlAttribute]
        public float speedCurrent;
        [XmlAttribute]
        public float speedDefault;
        [XmlAttribute]
        public float speedVariation;

        [XmlAttribute]
        public float sideSpeed;
        [XmlAttribute]
        public float accelerationMax;
        [XmlAttribute]
        public float timeBeforeControl;

        public LawControllerSpeedSideStep()
        {
            speedCurrent = 0.0f;
            speedDefault = 1.8f;
            accelerationMax = 0.8f;
            sideSpeed = 0.5f;
            timeBeforeControl = 0;
            speedVariation = 0.5f;
        }

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <param name="speed">Speed.</param>
        /// <param name="acceleration">Acceleration.</param>
        /// <param name="side_Speed">Angular speed.</param>
        /// <param name="timeBC">Time before control.</param>
        /// <param name="speedOffset">Speed offset.</param>
        public LawControllerSpeedSideStep(float speed, float acceleration, float side_Speed, float timeBC, float speedOffset)
        {
            speedCurrent = 0;

            speedDefault = speed;
            accelerationMax = acceleration;
            sideSpeed = side_Speed;
            timeBeforeControl = timeBC;

            speedVariation = speedOffset;
        }



        public bool computeGlobalMvt(float deltaTime,
                                        out Vector3 translation,
                                        out Vector3 rotation)
        {
            translation = new Vector3(0, 0, 0);
            rotation = new Vector3(0, 0, 0);

            float newSpeed = speedCurrent;
            if (timeBeforeControl > 0)
            {
                /* Cannot control */
                if (speedCurrent < speedDefault)
                    newSpeed = Math.Min(speedCurrent + deltaTime * accelerationMax, speedDefault);
                else
                    newSpeed = Math.Max(speedCurrent - deltaTime * accelerationMax, speedDefault);
                translation.z = newSpeed * deltaTime;
                timeBeforeControl = timeBeforeControl - deltaTime;
            }
            else
            {
                /* Can control */

                float desiredSpeed = ToolsInput.getAxisValue(ToolsAxis.Vertical) * speedVariation + speedDefault;
                if (speedCurrent < desiredSpeed)
                    newSpeed = Math.Min(speedCurrent + deltaTime * accelerationMax, desiredSpeed);
                else
                    newSpeed = Math.Max(speedCurrent - deltaTime * accelerationMax, desiredSpeed);


                translation.z = newSpeed * deltaTime;
                translation.x = sideSpeed * deltaTime * ToolsInput.getAxisValue(ToolsAxis.Horizontal);
            }

            speedCurrent = newSpeed;

            return true;
        }

        public bool applyMvt(Agent a, Vector3 translation, Vector3 rotation)
        {
            a.Rotate(rotation);
            a.Translate(translation);
            return true;
        }

        public void initialize(Agent a)
        {
        }
    }
}