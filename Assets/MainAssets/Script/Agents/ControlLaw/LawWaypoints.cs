﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;
using UnityEngine.AI;

namespace CrowdMP.Core
{

    /// <summary>
    /// Law controlling the speed and orientation of an agent to reach a list of waypoints one after the other
    /// </summary>
    public class LawWaypoints : ControlLaw
    {
        [XmlAttribute]
        public float speedCurrent;
        [XmlAttribute]
        public float speedDefault;
        [XmlAttribute]
        public float accelerationMax;
        [XmlAttribute]
        public float angularSpeed;
        [XmlAttribute]
        public float reachedDist;
        [XmlAttribute]
        public bool isLooping;


        [XmlArray("Waypoints")]
        [XmlArrayItem("Waypoint")]
        public List<ConfigVect> goals;

        private int currGoal = 0;
        private Agent linkedAgent;

        public LawWaypoints()
        {
            speedCurrent = 0;
            speedDefault = 1.33f;
            accelerationMax = 0.8f;
            reachedDist = 0.5f;
            angularSpeed = 360 * 100;
            isLooping = false;
            goals = new List<ConfigVect>();
        }

        public bool computeGlobalMvt(float deltaTime, out Vector3 translation, out Vector3 rotation)
        {
            translation = new Vector3(0, 0, 0);
            rotation = new Vector3(0, 0, 0);

            // Check goal
            Vector3 direction;
            if (currGoal >= goals.Count)
            {
                direction = new Vector3(0, 0, 0);
                return false;
            }
            else
            {


                direction = goals[currGoal].vect - linkedAgent.Position;
                direction.y = 0;
                if (direction.magnitude < reachedDist)
                    currGoal = isLooping ? (currGoal + 1) % goals.Count : (currGoal + 1);

                direction = direction.magnitude * (Quaternion.RotateTowards(linkedAgent.transform.rotation, Quaternion.LookRotation(direction), angularSpeed * deltaTime) * Vector3.forward);

            }

            float newSpeed = direction.magnitude / deltaTime;
            if (newSpeed > speedDefault)
                newSpeed = speedDefault;

            /* Cannot control */
            if (speedCurrent < newSpeed)
                newSpeed = Math.Min(speedCurrent + deltaTime * accelerationMax, newSpeed);
            else
                newSpeed = Math.Max(speedCurrent - deltaTime * accelerationMax, newSpeed);



            if (direction.magnitude == 0)
            {
                direction = linkedAgent.transform.forward;
            }

            translation.z = newSpeed * deltaTime;

            rotation = direction;
            //Quaternion q = Quaternion.LookRotation(direction);
            //rotation = q.eulerAngles - linkedAgent.transform.rotation.eulerAngles;

            //Quaternion relative = q * Quaternion.Inverse(linkedAgent.transform.rotation);
            //rotation = relative.eulerAngles;

            speedCurrent = newSpeed;

            return true;
        }

        public bool applyMvt(Agent a, Vector3 translation, Vector3 rotation)
        {
            if (translation.sqrMagnitude != 0)
                a.transform.rotation = Quaternion.LookRotation(rotation);

            a.Translate(translation);
            return true;
        }

        public void initialize(Agent a)
        {
            linkedAgent = a;
        }
    }
}