﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using System;
using System.Xml.Serialization;
using UnityEngine;

namespace CrowdMP.Core
{

    /// <summary>
    /// Law controlling speed and rotation of an agent from a device input (Made for player)
    /// </summary>
    public class LawControllerSpeedAngle : ControlLaw
    {
        [XmlAttribute]
        public float speedCurrent;
        public float speedCurrentLR;
        [XmlAttribute]
        public float speedDefault;
        [XmlAttribute]
        public float speedVariation;

        [XmlAttribute]
        public float angularSpeed;
        [XmlAttribute]
        public float accelerationMax;
        [XmlAttribute]
        public float timeBeforeControl;

        public LawControllerSpeedAngle()
        {
            speedCurrent = 0.0f;
            speedCurrentLR = 0.0f;
            speedDefault = 0.0f;
            accelerationMax = 0.8f;
            angularSpeed = 30f;
            timeBeforeControl = 0;
            speedVariation = 1.33f;
        }

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <param name="speed">Speed.</param>
        /// <param name="acceleration">Acceleration.</param>
        /// <param name="angularSpeed">Angular speed.</param>
        /// <param name="timeBeforeControl">Time before control.</param>
        /// <param name="speedOffset">Speed offset.</param>
        public LawControllerSpeedAngle(float speed, float acceleration, float angular_Speed, float timeBC, float speedOffset)
        {
            speedCurrent = 0;

            speedDefault = speed;
            accelerationMax = acceleration;
            angularSpeed = angular_Speed;
            timeBeforeControl = timeBC;

            speedVariation = speedOffset;
        }



        public bool computeGlobalMvt(float deltaTime,
                                        out Vector3 translation,
                                        out Vector3 rotation)
        {
            translation = new Vector3(0, 0, 0);
            rotation = new Vector3(0, 0, 0);

            float newSpeed = speedCurrent;
            float newSpeedLR = speedCurrentLR;

            if (timeBeforeControl > 0)
            {
                // Cannot control
                if (speedCurrent < speedDefault)
                    newSpeed = Math.Min(speedCurrent + deltaTime * accelerationMax, speedDefault);
                else
                    newSpeed = Math.Max(speedCurrent - deltaTime * accelerationMax, speedDefault);
                translation.z = newSpeed * deltaTime;
                timeBeforeControl = timeBeforeControl - deltaTime;
            }
            else
            {
                Vector2 translationValue = XRInputEvents.currentXRInputEvents.TranslationVector2Value;
                float translationForwardBackwardValue = translationValue.y;
                float translationLeftRightValue = translationValue.x;

                Vector2 rotationValue = XRInputEvents.currentXRInputEvents.RotationVector2Value;
                float rotationForwardBackwardValue = rotationValue.y;
                float rotationLeftRightValue = rotationValue.x;

                //float verticalValue = XRInteractions.getAxisValue(ToolsAxis.Vertical);
                //float horizontalValue = XRInteractions.getAxisValue(ToolsAxis.Horizontal);

                // Can control

                //float desiredSpeed = ToolsInput.getAxisValue(ToolsAxis.Vertical) * speedVariation + speedDefault;
                float desiredSpeedFB = translationForwardBackwardValue * speedVariation + speedDefault;
                if (speedCurrent < desiredSpeedFB)
                    newSpeed = Math.Min(speedCurrent + deltaTime * accelerationMax, desiredSpeedFB);
                else
                    newSpeed = Math.Max(speedCurrent - deltaTime * accelerationMax, desiredSpeedFB);

                translation.z = newSpeed * deltaTime;

                float desiredSpeedLR = translationLeftRightValue * speedVariation + speedDefault;
                if (speedCurrentLR < desiredSpeedLR)
                    newSpeedLR = Math.Min(speedCurrentLR + deltaTime * accelerationMax, desiredSpeedLR);
                else
                    newSpeedLR = Math.Max(speedCurrentLR - deltaTime * accelerationMax, desiredSpeedLR);

                translation.x = newSpeedLR * deltaTime;



                //rotation.y = angularSpeed * deltaTime * translationLeftRightValue;
                rotation.y = angularSpeed * deltaTime * rotationLeftRightValue;
                rotation.x = angularSpeed * deltaTime * rotationForwardBackwardValue;

                //rotation.y = angularSpeed * deltaTime * ToolsInput.getAxisValue(ToolsAxis.Horizontal);
            }

            speedCurrent = newSpeed;
            speedCurrentLR = newSpeedLR;

            return true;
        }

        public bool applyMvt(Agent a, Vector3 translation, Vector3 rotation)
        {
            a.Rotate(rotation);
            a.Translate(translation);
            return true;
        }

        public void initialize(Agent a)
        {
        }
    }
}
