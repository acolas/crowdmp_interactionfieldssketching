﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;
using UnityEngine.AI;

namespace CrowdMP.Core
{

    /// <summary>
    /// Law controlling the speed and orientation of an agent to reach a list of goals using unity nav mesh to navigate to the goal
    /// </summary>
    public class LawGoals : ControlLaw
    {
        [XmlAttribute]
        public float speedCurrent;
        [XmlAttribute]
        public float speedDefault;
        [XmlAttribute]
        public float accelerationMax;
        [XmlAttribute]
        public float angularSpeed;
        [XmlAttribute]
        public float reachedDist;
        [XmlAttribute]
        public bool isLooping;


        [XmlArray("Goals")]
        [XmlArrayItem("Goal")]
        public List<ConfigVect> goals;

        private Agent linkedAgent;
        private NavMeshAgent nav;
        private int currGoal = 0;
        private Vector3 oldPos;
        private Vector3 Noise;
        private NavMeshPath firstPath;
        private float approxRemainingDist;
        private float elevationOffset;

        private float timeToWait;

        public LawGoals()
        {
            speedCurrent = 0;
            speedDefault = 1.33f;
            accelerationMax = 0.8f;
            reachedDist = 0.5f;
            angularSpeed = 360 * 100;
            isLooping = false;
            goals = new List<ConfigVect>();
            linkedAgent = null;
            approxRemainingDist = 0;
        }

        public void initialize(Agent a)
        {
            Vector3 tmpPos = a.transform.position;
            //GameObject go = new GameObject();
            //go.transform.SetParent(a.gameObject.transform);
            //go.transform.position = a.gameObject.transform.position;
            //go.transform.rotation = a.gameObject.transform.rotation;
            //nav = go.gameObject.AddComponent<NavMeshAgent>();
            nav= a.gameObject.GetComponent<NavMeshAgent>();
            if (nav==null)
                nav = a.gameObject.AddComponent<NavMeshAgent>();
            elevationOffset = tmpPos.y - a.transform.position.y;


            NavMeshHit hit = new NavMeshHit();

            foreach (ConfigVect g in goals)
            {
                NavMesh.SamplePosition(g.vect, out hit, 10, 1);
                g.vect = hit.position;
                //Debug.Log(hit.position);
            }

            NavMesh.SamplePosition(a.transform.position, out hit, 10, 1);
            nav.Warp(hit.position);
            a.transform.position = hit.position;
            oldPos = hit.position;
            //nav.Move(new Vector3(0, -1, 0));
            nav.updatePosition = false;
            nav.updateRotation = false;

            nav.autoBraking = false;
            nav.autoRepath = true;
            nav.autoTraverseOffMeshLink = true;

            nav.radius = 0.33f;
            nav.acceleration = accelerationMax;
            nav.angularSpeed = angularSpeed;
            nav.obstacleAvoidanceType = ObstacleAvoidanceType.NoObstacleAvoidance;
            nav.speed = speedDefault;
            approxRemainingDist = 0;

            linkedAgent = a;
            //nextGoal();
            //nav.velocity = (nav.steeringTarget - nav.transform.position).normalized * speedCurrent;
            Noise = new Vector3(UnityEngine.Random.value * 5, UnityEngine.Random.value * 0, UnityEngine.Random.value * 5);

            firstPath = new NavMeshPath();
            NavMesh.CalculatePath(hit.position, goals[currGoal].vect, NavMesh.AllAreas, firstPath);
            timeToWait = goals[currGoal].t;
            currGoal = isLooping ? (currGoal + 1) % goals.Count : (currGoal + 1);

            if ((!isLooping && goals.Count == currGoal) || timeToWait > 0)
                nav.autoBraking = true;
            else
                nav.autoBraking = false;

            a.transform.position += new Vector3(0,elevationOffset,0);
        }

        bool nextGoal()
        {

            if (firstPath != null)
            {
                nav.SetPath(firstPath);
                firstPath = null;
                return true;
            }
            if (nav.pathPending == true)
                return true;

            if (timeToWait > 0)
            {
                timeToWait -= ToolsTime.DeltaTime;
                return false;
            }

            if (goals.Count > currGoal)
            {

                //Debug.Log("SetDest => " + goals[currGoal].vect);
                //if ()
                //    Debug.Log("Destination set");
                //else
                //    Debug.Log("Destination Not Set");
                nav.SetDestination(goals[currGoal].vect);
                timeToWait = goals[currGoal].t;

                currGoal = isLooping ? (currGoal + 1) % goals.Count : (currGoal + 1);


                if (!isLooping && goals.Count == currGoal)
                    nav.autoBraking = true;
            }
            return true;
        }

        public bool computeGlobalMvt(float deltaTime, out Vector3 translation, out Vector3 rotation)
        {
            translation = new Vector3(0, 0, 0);
            rotation = new Vector3(0, 0, 0);
            nav.Move(linkedAgent.transform.position - oldPos);
            oldPos = linkedAgent.transform.position;

            // Check goal
            if (float.IsInfinity(approxRemainingDist) || approxRemainingDist < reachedDist)
            {
                approxRemainingDist = nav.remainingDistance;
                if (approxRemainingDist < reachedDist)
                {
                    if (!nextGoal())
                    {
                        nav.nextPosition = oldPos;
                        return true;
                    }
                    return true;
                }

            }



            Vector3 direction = nav.nextPosition + new Vector3(0, elevationOffset, 0)-linkedAgent.Position;
            direction.y = 0;


            float newSpeed = direction.magnitude / deltaTime;
            if (newSpeed > speedDefault)
                newSpeed = speedDefault;

            /* Cannot control */
            if (speedCurrent < newSpeed)
                newSpeed = Math.Min(speedCurrent + deltaTime * accelerationMax, newSpeed);
            else
                newSpeed = Math.Max(speedCurrent - deltaTime * accelerationMax, newSpeed);



            //if (direction.magnitude == 0)
            //{
            //    direction = linkedAgent.transform.forward;
            //}

            translation.z = newSpeed * deltaTime;
            approxRemainingDist -= translation.z;

            rotation = direction;
            //Quaternion q = Quaternion.LookRotation(direction);
            //rotation = q.eulerAngles - linkedAgent.transform.rotation.eulerAngles;

            speedCurrent = newSpeed;


            nav.nextPosition = oldPos;

            if (!isLooping && goals.Count == currGoal && approxRemainingDist < reachedDist && nav.pathPending != true)
                return false;

            return true;
        }

        public bool applyMvt(Agent a, Vector3 translation, Vector3 rotation)
        {
            if (rotation.sqrMagnitude != 0)
                a.transform.rotation = Quaternion.LookRotation(rotation);

            a.Translate(translation);
            return true;
        }

    }
}