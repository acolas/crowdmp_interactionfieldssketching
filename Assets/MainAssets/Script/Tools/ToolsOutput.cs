﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using UnityEngine;
#if MIDDLEVR
using Hybrid.VRTools;
#endif

namespace CrowdMP.Core
{

    /// <summary>
    /// Control the creation of output file
    /// </summary>
    public class ToolsOutput
    {
        string filePath;

        public ToolsOutput(string path, bool overwrite=false)
        {
            //System.IO.Directory.CreateDirectory(_DirectoryPath);
            filePath = path;

            // Rename the file if it exists.
            int i = 0;
            while (File.Exists(filePath))
            {
                if (overwrite)
                {
                    File.Delete(filePath);
                } else
                {
                    i++;
                    filePath = path + ".c" + i.ToString();
                }
            }

#if MIDDLEVR
        if (VRTool.IsMaster())
#endif
            using (FileStream stream = File.Create(filePath)) { }
        }

        /// <summary>
        /// Adds to current log file a text.
        /// </summary>
        /// <param name='s'>
        /// Text to add inside the current log file.
        /// </param>
        public void writeLine(string s)
        {
#if MIDDLEVR
        if (VRTool.IsMaster())
#endif
            using (StreamWriter Writer = new StreamWriter(filePath, true))
            {
                Writer.WriteLine(s);
            }
        }
    }
}