﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/


using UnityEngine;

namespace CrowdMP.Core
{

    /// <summary>
    /// Control camera recording by taking screenshot at specific framerate
    /// </summary>
    public class ToolsCamRecord : MonoBehaviour
    {

        #region attributes
        private int imageIncrement = 0;             // NB images already save for incrementing files name
        public bool record = true;                  // Is recording or not

        public float timeToStart = 0;               // Time when recording shall start
        public float timeToStop = 60;               // Time when recording shall stop
        public int framerate = 25;                  // Framerate at which screenshot are taken
        public string saveDir = "Img/capture/";     // Directory where to save all the pictures
        #endregion

        /// <summary>
        /// Initialize the recording
        /// </summary>
        void Start()
        {
        }

        /// <summary>
        /// Change the recording framerate
        /// </summary>
        /// <param name="rate">new framerate</param>
        public void ChangeFramerate(int rate)
        {
            framerate = rate;
        }

        /// <summary>
        /// Create screenshot during recording time
        /// </summary>
        void Update()
        {
            if (record && !ToolsTime.isInPause && !(ToolsTime.TrialTime < timeToStart))
            {
                if (Time.captureFramerate == 0)
                    Time.captureFramerate = framerate;

                if (ToolsTime.TrialTime > timeToStop)
                {
                    record = false;
                    Time.captureFramerate = 0;
                    Debug.Log("record stopped !");
                    Application.Quit();
                    return;
                }
                ScreenCapture.CaptureScreenshot(LoaderConfig.dataPath + saveDir + imageIncrement.ToString("D" + 5) + ".png");
                imageIncrement++;
            }
        }

        //void OnGUI()
        //{
        //	Event e = Event.current;

        //	if ((EventType.KeyDown == e.type) 
        //	    && (Event.current.keyCode != KeyCode.None)) { // we need to avoid the "empty key pressed" which is detected
        //		KeyboardEvents(e.keyCode);
        //	}
        //}

        //void KeyboardEvents(KeyCode keyCode)
        //{
        //	if (KeyCode.Space == keyCode)
        //	{
        //		record = !record;
        //		if(record)
        //		{
        //			Time.captureFramerate = framerate;
        //		}
        //	}
        //}	
    }
}