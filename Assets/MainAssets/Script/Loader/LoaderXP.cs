﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using UnityEngine;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Linq;

namespace CrowdMP.Core
{

    /// <summary>
    /// Load or create trials files, contains all the parameters of the current trial and the trials list
    /// </summary>
    public class LoaderXP
    {

        private List<string> trialsList;

        public Trial currentTrial;
        public int maxTrial { get { return trialsList!=null ? trialsList.Count : -1; } }

        public LoaderXP()
        {
            // -------------
            // LOAD XP ORDER
            string orderFilePath = LoaderConfig.dataPath + @"/" + LoaderConfig.xpOrderFile.Replace("{USER}", LoaderConfig.xpCurrentUser.ToString());
            orderFilePath = orderFilePath.Replace("{TWO}", (LoaderConfig.xpCurrentUser % 2).ToString());

            if (File.Exists(orderFilePath) == true)
            {
                // If the file exist, store it's content and go to the first scene
                string[] sourceFileContentArray = File.ReadAllLines(orderFilePath);
                trialsList = sourceFileContentArray.ToList();

                ToolsDebug.log("Loaded scenario file : " + orderFilePath + ". Got " + trialsList.Count + " files");
            }
            else
            {
                ToolsDebug.logFatalError("End of program while trying to reach user " + LoaderConfig.xpCurrentUser.ToString() + " (File " + orderFilePath + " doesn't exist");
                Application.Quit();
            }
            // LOAD XP ORDER
            // -------------
        }

        public LoaderXP(Trial trial)
        {
            // -------------
            // LOAD XP ORDER
            // If the file exist, store it's content and go to the first scene
            trialsList = null;
            currentTrial = trial;
            // LOAD XP ORDER
            // -------------
        }

        /// <summary>
        /// Load current trial's parameters
        /// </summary>
        /// <returns>False if no more trial</returns>
        public bool loadXP()
        {
            if (LoaderConfig.xpCurrentTrial < 0)
                LoaderConfig.xpCurrentTrial = 0;
            if (trialsList != null && LoaderConfig.xpCurrentTrial < trialsList.Count)
            {
                string filePath = LoaderConfig.dataPath + trialsList[LoaderConfig.xpCurrentTrial];

                if (File.Exists(filePath) == true)
                {
                    currentTrial = (Trial)LoaderXML.LoadXML<Trial>(filePath);
                    return true;
                }
                else
                {
                    ToolsDebug.logError("Error, file " + filePath + " doesn't exist (found on line " + LoaderConfig.xpCurrentTrial + " of " + LoaderConfig.xpOrderFile + ").");
                }
            }
            return false;
        }

        /// <summary>
        /// Create a template trial file
        /// </summary>
        public static void CreateDefaultScenarioFile()
        {
            Trial defaultTrial = new Trial();

            //GazeReplayconfig parameters = new GazeReplayconfig();
            //parameters.rightEye = new eyeConfig();
            //defaultTrial.config = parameters;

            TrialRegularAgent tmp = new TrialRegularAgent();
            LawStanding tmpLawMoveStraight = new LawStanding();
            tmp.xmlControlLaw = tmpLawMoveStraight;
            tmp.xmlControlSim = new CustomXmlSerializer<TrialControlSim>();
            tmp.xmlControlSim.Data = new VOGConfig();
            defaultTrial.agents.Add(tmp);

            tmp = new TrialRegularAgent();
            LawFileData tmpLawWaypoint = new LawFileData();
            //tmpLawWaypoint.goals.Add(new ConfigVect());
            tmp.xmlControlLaw = tmpLawWaypoint;
            defaultTrial.agents.Add(tmp);
            defaultTrial.scene.endings.Add(new TrialEnding());
            defaultTrial.screenRecorder = new TrialScreenRecorder();

            defaultTrial.recordDataList = new List<CustomXmlSerializer<RecorderData>>();
            defaultTrial.recordDataList.Add(new DataUnitySpatial());
            defaultTrial.recordDataList.Add(new DataDeviceState());


            LoaderXML.CreateXML<Trial>(LoaderConfig.dataPath + @"/TrialTemplate.xml", defaultTrial);
        }
    }

    /// <summary>
    /// Trial parameters (XML serializable)
    /// </summary>
    public class Trial
    {
        public TrialScene scene;
        public CustomXmlSerializer<TrialConfig> config;

        [XmlElement("player")]
        public CustomXmlSerializer<TrialPlayer> player;


        public TrialScreenRecorder screenRecorder;
        [XmlArray("SavedDataList")]
        [XmlArrayItem("data", type: typeof(CustomXmlSerializer<RecorderData>))]
        public List<CustomXmlSerializer<RecorderData>> recordDataList;

        [XmlArray("agents")]
        [XmlArrayItem("agent")]
        public List<CustomXmlSerializer<TrialAgent>> agents;

        public Trial()
        {
            scene = new TrialScene();
            player = new TrialRegularPlayer();
            screenRecorder = null;

            recordDataList = null;
            agents = new List<CustomXmlSerializer<TrialAgent>>();
        }
    }

    #region Scene
    /// <summary>
    /// Trial parameters concerning the scene (XML serializable)
    /// </summary>
    public class TrialScene
    {
        [XmlAttribute]
        public string meshName;

        public ConfigVect Position;
        public ConfigVect Rotation;

        public string recordingFile;

        [XmlArray("endingConditions")]
        [XmlArrayItem("condition")]
        public List<TrialEnding> endings;

        public TrialScene()
        {
            meshName = "Kerlann";
            Position = new ConfigVect();
            Rotation = new ConfigVect();
            recordingFile = "Output_{USER}_{ITT}.csv";

            endings = new List<TrialEnding>();
        }

    }

    /// <summary>
    /// Trial parameters concerning the ending condition (XML serializable)
    /// </summary>
    public class TrialEnding
    {
        [XmlAttribute]
        public TrialParam parameter;
        [XmlAttribute]
        public TrialTest test;
        [XmlAttribute]
        public float value;

        public TrialEnding()
        {
            parameter = TrialParam.time;
            test = TrialTest.greater;
            value = 60;
        }

    }

    /// <summary>
    /// Enum the trials variables
    /// </summary>
    public enum TrialParam
    {
        time,
        x,
        y
    }

    /// <summary>
    /// Enum test that can be perform on trial variables
    /// </summary>
    public enum TrialTest
    {
        greater,
        less
    }
    #endregion

    public class TrialScreenRecorder
    {
        [XmlAttribute]
        public bool record;                  // Is recording or not
        [XmlAttribute]
        public float timeToStart;               // Time when recording shall start
        [XmlAttribute]
        public float timeToStop;               // Time when recording shall stop
        [XmlAttribute]
        public int framerate;                  // Framerate at which screenshot are taken

        public string saveDir;     // Directory where to save all the pictures

        public TrialScreenRecorder()
        {
            record = false;
            timeToStart = 0;
            timeToStop = 0;
            framerate = 25;
            saveDir = "./Output/";
        }
    }

    #region Agents
    /// <summary>
    /// Interface for the trial parameters concerning an agent control law (XML serializable)
    /// </summary>
    public interface TrialControlSim
    {
        int getConfigId();
        ControlSim createControlSim(int id);
    }

    /// <summary>
    /// Trial parameters concerning the player's input device (XML serializable)
    /// </summary>
    public class TrialDevice
    {
        [XmlAttribute]
        public string name;
        [XmlAttribute]
        public bool reverseHorizontalAxis;
        [XmlAttribute]
        public bool reverseVerticalAxis;

        public TrialDevice()
        {
            name = "Joystick";
            reverseHorizontalAxis = false;
            reverseVerticalAxis = false;
        }
    }
    #endregion

    /// <summary>
    /// Vector3 equivalent for XML serialization
    /// </summary>
    public class ConfigVect
    {
        [XmlAttribute]
        public float x;
        [XmlAttribute]
        public float y;
        [XmlAttribute]
        public float z;
        [XmlAttribute]
        public float t;

        public ConfigVect()
        {
            x = 0;
            y = 0;
            z = 0;
            t = 0;
        }

        public ConfigVect(float naturalX, float naturalY, float naturalZ)
        {
            x = naturalX;
            y = naturalY;
            z = naturalZ;
            t = 0;
        }

        public ConfigVect(Vector3 v)
        {
            x = -v.x;
            y = v.z;
            z = v.y;
        }


        [XmlIgnoreAttribute]
        public Vector3 vect
        {
            get { return new Vector3(-x, z, y); }
            set { x = -value.x; y = value.z; z = value.y; }
        }

    }
}