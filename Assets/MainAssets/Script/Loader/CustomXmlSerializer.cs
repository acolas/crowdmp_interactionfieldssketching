﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining
**  a copy of this software and associated documentation files (the
**  "Software"), to deal in the Software without restriction, including
**  without limitation the rights to use, copy, modify, merge, publish,
**  distribute, sublicense, and/or sell copies of the Software, and to
**  permit persons to whom the Software is furnished to do so, subject
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
**  SOFTWARE.
**
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using System;
using System.Xml.Serialization;

namespace CrowdMP.Core
{
    /// <summary>
    /// Serializer to handle xml serialization of abstract classes
    /// Code from https://stackoverflow.com/questions/20084/xml-serialization-and-inherited-types
    /// </summary>
    public class CustomXmlSerializer<AbstractType> : IXmlSerializable
    {
        #region Fields

        private AbstractType _data;

        #endregion

        #region Properties

        /// <summary>
        /// [Concrete] Data to be stored/is stored as XML.
        /// </summary>
        public AbstractType Data
        {
            get => _data;
            set => _data = value;
        }

        #endregion

        #region Constructors

        /// <summary>
        /// **DO NOT USE** This is only added to enable XML Serialization.
        /// </summary>
        /// <remarks>DO NOT USE THIS CONSTRUCTOR</remarks>
        public CustomXmlSerializer()
        {
            // Default Ctor (Required for Xml Serialization - DO NOT USE)
        }

        /// <summary>
        /// Initialises the Serializer to work with the given data.
        /// </summary>
        /// <param name="data">Concrete Object of the AbstractType Specified.</param>
        public CustomXmlSerializer(AbstractType data)
        {
            _data = data;
        }

        #endregion

        #region Methods

        // Override the Implicit Conversions Since the XmlSerializer
        // Casts to/from the required types implicitly.
        public static implicit operator AbstractType(CustomXmlSerializer<AbstractType> o)
        {
            return o.Data;
        }

        public static implicit operator CustomXmlSerializer<AbstractType>(AbstractType o)
        {
            return o == null ? null : new CustomXmlSerializer<AbstractType>(o);
        }

        #endregion

        #region IXmlSerializable Members

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null; // this is fine as schema is unknown.
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            // Cast the Data back from the Abstract Type.
            reader.ReadStartElement();
            string typeAttrib = reader.LocalName;

            string assemblyName = reader.GetAttribute("assembly");
            string fullName = typeAttrib;
            if (!string.IsNullOrWhiteSpace(assemblyName))
                fullName += "," + assemblyName;

            string @namespace = reader.GetAttribute("namespace");
            if (!string.IsNullOrWhiteSpace(@namespace))
                fullName += @namespace + "." + fullName;

            // Ensure the Type was Specified
            if (typeAttrib == null)
            {
                throw new ArgumentNullException("Unable to Read Xml Data for Abstract Type '" + typeof(AbstractType).Name +
                    "' because no 'type' attribute was specified in the XML.");
            }

            Type type = Type.GetType(fullName);

            // Check the Type is Found.
            if (type == null)
            {
                type = Type.GetType("CrowdMP.Core." + fullName);
            }
            if (type == null)
            {
                throw new InvalidCastException("Unable to Read Xml Data for Abstract Type '" + typeof(AbstractType).Name +
                    "' because the type " + fullName + " specified in the XML was not found.");
            }

            // Check the Type is a Subclass of the AbstractType.
            if (!typeof(AbstractType).IsAssignableFrom(type))
            {
                throw new InvalidCastException("Unable to Read Xml Data for Abstract Type '" + typeof(AbstractType).Name +
                    "' because the Type specified in the XML differs ('" + type.Name + "').");
            }

            // Read the Data, Deserializing based on the (now known) concrete type.
            //reader.ReadStartElement();
            Data = (AbstractType)new
                XmlSerializer(type).Deserialize(reader);
            reader.ReadEndElement();
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            // Write the Type Name to the XML Element as an Attrib and Serialize
            Type type = _data.GetType();

            // BugFix: Assembly must be FQN since Types can/are external to current.
            //writer.WriteAttributeString("type", type.FullName);
            new XmlSerializer(type).Serialize(writer, _data);
        }

        #endregion
    }
}