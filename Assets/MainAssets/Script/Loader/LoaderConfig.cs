﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using UnityEngine;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace CrowdMP.Core
{

    /// <summary>
    /// Load or create config/trials files, contains all the config and current trial parameters
    /// </summary>
    public static class LoaderConfig
    {
        #region staticAttributs
        static public string dataPath;

        static private Config data;
        static private LoaderXP trialData;
        private static string configFileName;
        #endregion

        static LoaderConfig()
        {
            string pathPlayer = Application.dataPath;
            int lastIndex = pathPlayer.LastIndexOf('/');
            dataPath = pathPlayer.Remove(lastIndex, pathPlayer.Length - lastIndex);

            //configFileName = dataPath + @"/Config.xml";

            data = null;
            //loadConfig(configFileName);

            //if (data == null)  
            //{
            //    CreateDefaultConfigFile();
            //    ToolsDebug.logFatalError("No config file, see folder configTemplate.xml");
            //    Application.Quit();
            //}

            //trialData = new LoaderXP();
        }

        public static bool loadConfig(string path)
        {
            configFileName = dataPath + path;

            if (File.Exists(path))
            {                
                data = (Config)LoaderXML.LoadXML<Config>(configFileName);
                trialData = new LoaderXP();
                return true;
            }
            else
            {
                return false;
            }
        }

        public static void loadConfig(TrialGen generator)
        {
            if (generator!=null)
            {
                configFileName = ".";
                data = new Config();
                data.log.debugLvl = 0;
                data.Recording.folder = "./";
                data.experience.userHeight = 1.7f;

                trialData = new LoaderXP(generator.createTrial());
            }
        }

        /// <summary>
        /// Create template of the config file
        /// </summary>
        public static void CreateDefaultConfigFile()
        {
            Config defaultData = new Config();

            //defaultData.addOnList.Add(new CustomXmlSerializer<ConfigExtra>(new ConfigFove()));

            LoaderXML.CreateXML<Config>(dataPath + @"/ConfigTemplate.xml", defaultData);

            LoaderXP.CreateDefaultScenarioFile();
        }


        /// <summary>
        /// Load parameters for current trial
        /// </summary>
        /// <returns>Return false if no more trial to load</returns>
        public static bool LoadXP()
        {
            if (trialData == null)
            {
                return false;
            }
            return trialData.loadXP();
        }

        /// <summary>
        /// Goes to the next trial file
        /// </summary>
        public static void NextTrial()
        {
            xpCurrentTrial++;
        }

        /// <summary>
        /// Goes to the previous trial file
        /// </summary>
        public static void ChangeTrial(int inc)
        {
            xpCurrentTrial += inc;
        }
        /// <summary>
        /// Goes to the previous trial file
        /// </summary>
        public static void PreviousTrial()
        {
            xpCurrentTrial--;
        }


        #region get_set
        // User
        public static int xpCurrentUser { get { return data.experience.userID; } }
        public static int xpCurrentTrial
        {
            get { return data.experience.trial; }
            set { data.experience.trial = value; }
        }
        public static int xpMaxTrial { get { return trialData.maxTrial; } }
        public static string xpOrderFile { get { return data.experience.xpFiles; } }
        public static float xpUserHeight { get { return data.experience.userHeight; } }
        // Log
        public static int debugLvl { get {
                if (data == null)
                {
                    return -1;
                }
                return data.log.debugLvl; } }
        // Recorder
        public static string RecDataSeparator { get { return data.Recording.dataSeparator; } }
        public static string RecDecimalSeparator { get { return data.Recording.decimalSeparator; } }
        public static string RecFolder { get { return data.Recording.folder; } }
        public static bool RecHeaders { get { return data.Recording.useHeaders; } }
        // Current Trial - Scene
        public static string sceneName { get { return trialData.currentTrial.scene.meshName; } }
        public static TrialConfig sceneConfig { get { return trialData.currentTrial.config.Data; } }
        public static Vector3 scenePosition { get { return trialData.currentTrial.scene.Position.vect; } }
        public static Vector3 sceneRotation { get { return trialData.currentTrial.scene.Rotation.vect; } }
        public static string sceneOutputFile { get { return trialData.currentTrial.scene.recordingFile; } }
        public static List<TrialEnding> sceneEndings { get { return trialData.currentTrial.scene.endings; } }
        // Current Trial - Player
        public static TrialPlayer playerInfo { get { return trialData.currentTrial.player; } }
        //public static Vector3   playerPosition          { get { return trialData.currentTrial.player.Position.vect; } }
        //public static Vector3   playerRotation          { get { return trialData.currentTrial.player.Rotation.vect; } }
        //public static TrialDevice playerDevice          { get { return trialData.currentTrial.player.device; } }
        //public static ControlLaw playerControlLaw       { get { return trialData.currentTrial.player.controlLaw; } }
        //public static TrialControlSim playerControlSim  { get { return trialData.currentTrial.player.controlSim; } }
        // Current Trial - Agent
        public static List<CustomXmlSerializer<TrialAgent>> agentsInfo { get { return trialData.currentTrial.agents; } }
        // Record data
        public static List<CustomXmlSerializer<RecorderData>> recordedDataList { get { return trialData.currentTrial.recordDataList; } }
        public static TrialScreenRecorder screenRecorder { get { return trialData.currentTrial.screenRecorder; } }
        // ADD ON
        public static List<CustomXmlSerializer<ConfigExtra>> addons { get { return data.addOnList; } }
        #endregion
    }

    /// <summary>
    /// Config parameters (XML serializable)
    /// </summary>
    public class Config
    {
        public ConfigUser experience;
        public ConfigRecorder Recording;
        public ConfigLog log;

        [XmlArray("AddOns")]
        [XmlArrayItem("AddOn", type: typeof(CustomXmlSerializer<ConfigExtra>))]
        public List<CustomXmlSerializer<ConfigExtra>> addOnList;


        public Config()
        {
            experience = new ConfigUser();
            log = new ConfigLog();
            Recording = new ConfigRecorder();
            addOnList = new List<CustomXmlSerializer<ConfigExtra>>();
        }

    }

    /// <summary>
    /// Config parameters concerning the users (XML serializable)
    /// </summary>
    public class ConfigUser
    {
        [XmlAttribute("currentUser")]
        public int userID;
        [XmlAttribute("startingTrial")]
        public int trial;

        [XmlElement("sourceFileExperiment")]
        public string xpFiles;

        public float userHeight;

        public ConfigUser()
        {
            userID = 0;
            trial = 0;
            userHeight = 1.7f;
            xpFiles = "./Scenario/Test/FileOrder{USER}.csv";
        }
    }

    /// <summary>
    /// Config parameters concerning the logging process (XML serializable)
    /// </summary>
    public class ConfigLog
    {
        [XmlAttribute]
        public int debugLvl;

        public ConfigLog()
        {
            debugLvl = 0;
        }
    }

    /// <summary>
    /// Config parameters concerning the recorded data format (XML serializable)
    /// </summary>
    public class ConfigRecorder
    {
        [XmlAttribute]
        public string dataSeparator;
        [XmlAttribute]
        public string decimalSeparator;
        [XmlAttribute]
        public bool useHeaders;

        public string folder;

        public ConfigRecorder()
        {
            dataSeparator = ",";
            decimalSeparator = ".";
            useHeaders = true;

            folder = "./Output/";
        }

    }

}