﻿/*  CrowdMP - Platform to design virtual experiment with reactive crowd
**  MIT License
**  Copyright(C) 2020  - Inria Rennes - Rainbow - Julien Pettre
**
**  Permission is hereby granted, free of charge, to any person obtaining 
**  a copy of this software and associated documentation files (the 
**  "Software"), to deal in the Software without restriction, including 
**  without limitation the rights to use, copy, modify, merge, publish, 
**  distribute, sublicense, and/or sell copies of the Software, and to 
**  permit persons to whom the Software is furnished to do so, subject 
**  to the following conditions:
**
**  The above copyright notice and this permission notice shall be 
**  included in all copies or substantial portions of the Software.
**
**  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
**  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
**  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
**  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
**  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
**  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
**  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
**  SOFTWARE.
**  
**  Authors: Julien Bruneau
**  Contact: crowd_group@inria.fr
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CrowdMP.Core
{

    /// <summary>
    /// The main manager controlling the Experiment flow
    /// </summary>
    public class SpawnManager : MonoBehaviour
    {

        public TrialGen configGen;
        public bool noTransition = false;

        private bool trialStarted;
        private bool isEndXP;
        private Player player;
        private GameObject[] sceneObjectsModels;
        private GameObject[] agentModels;
        private GameObject[] playerModels;
        private GameObject sceneGameObject;

        private GameObject transitionSign;
        private string transitionText;
        private GameObject endSign;

        private TrialManager currentTrialManager;

        void Awake()
        {
            if (configGen == null)
            {
                ToolsDebug.logFatalError("Error, Trial Generator not linked to manager");
                Application.Quit();
            }

            UnityEngine.Random.InitState(454682);
            LoaderConfig.loadConfig(configGen);
        }

        // Use this for initialization
        void Start()
        {


            if (!initializeConstants())
            {
                ToolsDebug.logFatalError("Error on GUIManager constants initialisation");
                Application.Quit();
            }

            isEndXP = false;
            startTrial();
            startTransition();
            if (noTransition)
            {
                if (!trialStarted)
                {
                    trialStarted = true;
                    currentTrialManager.startTrial();
                }

                if (isEndXP)
                    CleanQuit();
                else
                    transitionSign.SetActive(ToolsTime.tooglePause());
            }
        }

        /// <summary>
        /// Load and setup current trial
        /// </summary>
        void startTrial()
        {
            // Load the trial scene and unload the others
            string sceneMeshName = LoaderConfig.sceneName;
            foreach (GameObject currentGameObject in sceneObjectsModels)
            {
                /*Make every scene disapered exept the one given by the scenarioLoader*/
                if (currentGameObject.name == sceneMeshName)
                {
                    currentGameObject.SetActive(true);

                    if (currentGameObject.GetComponent<TrialManager>() == null)
                    {
                        ToolsDebug.logFatalError("The trial need a Manager");
                        return;
                    }


                    sceneGameObject = currentGameObject;
                    sceneGameObject.transform.position = LoaderConfig.scenePosition;
                    sceneGameObject.transform.rotation = Quaternion.Euler(LoaderConfig.sceneRotation);
                    break;
                }
            }

            if (sceneGameObject == null)
                ToolsDebug.logFatalError("Scene not found (" + sceneMeshName + ")");

            // Reset trial time
            ToolsTime.newLevel();

            // Initialize the trial manager
            currentTrialManager = sceneGameObject.GetComponent<TrialManager>();
            if (currentTrialManager == null)
            {
                ToolsDebug.logFatalError("Trials " + LoaderConfig.xpCurrentTrial.ToString() + " does not have a manager");
                CleanQuit();
            }
            currentTrialManager.initializeTrial(playerModels, agentModels);
            player = currentTrialManager.getPlayer();

            trialStarted = false;
        }

        /// <summary>
        /// Method used to initialize scene constants
        /// </summary>
        /// <returns>True if constant are well initialized, otherwise False</returns>
        private bool initializeConstants()
        {
            playerModels = GameObject.FindGameObjectsWithTag("Player");
            foreach (GameObject p in playerModels)
            {
                p.SetActive(false);
            }
            sceneObjectsModels = GameObject.FindGameObjectsWithTag("Stage");
            foreach (GameObject scene in sceneObjectsModels)
            {
                scene.SetActive(false);
            }

            agentModels = GameObject.FindGameObjectsWithTag("AgentModels");
            foreach (GameObject go in agentModels)
            {
                go.SetActive(false);
            }


            Canvas[] signs = GetComponentsInChildren<Canvas>();
            foreach (Canvas sign in signs)
            {
                if (sign.gameObject.name == "Transition")
                {
                    transitionSign = sign.gameObject;
                    transitionText = transitionSign.GetComponentInChildren<Text>().text;

                    //// Place in front of camera
                    //transitionSign.transform.SetParent(playerHead.transform);
                    //transitionSign.transform.position = playerHead.transform.position + playerHead.transform.forward * 2;
                    //transitionSign.transform.rotation = playerHead.transform.rotation;
                }
                if (sign.gameObject.name == "End")
                {
                    endSign = sign.gameObject;

                    // Place in front of camera
                    //endSign.transform.SetParent(playerHead.transform);
                    //endSign.transform.position = playerHead.transform.position + playerHead.transform.forward * 2;
                    //endSign.transform.rotation = playerHead.transform.rotation;

                }

                sign.gameObject.SetActive(false);
            }

            PluginManager[] plugins = gameObject.GetComponentsInChildren<PluginManager>();
            foreach (PluginManager plugin in plugins)
            {
                plugin.LoadPlugin();
            }

            return true;
        }

        // Update is called once per frame
        void Update()
        {
            ToolsTime.updateTime();

            if (currentTrialManager == null)
                return;

            if (currentTrialManager.hasEnded())
                endTrial();
            else
                currentTrialManager.doStep();


        }

        // Handle key/button input in LateUpdate
        private void LateUpdate()
        {
            if ((currentTrialManager == null || currentTrialManager.isReady())
                && (XRInputEvents.currentXRInputEvents.StartButtonDown))
            {
                if (isEndXP)
                    CleanQuit();
                else
                {
                    if (!trialStarted)
                    {
                        trialStarted = true;
                        currentTrialManager.startTrial();
                    }
                    transitionSign.SetActive(false);
                    ToolsTime.tooglePause();
                }
            }

            if (XRInputEvents.currentXRInputEvents.GoNextButtonDown)
            {
                endTrial();
            }

            if (XRInputEvents.currentXRInputEvents.GoPreviousButtonDown)
            {
                endTrial(-1);
            }

            if (XRInputEvents.currentXRInputEvents.RepeatButtonDown)
            {
                endTrial(0);
            }

            if (XRInputEvents.currentXRInputEvents.EscapeButtonDown)
            {
                CleanQuit();
            }
        }

        /// <summary>
        /// End the current trial change it
        /// </summary>
        void endTrial(int trialSwitch = 1)
        {
            currentTrialManager.clear();
            sceneGameObject.gameObject.SetActive(false);
            sceneGameObject = null;
            currentTrialManager = null;
            startTrial();
            startTransition();
            if (noTransition)
            {
                if (!trialStarted)
                {
                    trialStarted = true;
                    currentTrialManager.startTrial();
                }

                if (isEndXP)
                    CleanQuit();
                else
                    transitionSign.SetActive(ToolsTime.tooglePause());
            }
        }

        /// <summary>
        /// Update and load transition screen
        /// </summary>
        void startTransition()
        {
            if (isEndXP)
            {
                player.gameObject.SetActive(true);
                endSign.gameObject.SetActive(true);
                return;
            }



            GameObject playerHead = player.getHeadObject();
            // Place in front of camera
            transitionSign.transform.SetParent(playerHead.transform);
            transitionSign.transform.position = playerHead.transform.position + playerHead.transform.forward * 2;
            transitionSign.transform.rotation = playerHead.transform.rotation;

            endSign.transform.SetParent(playerHead.transform);
            endSign.transform.position = playerHead.transform.position + playerHead.transform.forward * 2;
            endSign.transform.rotation = playerHead.transform.rotation;


            transitionSign.GetComponentInChildren<Text>().text = transitionText;
            transitionSign.GetComponentInChildren<Text>().text = transitionSign.GetComponentInChildren<Text>().text.Replace("{ITT}/{TOT}", "");

            transitionSign.SetActive(true);
        }
        void CleanQuit()
        {
            PluginManager[] plugins = gameObject.GetComponentsInChildren<PluginManager>();
            foreach (PluginManager plugin in plugins)
            {
                plugin.UnloadPlugin();
            }

#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
                Application.Quit();
#endif
        }
    }

}