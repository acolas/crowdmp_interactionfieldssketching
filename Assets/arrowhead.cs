using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class arrowhead : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = GetComponentInParent<LineRenderer>().GetPosition(GetComponentInParent<LineRenderer>().positionCount - 1);  
    }
}
