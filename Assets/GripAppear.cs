using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine;

public class GripAppear : MonoBehaviour
{
    // Start is called before the first frame update
    public InputActionReference apparition = null;

    void Start()
    {
        apparition.action.Enable();
        GetComponent<MeshRenderer>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(apparition.action.ReadValue<float>() == 1f) {
            GetComponent<MeshRenderer>().enabled = !GetComponent<MeshRenderer>().enabled;

        }
    }
}
