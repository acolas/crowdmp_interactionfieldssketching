
using UnityEngine;
using UnityEngine.InputSystem;

public class DragObject : MonoBehaviour
{

    public InputActionReference drag = null;
    private bool isSelected= false;
    [Header("Select To include in drag")]
    public bool x;
    public bool y;
    public bool z;

    public void Update()
    {
        Drag();

        
    }

    public void Hover()
    {

        gameObject.GetComponent<Renderer>().material.EnableKeyword("_EMISSION");
        
    }
    public void HoverOut()
    {

        gameObject.GetComponent<Renderer>().material.DisableKeyword("_EMISSION");

    }
    public void Selected()
    {
        //gameObject.GetComponent<Renderer>().material.EnableKeyword("_EMISSION");
        isSelected = true;
       
    }

    public void Activate()
    {
        if (isSelected)
        {
            
            Destroy(gameObject);
            
        }
    }
    public void Drag()
    {
       
        if (isSelected)
        {
            Transform pointer = GameObject.FindGameObjectWithTag("Pointer").transform;
            float newX = x ? pointer.position.x : transform.position.x;
            float newY = y ? pointer.position.y : 0.1f;
            float newZ = z ? pointer.position.z : transform.position.z;

            Vector3 candidate = new Vector3(newX, 0.1f, newZ);

            
            //if (GetComponentInParent<BoxCollider>().bounds.Contains(candidate))
            //{
                this.transform.position = candidate;
            //}

        }


    }
    public void Drop()
    {

        isSelected = false;
        gameObject.GetComponent<Renderer>().material.DisableKeyword("_EMISSION");
       
    }
}
